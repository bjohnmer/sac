<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reportes extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}

		$this->load->library("form_validation");
		$this->load->library("datemanager");
	}
	public function ver($data = null, $titulo = null, $vista = "contenido")
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$vista,$titulo);
		$this->load->view('commons/footer');
	}
	public function ii()
	{
		$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
		$data['periodo'] = $p->perid;
		$titulo['titulo'] = "Imprimir Inscripción";
		$this->ver($data,$titulo,"formiinscripcion");
	}
	public function print_ii()
	{
		$this->form_validation->set_rules('cedula','Cédula del Estudiante','required|max_length[15]|min_length[4]|trim|numeric|integer');
		if ($this->form_validation->run()) 
		{
			$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);

			$data['estudiante'] = $this->db
										->join("recaudos","estudiantes.estcodigodo = recaudos.estudiante_id")
										->join("representantes","estudiantes.representante = representantes.repid")
										->where('estcedulava',$this->input->post('cedula'))
										->where('estcodigodo IN (SELECT estudiante FROM notas WHERE periodo = '.$p->perid.' )',false,false)
										->get('estudiantes')
										->row(0);
			
			$data['datos_inscripcion'] = $this->db
											// ->join("estudiantes","estudiantes.estcodigodo = notas.estudiante")
											->join("secciones","secciones.secid = notas.seccion")
											->join("anosescolares","anosescolares.anoid = notas.anoescolar")
											->where('estudiante',$data['estudiante']->estcodigodo)
											->where('notas.periodo',$p->perid)
											->get('notas')
											->row(0);
			
			if (!empty($data['estudiante'])) 
			{
				// $this->load->view("dashboard/print_inscripcion",$data);
				
				$this->load->library('pdf');
				$this->pdf->load_view("dashboard/print_inscripcion",$data);
				$this->pdf->render();
				$this->pdf->stream('print_inscripcion.pdf');
			}
			else 
			{
				$data['errores'] = array(
										'clase' => 'danger',
										'mensaje_error' => 'El estudiante no se encuentra inscrito en este periodo'
									);
				$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
				$data['periodo'] = $p->perid;
				$titulo['titulo'] = "Imprimir Inscripción";
				$this->ver($data,$titulo,"formiinscripcion");
			}
			
		}
		else
		{
			$this->ii();
		}
	}
	public function boletin()
	{
		$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
		$data['periodo'] = $p->perid;
		$titulo['titulo'] = "Imprimir Boletín";
		$this->ver($data,$titulo,"formboletin");
	}
	public function print_boletin()
	{
		$this->form_validation->set_rules('cedula','Cédula del Estudiante','required|max_length[15]|min_length[4]|trim|numeric|integer');
		if ($this->form_validation->run()) 
		{
			$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
			$data['estudiante'] = $this->db
										->select("estudiantes.*")
										->join("representantes","estudiantes.representante = representantes.repid")
										->where('estcedulava',$this->input->post('cedula'))
										->where('estcodigodo IN (SELECT estudiante FROM notas WHERE periodo = '.$p->perid.' )',false,false)
										->get('estudiantes')
										->row(0);
			$data['notas'] = $this->db
										->select("notas.*, anosescolares.anonombreva, secciones.seccodigova, unidadescurriculares.uninombreva")
										->join("anosescolares","notas.anoescolar = anosescolares.anoid")
										->join("unidadescurriculares","notas.materia = unidadescurriculares.uniid")
										->join("secciones","notas.seccion = secciones.secid")
										->where('estudiante',$data['estudiante']->estcodigodo)
										->where('notas.periodo',$p->perid)
										->get('notas')
										->result();
			if (!empty($data['estudiante'])) 
			{
				$this->load->library('pdf');
				$this->pdf->load_view("dashboard/print_boletin",$data);
				$this->pdf->render();
				$this->pdf->stream('print_boletin.pdf');
			}
			else 
			{
				$data['errores'] = array(
										'clase' => 'danger',
										'mensaje_error' => 'El estudiante no se encuentra inscrito en este periodo'
									);
				$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
				$data['periodo'] = $p->perid;
				$titulo['titulo'] = "Imprimir Boletín";
				$this->ver($data,$titulo,"formboletin");
			}
			
		}
		else
		{
			$this->boletin();
		}
	}
	public function nomina($data = null)
	{
		
		$data['anosescolares'] = $this->db
									->order_by('anoid','ASC')
									->get('anosescolares')
									->result();

		$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
		$data['periodo'] = $p->perid;
		$data['secciones'] = 
			$this->db
				->where("seccodanoescolarva",$data['anosescolares'][0]->anoid)
				->where("seccodigova <>","TRASLADO")
				->get('secciones')
				->result();
		// $data['materias'] = $this->db
		// 		->where("unicodanoescolarva",$data['anosescolares'][0]->anoid)
		// 		->where("uniestatusen","Activo")
		// 		->get('unidadescurriculares')
		// 		->result();
		// $data['lapsos'] = $this->db
		// 		->get('lapsos')
		// 		->result();

		$titulo['titulo'] = "Imprimir Nómina de Estudiantes";
		$this->ver($data,$titulo,"formrepnomina");
		
	}

	public function imprimirnomina($value='')
	{

		$ane = $this->input->post('anoescolar');
		$per = $this->session->userdata('perid');
		// $mat = $this->input->post('materia');
		$sec = $this->input->post('seccion');
		// $lap = $this->input->post('lapso');
		if (
			!empty($ane) &&
			!empty($per) &&
			// !empty($mat) &&
			!empty($sec) 
			// &&
			// !empty($lap) 
			) {
				$datos['anoescolar'] = $this->db
								->where('anoid',$ane)
								->get('anosescolares')
								->row(0);

				$datos['periodo'] = $this->db
								->where('perid',$per)
								->get('periodos')
								->row(0);

				// $datos['materia'] = $this->db
				// 				->where('uniid',$mat)
				// 				->get('unidadescurriculares')
				// 				->row(0);

				$datos['seccion'] = $this->db
								->where('secid',$sec)
								->get('secciones')
								->row(0);

				// $datos['lapso'] = $this->db
				// 				->where('lapsoid',$lap)
				// 				->get('lapsos')
				// 				->row(0);

				$datos['notas'] = $this->db
								->select('DISTINCT(estudiante), estudiantes.estcedulava, estudiantes.estnombreva, estudiantes.estapellidova, estudiantes.estsexoen, estudiantes.estfechanacda')
								->where('anoescolar',$ane)
								->where('periodo',$per)
								->order_by('estudiantes.estcedulava')
								->where('seccion',$sec)
								->join('estudiantes','notas.estudiante = estudiantes.estcodigodo')
								->get('notas')
								->result();
				
				if (empty($datos['notas'])) {
					
					$data['errores'] = array(
											'clase' => 'danger',
											'mensaje' => 'No hay estudiantes registrados en la sección seleccionada'
										);
					
					$this->nomina($data);
					return false;

				}

		} else {
			$datos['mensaje'] = "Falta información por seleccionar";
			$datos['clase'] = "danger";
			$this->index($datos);
			return false;
		}

		$this->load->library('pdf');
		$this->pdf->load_view("dashboard/printNomina",$datos);
		$this->pdf->render();
		$this->pdf->stream('print_inscripcion.pdf');
		
	}

	public function getSecciones()
	{
		$data = $this->db
				->where("seccodanoescolarva",$this->input->post("elegido"))
				->where("seccodigova <>","TRASLADO")
				->where("periodo",$this->session->userdata('perid'))
				->where("secestatusen","Activo")
				->get('secciones')
				->result();
		
		$opciones = "";

		if ($data) 
		{
			foreach ($data as $opcion) {
				$opciones .= "<option value='".$opcion->secid."'>".$opcion->seccodigova." </option> ";
			}
		}
		else
		{
			$opciones = '<option value="">No hay secciones</option>';
		}
		echo $opciones;
	}

	public function getMaterias()
	{
		$data = $this->db
				->where("unicodanoescolarva",$this->input->post("elegido"))
				->where("uniestatusen","Activo")
				->get('unidadescurriculares')
				->result();
		
		$opciones = "";

		if ($data) 
		{
			foreach ($data as $opcion) {
				$opciones .= "<option value='".$opcion->uniid."'>".$opcion->unicodigova." ". $opcion->uninombreva ." </option> ";
			}
		}
		else
		{
			$opciones = '<option value="">No hay Unidades Curriculares</option>';
		}
		echo $opciones;
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}