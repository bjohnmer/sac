<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class usuarios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$this->logout();	

		}

		$this->load->library("grocery_CRUD");
	}
	
	public function ver($data = null, $titulo = null, $vista = "contenido")
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$vista,$titulo);
		$this->load->view('commons/footer');
	}

	public function index()
	{
		$usuarios = new grocery_CRUD();
		$usuarios->set_table("administrador");
		$usuarios->set_subject("Usuario");

		$usuarios->columns("admloginva", "admnombreva", "admnivelen", "admestatusen", "seccional_id");

		$usuarios->display_as("admloginva","Login");
		$usuarios->display_as("admnombreva","Nombre");
		$usuarios->display_as("admnivelen","Nivel");
		$usuarios->display_as("admestatusen","Estado");
		$usuarios->display_as("admclaveva","Clave");
		$usuarios->display_as("admclavecva","Confirmar Clave");
		$usuarios->display_as("seccional_id","Seccional");
		$usuarios->set_relation("seccional_id","seccionales","{descripcion}");
		
		$usuarios->unset_add_fields("seccional_id");
		$usuarios->unset_edit_fields("seccional_id");

		$usuarios->field_type("admclaveva","password");
		$usuarios->field_type("admclavecva","password");
		$usuarios->set_rules("admloginva","Login","required|min_length[5]|max_length[15]");
		$usuarios->set_rules("admnombreva","Nombre","required|min_length[5]|max_length[50]");
		$usuarios->set_rules("admnivelen","Nivel","required");
		$usuarios->set_rules("admestatusen","Estado","required");
		$usuarios->set_rules("admclaveva","Clave","min_length[4]|max_length[20]|matches[admclavecva]");
		$usuarios->set_rules("admclavecva","Confirmar Clave","min_length[4]|max_length[20]|matches[admclaveva]");
		
		$usuarios->set_rules("fecha_nac_usuario","fecha de Nacimiento","required");
		$usuarios->set_rules("rol_usuario","Rol","required");

		$usuarios->callback_before_insert(array($this,'encrypt_clave_callback'));
		$usuarios->callback_before_update(array($this,'encrypt_confirmar_callback'));

		$usuarios->callback_edit_field('admclaveva',array($this,'vaciar_campo_clave'));
		$usuarios->callback_edit_field('admclavecva',array($this,'vaciar_campo_confirmar'));

		$usuarios->unset_read();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			$usuarios->unset_delete();
			$usuarios->unset_edit();
		}

		if ($this->session->userdata('admnivelen') == "Jefe de Seccional") 
		{
			
			$usuarios->unset_add();
			$usuarios->unset_edit();
			$usuarios->unset_delete();

		}

		if ($this->session->userdata('admnivelen') == "Administrador") 
		{
			
			$usuarios->add_action('Asignar Seccional', '', 'usuarios/asec','glyphicon glyphicon-tasks');

		}

		$data = $usuarios->render();
		

		$titulo['titulo'] = "Usuarios";
		
		$this->ver($data,$titulo);
	}

	public function asec( $user = null)
	{
		$this->load->library("form_validation");

		if ( empty( $user ) ) {
			$user =$this->uri->segment(3);
		}

	    $user = $this->db
    				->where("admid",$user)
    				->where("( admnivelen = 'Jefe de Seccional' OR admnivelen = 'Operador' )", false, false)
    				->get('administrador');
	    if (! ( $user->num_rows() == 1 ) ) 
	    {
	    	$this->session->set_flashdata('clase', 'danger');
	    	$this->session->set_flashdata('mensaje', 'El usuario no se encuentra o no es del tipo Jefe de Seccional u Operador');
	    	redirect("usuarios");
	    }

	    $titulo['titulo'] = "Asignar Seccional a este Usuario";

	    $seccionales = $this->db->get("seccionales");
	    
	    if (! ( $seccionales->num_rows() > 0 ) ) 
	    {
	    	$this->session->set_flashdata('clase', 'danger');
	    	$this->session->set_flashdata('mensaje', 'No hay seccionales para asignar');
	    	redirect("usuarios");
	    }

	    $data['seccionales'] = $seccionales->result();
	    $data['usuario'] = $user->row(0);

	    $this->ver($data , $titulo, "asignarSeccionales");

	}

	public function storeAsec()
	{
		$this->load->library("form_validation");

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) 
		{
			$post = $this->input->post();
			
			$this->form_validation->set_rules("usuario","Usuario","required|is_numeric|integer|is_natural_no_zero");
			if ( $this->form_validation->run() ) 
			{
				$seccional_id = $post['seccional_id'];
				$usuario_id = $post['usuario'];

				$user = $this->db
								->where("admid",$usuario_id)
								->get('administrador');
				
				if (! ( $user->num_rows() == 1 ) ) 
				{
					
					$this->session->set_flashdata('clase', 'danger');
					$this->session->set_flashdata('mensaje', 'El usuario no se encuentra');
					redirect("usuarios/asec/".$usuario_id);
				}

				$where = array(
						'admid' => $usuario_id
					);
				if ( $this->db->where( $where )->update('administrador', array('seccional_id' => $seccional_id ) ) ) 
				{
					
					$this->session->set_flashdata('clase', 'success');
					$this->session->set_flashdata('mensaje', 'La seccional fué asignada correctamente');
					redirect("usuarios");
				}
				else
				{

					$this->session->set_flashdata('clase', 'danger');
					$this->session->set_flashdata('mensaje', 'La seccional NO pudo ser asignada');
					redirect("usuarios");
				}
			}
			else
			{
				$this->asec( $post['usuario'] );
			}
		}
		else
		{
			
			$this->session->set_flashdata('clase', 'danger');
			$this->session->set_flashdata('mensaje', 'No tiene permiso para acceder a la página especificada');
			redirect("usuarios");
		}
	}

	function encrypt_clave_callback($post_array, $primary_key = null)
	{
		
	    $post_array['admclaveva'] = $this->encrypt->encode($post_array['admclaveva']);
	    $post_array['admclavecva'] = $post_array['admclaveva'];

	    return $post_array;
	}

	function encrypt_confirmar_callback($post_array, $primary_key = null)
	{
	    
	    if (!empty($post_array['admclaveva']))
	    {
	        $this->load->library("encrypt");
	        $post_array['admclaveva'] = $this->encrypt->encode($post_array['admclaveva']);
	        $post_array['admclavecva'] = $post_array['admclaveva'];
	    }
	    else
	    {
	    
	        unset($post_array['admclaveva']);
	        unset($post_array['admclavecva']);
	    
	    }
	    
	    return $post_array;
	
	}

	function vaciar_campo_clave($post_array, $primary_key = null)
	{
	    
	    return '<input type="password" maxlength="50" name="admclaveva" id="field-admclaveva">';
	
	}

	function vaciar_campo_confirmar($post_array, $primary_key = null)
	{
	    
	    return '<input type="password" maxlength="50" name="admclavecva" id="field-admclavecva">';
	
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}

}