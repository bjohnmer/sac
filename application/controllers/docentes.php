<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class docentes extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// print_r( $this->session->all_userdata() );
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Docentes";
		$docentes = new grocery_CRUD();
		$docentes->set_table("docentes");
		$docentes->display_as("doccedulava","Cédula");
		$docentes->display_as("docnombreva","Nombre");
		$docentes->display_as("docapellidova","Apellido");
		$docentes->display_as("docsexoen","Género");
		$docentes->display_as("docdireccionva","Dirección");
		$docentes->display_as("doctelefono1va","Teléfono");
		$docentes->display_as("doctelefono2va","Otro Teléfono");
		$docentes->display_as("docprofesionva","Profesión");
		$docentes->display_as("docfechanacda","Fecha de Nacimiento");

		$docentes->set_rules("doccedulava","Cédula","required|numeric|integer|is_natural_no_zero");
		$docentes->set_rules("docnombreva","Nombre","required");
		$docentes->set_rules("docapellidova","Apellido","required");
		$docentes->set_rules("docsexoen","Género","required");
		$docentes->set_rules("docdireccionva","Dirección","required");
		$docentes->set_rules("doctelefono1va","Teléfono","required|numeric|integer|is_natural_no_zero");
		$docentes->set_rules("doctelefono2va","Otro Teléfono","numeric|integer|is_natural_no_zero");
		$docentes->set_rules("docprofesionva","Profesión","required");
		$docentes->set_rules("docfechanacda","Fecha de Nacimiento","required");
		$docentes->callback_field('docfechanacda',array($this,'_cambiar_objeto'));

		$docentes->unset_read();
		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			$docentes->unset_delete();
			$docentes->unset_edit();
		}
		if ($this->session->userdata('admnivelen') == "Jefe de Seccional") 
		{
			
			$docentes->unset_delete();

		}

		$data = $docentes->render();
		$this->ver($data,$titulo);
	}
	public function _cambiar_objeto()
	{
		return '<input id="field-docfechanacda" class="fecha datepicker-input" type="text" maxlength="10" value="" name="docfechanacda"><a class="datepicker-input-clear" tabindex="-1">Resetear</a>(dd/mm/yyyy)';
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}