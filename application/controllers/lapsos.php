<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lapsos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Lapsos";
		$lapsos = new grocery_CRUD();
		$lapsos->set_table("lapsos");

		$lapsos->order_by("lapsoid");
		
		$lapsos->display_as("lapnombreva","Nombre");
		$lapsos->display_as("lapestatusen","Estatus");

		$lapsos->display_as("lapnombreva","Nombre","required");
		$lapsos->display_as("lapestatusen","Estatus","required");

		$lapsos->unset_read();
		$lapsos->unset_delete();
		$lapsos->unset_add();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$lapsos->unset_edit();
			
		}


		$data = $lapsos->render();
		$this->ver($data,$titulo);
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}