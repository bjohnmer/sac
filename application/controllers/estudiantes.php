<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class estudiantes extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Estudiantes";
		$estudiantes = new grocery_CRUD();
		$estudiantes->set_table("estudiantes");
		$estudiantes->set_relation("representante","representantes","{repcedulava} - {repnombreva} {repapellidova}");
		$estudiantes->display_as("estcedulava","Cédula");
		$estudiantes->display_as("estnombreva","Nombre");
		$estudiantes->display_as("estapellidova","Apellido");
		$estudiantes->display_as("estfechanacda","Fecha de Nacimiento");
		$estudiantes->display_as("estsexoen","Género");
		$estudiantes->display_as("estdireccionva","Dirección");
		$estudiantes->display_as("esttelefonova","Teléfono");

		$estudiantes->set_rules("estcedulava","Cédula","required");
		$estudiantes->set_rules("estnombreva","Nombre","required");
		$estudiantes->set_rules("estapellidova","Apellido","required");
		$estudiantes->set_rules("estfechanacda","Fecha de Nacimiento","required");
		$estudiantes->set_rules("estsexoen","Género","required");
		$estudiantes->set_rules("estdireccionva","Dirección","required");
		$estudiantes->set_rules("esttelefonova","Teléfono","required");
		$estudiantes->set_rules("representante","Representante","required");
		$estudiantes->callback_field('estfechanacda',array($this,'_cambiar_objeto'));
		$estudiantes->callback_before_delete(array($this,'_before_delete'));
		$estudiantes->unset_read();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			$estudiantes->unset_delete();
			$estudiantes->unset_edit();
		}
		if ($this->session->userdata('admnivelen') == "Jefe de Seccional") 
		{
			
			$estudiantes->unset_delete();

		}
		
		$data = $estudiantes->render();
		$this->ver($data,$titulo);
	}

	function _before_delete( $primary_key )
	{
		
		$tieneSecciones = $this->db
												->where('estudiante', $primary_key)
												->get('notas')
												->num_rows();

		if ( $tieneSecciones == 0 ) 
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function _cambiar_objeto()
	{
		
		return '<input id="field-estfechanacda" class="fecha datepicker-input" type="text" maxlength="10" value="" name="estfechanacda"><a class="datepicker-input-clear" tabindex="-1">Resetear</a>(dd/mm/yyyy)';
	}


	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
	
}