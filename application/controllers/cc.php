<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cc extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// print_r( $this->session->all_userdata() );
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/cc',$titulo);
		$this->load->view('commons/footer');
	}

	public function index()
	{
		$usuarios = new grocery_CRUD();
		$usuarios->set_table("administrador");
		$usuarios->set_subject("Usuario");


		$usuarios->display_as("admloginva","Login");
		$usuarios->display_as("admclaveva","Clave");
		$usuarios->display_as("admclavecva","Confirmar Clave");
		
		$usuarios->field_type("admclaveva","password");
		$usuarios->field_type("admclavecva","password");
		
		$usuarios->unset_fields("admloginva","admnombreva","admnivelen", "admestatusen", "seccional_id");
		
		$usuarios->unset_list();
		$usuarios->unset_read();
		$usuarios->unset_add();
		$usuarios->unset_delete();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$usuarios->unset_edit();
			
		}

		$usuarios->unset_back_to_list();

		$usuarios->set_rules("admloginva","Login","required|min_length[5]|max_length[15]");
		$usuarios->set_rules("admclaveva","Clave","required|min_length[4]|max_length[20]|matches[admclavecva]");
		$usuarios->set_rules("admclavecva","Confirmar Clave","required|min_length[4]|max_length[20]|matches[admclaveva]");
		$usuarios->callback_before_update(array($this,'_encrypt_confirmar_callback'));

		$usuarios->callback_edit_field('admclaveva',array($this,'vaciar_campo_clave'));
		$usuarios->callback_edit_field('admclavecva',array($this,'vaciar_campo_confirmar'));

		$data = $usuarios->render();
		

		$titulo['titulo'] = "Cambiar Clave de Usuario";
		
		$this->ver($data,$titulo);
	}

	function _encrypt_confirmar_callback($post_array, $primary_key = null)
	{
	    
        $this->load->library("encrypt");
        $post_array['admclaveva'] = $this->encrypt->encode($post_array['admclaveva']);
        $post_array['admclavecva'] = $post_array['admclaveva'];
    
    	return $post_array;
	
	}

	function vaciar_campo_clave($post_array, $primary_key = null)
	{
	    
	    return '<input type="password" maxlength="50" name="admclaveva" id="field-admclaveva">';
	
	}

	function vaciar_campo_confirmar($post_array, $primary_key = null)
	{
	    
	    return '<input type="password" maxlength="50" name="admclavecva" id="field-admclavecva">';
	
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}

}