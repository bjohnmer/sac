<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class seccionales extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null, $vista = "contenido")
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$vista,$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Seccionales";
		$seccionales = new grocery_CRUD();
		$seccionales->set_table("seccionales");

		$seccionales->columns("id","descripcion");
		$seccionales->order_by("id");
		
		$seccionales->display_as("descripcion","Descripción");

		$seccionales->set_rules("descripcion","Descripción","required");

		$seccionales->unset_texteditor("descripcion");

		if ($this->session->userdata('admnivelen') == "Administrador") 
		{
			
			$seccionales->add_action('Asignar Secciones', '', 'seccionales/asignar','glyphicon glyphicon-check');

		}

		$seccionales->unset_read();

		$seccionales->callback_before_delete(array($this,'_before_delete'));
		
		if ($this->session->userdata('admnivelen') != "Administrador") 
		{
			
			$seccionales->unset_add();
			$seccionales->unset_edit();
			$seccionales->unset_delete();
			
		}

		$data = $seccionales->render();
		$this->ver($data,$titulo);
	}
	
	function _before_delete( $primary_key )
	{
		
		$tieneSecciones = $this->db
												->where('seccional_id', $primary_key)
												->get('secciones')
												->num_rows();

		if ( $tieneSecciones == 0 ) 
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function asignar($seccional_id = null)
	{
		$this->load->library("form_validation");

		if ( empty( $seccional_id ) ) {
			$seccional_id =$this->uri->segment(3);
		}

	    $seccional = $this->db
	    				->where("id",$seccional_id)
	    				->get('seccionales');

	    if (! ( $seccional->num_rows() == 1 ) ) 
	    {
	    	$this->session->set_flashdata('clase', 'danger');
	    	$this->session->set_flashdata('mensaje', 'La seccional no se encuentra');
	    	redirect("seccionales");
	    }

	    $titulo['titulo'] = "Asignar Secciones a la Seccional : " .$seccional->row(0)->descripcion;

	    $secciones = $this->db
							->where("secestatusen", "Activo")
							->where("seccodigova <>", "Traslado")
							->where("( seccional_id = 0 OR seccional_id = '".$seccional_id."' )", false, false)
    					->where( "periodo" , $this->session->userdata('perid') )
    					->order_by( "seccodanoescolarva, seccodigova" , "ASC" )
    					->get( "secciones" );
	    
	    if (! ( $secciones->num_rows() > 0 ) ) 
	    {
	    	$this->session->set_flashdata('clase', 'danger');
	    	$this->session->set_flashdata('mensaje', 'No hay secciones para asignar');
	    	redirect("seccionales");
	    }
		
		$anos = $this->db
					->where("anoestatusen", "Activo")
					->order_by( "anoid" , "ASC" )
					->get( "anosescolares" );
	    
	    if (! ( $anos->num_rows() > 0 ) ) 
	    {
	    	$this->session->set_flashdata('clase', 'danger');
	    	$this->session->set_flashdata('mensaje', 'No hay años escolares para asignar');
	    	redirect("seccionales");
	    }

	    $data['secciones'] = $secciones->result();
	    $data['anos_escolares'] = $anos->result();
	    $data['seccional'] = $seccional->row(0);

	    $this->ver($data , $titulo, "asignarSecciones");
	}
	
	public function storeAsec()
	{

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' )   
		{
			$post = $this->input->post();

			
			$this->form_validation->set_rules("secciones","las Secciones que van a ser asignadas","required");
			$this->form_validation->set_rules("seccional","Seccional","required|is_numeric|integer|is_natural_no_zero");
			if ( $this->form_validation->run() ) 
			{
				$band = true;
					echo "<pre>";
										print_r($post['secciones']);
										echo "</pre>";					
				$data = array(
						'seccional_id' => (int)$post['seccional'], 
					);

				$this->db
							->where("seccional_id",$post['seccional'])
							->update("secciones", array( 'seccional_id' => 0 ) );

				foreach ($post['secciones'] as $anoescolar => $seccion) 
				{

					foreach ($seccion as $s)
					{
						
						$x =	$this->db->where("secid",(int)$s)->update("secciones",$data);

						if (! $x)
						{

							$band = false;
						}
					}
				}
				if ($band) 
				{

					$this->session->set_flashdata('clase', 'success');
					$this->session->set_flashdata('mensaje', 'Todas las secciones fueron asignadas correctamente');
					redirect("seccionales");
				}
				else
				{
					
					$this->session->set_flashdata('clase', 'danger');
					$this->session->set_flashdata('mensaje', 'No se pudo asignar las secciones');
					redirect("seccionales");
				}

			}
			else
			{
				$this->asignar( $post['seccional'] );
			}
		}
		else
		{
			
			$this->session->set_flashdata('clase', 'danger');
			$this->session->set_flashdata('mensaje', 'No tiene permiso para acceder a la página especificada');
			redirect("seccionales");
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}