<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class inscripcion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null, $vista = "contenido")
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$vista,$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		
		if ($this->session->userdata('admnivelen') != "Administrador") 
		{
			$haySeccionales = $this->db
														->where('seccional_id <>','0')
														->get('secciones')
														->num_rows();

			if ( $haySeccionales == 0 )
			{
				
				$data =
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'Usted no puede inscribir porque no hay secciones asignadas',
							'seccionales' => true
						)
					);

			}

			$hayUnidades = $this->db
														->get('unidadescurriculares')
														->num_rows();

			if ( $hayUnidades == 0 )
			{
				
				$data =
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'No hay unidades curriculares creadas',
							'seccionales' => true
						)
					);

			}

			if ( $this->session->userdata('seccional_id') == 0 )
			{
				$data =
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'Usted no puede inscribir porque no tiene asignada una seccional',
							'seccionales' => true
						)
					);
			}
		}



		$p = $this->db
								->where("perestatusen","Activo")
								->get('periodos')
								->row(0);
		$data['periodo'] = $p->perid;
		$titulo['titulo'] = "Inscribir Estudiante";
		$this->ver($data,$titulo,"forminscripcion");
		
	}
	public function seleccionarDatos($data = null)
	{
		
		$existeEstudiante = $this->db
								->where('estcedulava',$this->input->post('cedula'))
								->join('representantes','representantes.repid = estudiantes.representante')
								->get('estudiantes')
								->result();
		
		if ( $existeEstudiante ) 
		{
			
			$notas = $this->db
											->where('estudiante',$existeEstudiante[0]->estcodigodo);
											
			$nuevoIngreso = $notas->get('notas')->num_rows();

			if ( $nuevoIngreso == 0 ) 
			{

				$secciones = $this->db
										->where('seccional_id',$this->session->userdata('seccional_id'))
										->where('seccodanoescolarva', '1')
										->order_by('secid', 'DESC')
										->get('secciones')
										->num_rows();

				if ( $secciones == 0 ) 
				{

					$enviarMensaje =
						
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'Usted no puede inscribir a este estudiante porque no pertenece a la seccional que corresponde'
						)
					);

					$this->index($enviarMensaje);
					return false;
				}
			}
			else
			{
				$unidadesAplazadas = $notas
															->where('periodo', (int)$this->session->userdata('perid') - 1 )
															->where('estudiante', $existeEstudiante[0]->estcodigodo )
															->where('notad <', 10 )
															->order_by('notaid', 'DESC')
															->get('notas')
															->num_rows();

				$paramcmr = $this->db
								->get('parametros')
								->row(0);

				$notasEstudiante = $notas
															->where('periodo', (int)$this->session->userdata('perid') - 1 )
															->order_by('notaid', 'DESC')
															->get('notas')
															->result();

				if ( $unidadesAplazadas >= $paramcmr->paramcmr ) 
				{
					
					$aei = $notasEstudiante[0]->anoescolar; // Año escolar a inscribir	
				}
				else
				{

					$aei = $notasEstudiante[0]->anoescolar + 1;	// Año escolar a inscribir	
				}

				$secciones = $this->db
										->where('seccional_id',$this->session->userdata('seccional_id'))
										->where('seccodanoescolarva', $aei)
										->get('secciones')
										->num_rows();

				if ( $secciones == 0 ) 
				{

					$enviarMensaje =
						
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'Usted no puede inscribir a este estudiante porque no pertenece a la seccional que corresponde'
						)
					);

					$this->index($enviarMensaje);
					return false;
				}
			}
		}

		if (empty($data)) {
			
			$this->form_validation->set_rules('cedula','Cédula del Estudiante','required|max_length[15]|min_length[4]|trim|numeric|integer');
			
			if ($this->form_validation->run()) 
			{

				$existeEstudiante = $this->db
										->where('estcedulava',$this->input->post('cedula'))
										->join('representantes','representantes.repid = estudiantes.representante')
										->get('estudiantes')
										->result();
				if ($existeEstudiante)
				{
					
					$estaInscrito = $this->db
										->where('estudiante',$existeEstudiante[0]->estcodigodo)
										->where('periodo',$this->input->post('periodo'))
										->get('notas')
										->num_rows();

					if ($estaInscrito == 0) 
					{
						$titulo['titulo'] = "Inscripción";
						
						$pactual = $this->db->where('perestatusen','Activo')->get('periodos')->row(0);
						$panterior = $pactual->perid - 1;

						$maxAe = $this->db
									->select('(anoescolar) as ae, seccion, estudiante')
									->where('estudiante',$existeEstudiante[0]->estcodigodo)
									->order_by('periodo','DESC')
									->order_by('anoescolar','DESC')
									->limit(1)
									->get('notas')
									->row(0);
						
						if (!empty($maxAe)) 
						{
							$AESiguiente = $maxAe->ae + 1;
							$seccioAn = $maxAe->seccion;
							$MateriasAplazadas = $this->db
												->where('estudiante',$existeEstudiante[0]->estcodigodo)
												->join('unidadescurriculares',' uniid = materia ')
												->where('anoescolar',$maxAe->ae)
												->where('notad <',10)
												->order_by('periodo','DESC')
												->get('notas')
												->result();

						}
						else
						{
							$AESiguiente = 1;
							$seccioAn = 0;
							$MateriasAplazadas = null;
						}

						$paramcmr = $this->db
										->get('parametros')
										->row(0);
						if ( count( $MateriasAplazadas ) >= $paramcmr->paramcmr  ) 
						{

							$AESiguiente = $maxAe->ae;
							$materiasAC = $MateriasAplazadas;
							$materiasAR = null;
						}
						else
						{
							$materiasAC = $this->db
												->where('unicodanoescolarva',$AESiguiente)
												->get('unidadescurriculares')
												->result();
							if ( count( $MateriasAplazadas ) != 0 ) {
								
								$materiasAR = $MateriasAplazadas;

							}
							else
							{
								$materiasAR = null;
							}

						}

						$secciones = $this->db
										->where('seccional_id', $this->session->userdata('seccional_id') )
										->where('periodo',$this->session->userdata('perid'))
										->where('seccodigova <>',"Traslado")
										->where('secestatusen <>',"Inactivo")
										->order_by('seccodigova', 'ASC')
										->get('secciones')
										->row(0);
						$data['materiasAC'] = $materiasAC;
						$data['materiasAR'] = $materiasAR;
						$an = $this->db->where('anocodigova',$AESiguiente)->get('anosescolares')->row(0);
						$data['nanoescolar'] = $an->anonombreva;
						$data['anoescolar'] = $AESiguiente;
						$data['seccionAnterior'] = $secciones->secid;
						$data['estudiante'] = $existeEstudiante[0];

						$this->ver($data,$titulo,"vinscripcion");
					}
					else
					{
						$enviarMensaje =
					
						array('errores' => 
							array(
								'clase' => 'danger', 
								'mensaje_error' => 'El Estudiante <strong>'.$existeEstudiante[0]->estnombreva.' '.$existeEstudiante[0]->estapellidova.'</strong> ya está inscrito en éste periodo'
							)
						);

						$this->index($enviarMensaje);
					}
				}
				else
				{

					$enviarMensaje =
					
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'El Estudiante no se encuentra registrado en el sistema<br><a href="'.base_url().'estudiantes">Registrar Nuevo Estudiante</a>'
						)
					);

					$this->index($enviarMensaje);

				}
			}
			else
			{
				$this->index();
			}
		} 
		else {

			$existeEstudiante = $this->db
									->where('estcedulava',$data['cedula'])
									->join('representantes','representantes.repid = estudiantes.representante')
									->get('estudiantes')
									->result();
			if ($existeEstudiante)
			{
				
				$estaInscrito = $this->db
									->where('estudiante',$existeEstudiante[0]->estcodigodo)
									->where('periodo',$data['periodo'])
									->get('notas')
									->num_rows();

				if ($estaInscrito == 0) 
				{
				
					$titulo['titulo'] = "Inscripción";
					
					$pactual = $this->db->where('perestatusen','Activo')->get('periodos')->row(0);
					$panterior = $pactual->perid - 1;

					$maxAe = $this->db
								->select('(anoescolar) as ae, seccion, estudiante')
								->where('estudiante',$existeEstudiante[0]->estcodigodo)
								->order_by('periodo','DESC')
								->order_by('anoescolar','DESC')
								->limit(1)
								->get('notas')
								->row(0);
					

					/* ---------------------------------------------- */

					if (!empty($maxAe)) 
					{

						$secciones = $this->db
												->where('seccional_id',$this->session->userdata('seccional_id'))
												->where('seccodanoescolarva', $aei)
												->get('secciones')
												->row(0);


						$AESiguiente = $maxAe->ae + 1;

						$seccioAn = $secciones->secid;

						$MateriasAplazadas = $this->db
											->where('estudiante',$existeEstudiante[0]->estcodigodo)
											->join('unidadescurriculares',' uniid = materia ')
											->where('anoescolar',$maxAe->ae)
											->where('notad <',10)
											->order_by('periodo','DESC')
											->get('notas')
											->result();

					}
					else
					{
						$AESiguiente = 1;
						$seccioAn = 0;
						$MateriasAplazadas = null;
					}
				
					$paramcmr = $this->db
									->get('parametros')
									->row(0);
					
					if ( count( $MateriasAplazadas ) >= $paramcmr->paramcmr  ) 
					{
						$AESiguiente = $maxAe->ae;
						$materiasAC = $MateriasAplazadas;
						$materiasAR = null;

					}
					else
					{
						$materiasAC = $this->db
											->where('unicodanoescolarva',$AESiguiente)
											->get('unidadescurriculares')
											->result();
						if ( count( $MateriasAplazadas ) != 0 ) {
							
							$materiasAR = $MateriasAplazadas;

						}
						else
						{
							$materiasAR = null;
						}

					}

					$data['materiasAC'] = $materiasAC;
					$data['materiasAR'] = $materiasAR;
					$an = $this->db->where('anocodigova',$AESiguiente)->get('anosescolares')->row(0);
					$data['nanoescolar'] = $an->anonombreva;
					$data['anoescolar'] = $AESiguiente;
					$data['seccionAnterior'] = $seccioAn;
					$data['estudiante'] = $existeEstudiante[0];

					$this->ver($data,$titulo,"vinscripcion");
				}
				else
				{
					$enviarMensaje =
				
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'El Estudiante <strong>'.$existeEstudiante[0]->estnombreva.' '.$existeEstudiante[0]->estapellidova.'</strong> ya está inscrito en éste periodo'
						)
					);

					$this->index($enviarMensaje);
				}
			}
			else
			{

				$enviarMensaje =
				
				array('errores' => 
					array(
						'clase' => 'danger', 
						'mensaje_error' => 'El Estudiante no se encuentra registrado en el sistema<br><a href="'.base_url().'estudiantes">Registrar Nuevo Estudiante</a>'
					)
				);

				$this->index($enviarMensaje);

			}
		}

	}

	public function procesarInscripcion()
	{

		$this->form_validation->set_rules('cedula','Cédula del Estudiante','required|max_length[15]|min_length[4]|trim|numeric|integer');

		$this->form_validation->set_rules('estudiante','Estudiante','required');
		$this->form_validation->set_rules('periodo','Periodo','required');
		$this->form_validation->set_rules('foto_estudiante','Recaudo: Foto del Estudiante','required');
		$this->form_validation->set_rules('copiac_representante','Recaudo: Copia de Cédula del Representante','required');
		if ( $this->form_validation->run() )
		{

			/**
			 * Se consultan los parámetros
			 */
			$parametros = $this->db
							->get('parametros')
							->row(0);

			/**
			 * Se verifica que el estudiante no esté inscrito
			 * ya en el periodo actual
			 */
			$estaInscrito = $this->db
											->where('estudiante',$this->input->post('estudiante'))
											->where('periodo',$this->input->post('periodo'))
											->get('notas')
											->num_rows();

			if ($estaInscrito == 0) 
			{
				/**
				 * Se pregunta si el estudiante tiene registro de notas anteriores
				 * para ver si es un estudiante regular o nuevo ingreso (Primer Año)
				 */
				$notas = $this->db
									->where( 'estudiante' , $this->input->post('estudiante') )
									->order_by( 'notaid' , 'DESC' )
									->get('notas');
				$tieneNotas = $notas->num_rows();
				
				if ( $tieneNotas == 0 )
				{

					/**
					 * Se define el Año Escolar a Inscribir
					 */
					$aeAI = 1;

					/**
					 * Es un ingreso para primer año, se
					 * debe verificar si el usuario tiene 
					 * seccionales con secciones de primer año
					 */
					if ( $this->session->userdata('admnivelen') == 'Administrador' )
					{
						
						/**
						 * Si el usuario es administrador puede inscribirlo 
						 * en primer año a partir de la sección A siempre y 
						 * cuando haya cupo
						 */
						$SeccionCCupo = $this->db
														->select('*, ( 
																					SELECT count( DISTINCT(n.estudiante) ) 
																					FROM notas as n 
																					WHERE n.periodo =  '.$this->session->userdata('perid').' 
																					AND n.seccion = s.secid	
																				) as total')
														->where( 'seccodanoescolarva' , $aeAI )
														->where( 'seccodigova <>',"Traslado")
														->order_by( 'total DESC, seccodanoescolarva ASC, secid ASC, seccodigova ASC' )
														->get( 'secciones as s' );

						/**
						 * Primero si hay secciones de primer año
						 */
						$haySecciones = $SeccionCCupo->num_rows();
						if ( $haySecciones > 0 ) 
						{
							
							/**
							 * Como hay secciones, se procede a verificar la que tenga cupo
							 * para inscribir ahí el estudiante
							 */
							$puedeInscribir = false;
							foreach ( $SeccionCCupo->result() as $value ) 
							{
								if ( $value->total <  $parametros->paramlimite )
								{
									
									$seccionAI = $value->secid; // Sección a Inscribir
									
									$puedeInscribir = true;
									break;
								}
							}


							if (! $puedeInscribir ) 
							{

								/**
								 * El usuario no tiene secciones con cupo
								 */
								$enviarMensaje =			
								array('errores' => 
									array(
										'clase' => 'danger', 
										'mensaje_error' => 'Usted no posee secciones con cupo para inscribir'
									)
								);

								$this->index($enviarMensaje);
								
								return false;
							}
						}
						else
						{
							
							/**
							 * No hay secciones de primer año para inscribir
							 */
							$enviarMensaje =			
							array('errores' => 
								array(
									'clase' => 'danger', 
									'mensaje_error' => 'Usted no posee secciones en primer año para inscribir'
								)
							);

							$this->index($enviarMensaje);
							return false;
						}

					}
					else
					{
						
						/**
						 * Si el usuario no es Administrador hay que
						 * buscar las secciones de primer año que puede
						 * inscribir el usuario
						 */
						$seccionesPrimerAno = $this->db
																		->where( 'seccional_id' , $this->session->userdata('seccional_id') )
																		->where( 'seccodanoescolarva' , $aeAI  )
																		->where( 'seccodigova <>' , 'Traslado' )
																		->get( 'secciones' );

						$haySecciones = $seccionesPrimerAno->num_rows();

						if ( $haySecciones > 0 ) 
						{
							
							/**
							 * Hay que buscar la sección que puede inscribir el usuario
							 * encontrando la que tiene cupo
							 */
							$SeccionCCupo = $this->db
															->select('*, ( 
																						SELECT count( DISTINCT(n.estudiante) ) 
																						FROM notas as n 
																						WHERE n.periodo =  '.$this->session->userdata('perid').' 
																						AND n.seccion = s.secid	
																					) as total')
															->where( 'seccional_id' , $this->session->userdata('seccional_id') )
															->where( 'seccodanoescolarva' , $aeAI )
															->where( 'seccodigova <>',"Traslado")
															->order_by( 'total DESC, seccodanoescolarva ASC, secid ASC, seccodigova ASC' )
															->get( 'secciones as s' );

							/**
							 * Primero si hay secciones de primer año
							 */
							$haySecciones = $SeccionCCupo->num_rows();
							if ( $haySecciones > 0 ) 
							{
								
								/**
								 * Como hay secciones, se procede a verificar la que tenga cupo
								 * para inscribir ahí el estudiante
								 */
								$puedeInscribir = false;
								foreach ( $SeccionCCupo->result() as $value ) 
								{
									if ( $value->total <  $parametros->paramlimite )
									{
										
										$seccionAI = $value->secid; // Sección a Inscribir
										
										$puedeInscribir = true;
										break;
									}
								}

								if (! $puedeInscribir ) 
								{

									/**
									 * El usuario no tiene secciones con cupo
									 */
									$enviarMensaje =			
									array('errores' => 
										array(
											'clase' => 'danger', 
											'mensaje_error' => 'Usted no posee secciones con cupo para inscribir'
										)
									);

									$this->index($enviarMensaje);
									
									return false;
								}
							}
							else
							{
								
								/**
								 * No hay secciones de primer año para inscribir
								 */
								$enviarMensaje =			
								array('errores' => 
									array(
										'clase' => 'danger', 
										'mensaje_error' => 'Usted no posee secciones en primer año para inscribir'
									)
								);

								$this->index($enviarMensaje);
								return false;
							}
						}
					}
				}
				else
				{
					/**
					 * Nos consultamos el último registro de notas para conocer
					 * el último año escolar cursado y asignar el siguiente
					 */
					$ultRegNotas = $notas->row(0);

					/**
					 * Se consiguen las unidades aplazadas para saber en cual
					 * año escolar va a ser inscrito
					 */

					$hayUnidadesAplazadas = $this->db
																		->where( 'estudiante' , $this->input->post( 'estudiante' ) )
																		->where( 'periodo' , ( (int)$ultRegNotas->periodo ) )
																		->where( 'notad <' , '10' )
																		->get('notas')
																		->num_rows();
																		
					if ( $hayUnidadesAplazadas >= $parametros->paramcmr ) 
					{
						
						/**
						 * Se va a inscribir en el mismo año escolar 
						 * anterior porque es repitiente
						 */
						$aeAI = (int)$ultRegNotas->anoescolar;
					}
					else
					{

						/**
						 * El estudiante pasó para un siguiente
						 * año escolar
						 */
						$aeAI = (int)$ultRegNotas->anoescolar + 1;
					}
					
					/**
					 * Como es una inscripción a un estudiante regular
					 * se debe verificar si el usuario tiene 
					 * seccionales con secciones del año escolar a inscribir
					 */
					if ( $this->session->userdata('admnivelen') == 'Administrador' )
					{

						/**
						 * Si el usuario es administrador puede inscribirlo 
						 * en el año escolar que le corresponde,
						 * a partir de la sección A
						 */
						$SeccionCCupo = $this->db
														->select('*, ( 
																					SELECT count( DISTINCT(n.estudiante) ) 
																					FROM notas as n 
																					WHERE n.periodo =  '.$this->session->userdata('perid').' 
																					AND n.seccion = s.secid	
																				) as total')
														->where( 'seccodanoescolarva' , $aeAI )
														->where( 'seccodigova <>',"Traslado")
														->order_by( 'total DESC, seccodanoescolarva ASC, secid ASC, seccodigova ASC' )
														->get( 'secciones as s' );

						/**
						 * Primero si hay secciones del Año escolar a inscribir
						 */
						$haySecciones = $SeccionCCupo->num_rows();
						if ( $haySecciones > 0 ) 
						{
							
							/**
							 * Como hay secciones, se procede a verificar la que tenga cupo
							 * para inscribir ahí el estudiante
							 */
							$puedeInscribir = false;
							foreach ( $SeccionCCupo->result() as $value ) 
							{
								if ( $value->total <  $parametros->paramlimite )
								{
									
									$seccionAI = $value->secid; // Sección a Inscribir
									
									$puedeInscribir = true;
									break;
								}
							}

							if (! $puedeInscribir ) 
							{

								/**
								 * El usuario no tiene secciones con cupo
								 */
								$enviarMensaje =			
								array('errores' => 
									array(
										'clase' => 'danger', 
										'mensaje_error' => 'No hay secciones con cupo para inscribir'
									)
								);

								$this->index($enviarMensaje);
								
								return false;
							}
						}
						else
						{
							
							/**
							 * No hay secciones para inscribir
							 */
							$enviarMensaje =			
							array('errores' => 
								array(
									'clase' => 'danger', 
									'mensaje_error' => 'No hay secciones en el año escolar para inscribir'
								)
							);

							$this->index($enviarMensaje);
							return false;
						}
					}
					else
					{

						/**
						 * Una vez que tenemos el Año escolar a inscribir debemos revisar si
						 * el usuario puede inscribir al estudiante segun las secciones 
						 * que tenga asignada su seccional
						 */
						$SeccionCCupo = $this->db
														->select('*, ( 
																					SELECT count( DISTINCT(n.estudiante) ) 
																					FROM notas as n 
																					WHERE n.periodo =  '.$this->session->userdata('perid').' 
																					AND n.seccion = s.secid	
																				) as total')
														->where( 'seccional_id' , $this->session->userdata('seccional_id') )
														->where( 'seccodanoescolarva' , $aeAI )
														->where( 'seccodigova <>',"Traslado")
														->order_by( 'total DESC, seccodanoescolarva ASC, secid ASC, seccodigova ASC' )
														->get( 'secciones as s' );

						/**
						 * Primero si hay secciones del Año escolar a inscribir
						 */
						$haySecciones = $SeccionCCupo->num_rows();
						if ( $haySecciones > 0 ) 
						{
							
							/**
							 * Como hay secciones, se procede a verificar la que tenga cupo
							 * para inscribir ahí el estudiante
							 */
							$puedeInscribir = false;
							foreach ( $SeccionCCupo->result() as $value ) 
							{
								if ( $value->total <  $parametros->paramlimite )
								{
									
									$seccionAI = $value->secid; // Sección a Inscribir
									
									$puedeInscribir = true;
									break;
								}
							}

							if (! $puedeInscribir ) 
							{

								/**
								 * El usuario no tiene secciones con cupo
								 */
								$enviarMensaje =			
								array('errores' => 
									array(
										'clase' => 'danger', 
										'mensaje_error' => 'Usted no posee secciones con cupo para inscribir'
									)
								);

								$this->index($enviarMensaje);
								
								return false;
							}
						}
						else
						{
							/**
							 * No hay secciones para inscribir
							 */
							$enviarMensaje =			
							array('errores' => 
								array(
									'clase' => 'danger', 
									'mensaje_error' => 'Usted no posee secciones en el año escolar para inscribir'
								)
							);

							$this->index($enviarMensaje);
							return false;
						}

					}
				}

				/**
				 * Se organizan los datos para ser almacenados
				 * en la tabla notas
				 */
				
				$materiasAC = $this->input->post('id_materiaAC');
				if (!empty($materiasAC))
				{
					$i = 0;
					foreach ($materiasAC as $materia)
					{
						$inscripcion[$i] = array(
											'estudiante' => $this->input->post('estudiante'),
											'periodo' => $this->input->post('periodo'),
											'seccion' => $seccionAI,
											'anoescolar' => $this->input->post('anoescolar'),
											'materia' => $materia
										);
						$i++;
					}
				}

				$materiasAR = $this->input->post('id_materiaAR');
				if (!empty($materiasAR))
				{
					foreach ($materiasAR as $materia)
					{
						$inscripcion[$i] = array(
											'estudiante' => $this->input->post('estudiante'),
											'periodo' => $this->input->post('periodo'),
											'seccion' => $seccionAI,
											'anoescolar' => (int)$this->input->post('anoescolar') - 1,
											'materia' => $materia
										);
						$i++;
					}
				}

				$recaudos = array(
						'estudiante_id' => $this->input->post('estudiante'),
						'periodo_id' => $this->session->userdata('perid'),
						'foto_estudiante' => $this->input->post('foto_estudiante'),
						'partidanac_estudiante' => $this->input->post('partidanac_estudiante'),
						'copiacedula_estudiante' => $this->input->post('copiacedula_estudiante'),
						'constanciabc_estudiante' => $this->input->post('constanciabc_estudiante'),
						'notasc_estudiante' => $this->input->post('notasc_estudiante'),
						'foto_representante' => $this->input->post('foto_representante'),
						'copiac_representante' => $this->input->post('copiac_representante')
					);
				
				$this->db->insert_batch('notas',$inscripcion);

				$this->db->insert('recaudos',$recaudos);

				$enviarMensaje =			
					array('errores' => 
						array(
							'clase' => 'success', 
							'mensaje_error' => 'El Estudiante ha sido inscrito satisfactoriamente'
						)
					);

				$this->index($enviarMensaje);
			}
			else
			{

				$enviarMensaje =
					array('errores' => 
						array(
							'clase' => 'danger', 
							'mensaje_error' => 'El Estudiante ya está inscrito en el periodo actual'
						)
					);

				$this->index($enviarMensaje);
			}
		}
		else
		{
			$this->seleccionarDatos(array('errorV' => true, 'cedula' => $this->input->post('cedula'), 'periodo' => $this->input->post('periodo')));
		}

	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}

}