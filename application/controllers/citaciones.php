<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class citaciones extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// print_r( $this->session->all_userdata() );
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{

		$titulo['titulo'] = "Citaciones Abiertas";
		$citaciones = new grocery_CRUD();
		$citaciones->set_table("citaciones");
		$citaciones->where('citestatusen','Abierta');
		
		$citaciones->order_by("citfechadate","DESC");
		$citaciones->unset_fields('citfechadate','fechacierre', 'observacion');
		$citaciones->columns('cittipodo', 'citidestudiantedo', 'citfechadate', 'citestatusen');

		$citaciones->unset_texteditor("citobservacionva");

		$rows = $this->db
						->select('estudiante')
						->where('periodo', $this->session->userdata('perid'))
						->distinct()
						->get('notas');

		$where = "";
		$x = $rows->num_rows();
		$i = 1;
		foreach ($rows->result() as $row) {
			$where .= 'estcodigodo = '. $row->estudiante;
			if ($i < $x) {
				$where .= ' OR ';
			}
			$i++;
		}
		$where .= ' AND ';

		$citaciones->set_relation("citidestudiantedo","estudiantes","{estcedulava} {estnombreva} {estapellidova}", array("$where" => TRUE));
		$citaciones->set_relation("cittipodo","tiposcitaciones","{tipnombreva}","tipnombreva <> 'Por Inasistencias'");
		
		$citaciones->display_as("citidestudiantedo","Estudiante");
		$citaciones->display_as("citobservacionva","Observación");
		$citaciones->display_as("cittipodo","Tipo");
		$citaciones->display_as("citfechadate","Fecha");
		$citaciones->display_as("citestatusen","Estatus");
		$citaciones->display_as("fechacierre","Fecha de Cierre");

		$citaciones->set_rules("citidestudiantedo","Estudiante","required");
		$citaciones->set_rules("citobservacionva","Observación","required");
		$citaciones->set_rules("cittipodo","Tipo","required");

		$citaciones->field_type('citestatusen','hidden','Abierta');
		$citaciones->unset_read();
		$citaciones->unset_delete();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$citaciones->unset_edit();
			
		}


		$citaciones->unset_read();
		
		if ($this->session->userdata('admnivelen') != "Operador") 
		{
			
			$citaciones->add_action('Imprimir Citación', '', 'citaciones/imprimir','glyphicon glyphicon-print');

		}

		$citaciones->add_action('Cerrar Citación', '', 'citaciones/cerrar','glyphicon glyphicon-remove');

		$data = $citaciones->render();
		$this->ver($data,$titulo);
	}

	public function cerradas($data = null)
	{
		
		$titulo['titulo'] = "Citaciones Cerradas";
		$citaciones = new grocery_CRUD();
		$citaciones->set_table("citaciones");
		$citaciones->where('citestatusen','Cerrada');
		
		$citaciones->order_by("citfechadate","DESC");
		$citaciones->unset_fields('citfechadate','observacion');
		$citaciones->unset_columns('citobservacionva', 'fechacierre');

		$citaciones->unset_texteditor("citobservacionva");
		$citaciones->set_relation("citidestudiantedo","estudiantes","{estcedulava} {estnombreva} {estapellidova}");
		$citaciones->set_relation("cittipodo","tiposcitaciones","{tipnombreva}","tipnombreva <> 'Por Inasistencias'");
		
		$citaciones->display_as("citidestudiantedo","Estudiante");
		$citaciones->display_as("citobservacionva","Observación");
		$citaciones->display_as("cittipodo","Tipo");
		$citaciones->display_as("citfechadate","Fecha");
		$citaciones->display_as("citestatusen","Estatus");
		$citaciones->display_as("fechacierre","Fecha de Cierre");

		$citaciones->set_rules("citidestudiantedo","Estudiante","required");
		$citaciones->set_rules("citobservacionva","Observación","required");
		$citaciones->set_rules("cittipodo","Tipo","required");

		$citaciones->field_type('citestatusen','hidden','Abierta');

		$citaciones->unset_read();
		$citaciones->unset_edit();
		$citaciones->unset_delete();

		$citaciones->unset_read();

		$data = $citaciones->render();
		$this->ver($data,$titulo);
	}

	public function cerrar()
	{
		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$this->logout();
			
		}

		$datos = array(
			'observacion' => $this->input->post('obser'), 
			'fechacierre' => date('Y-m-d'),
			'citestatusen' => 'Cerrada'
			);
		$salida = array('tipo' => 'error', 'msg' => 'Ha ocurrido un error al guardar');
		if($this->db->where('citcodigoin',$this->input->post('id'))->update('citaciones',$datos))
		{
			$salida = array('tipo' => 'ok', 'msg' => 'La Citación se ha cerrado con éxito');
		}
		echo json_encode($salida);

	}

	public function imprimir()
	{
		$data['citacion'] = $this->db
								->join('tiposcitaciones','citaciones.cittipodo = tiposcitaciones.tipcodigoti')
								->join('estudiantes','citaciones.citidestudiantedo = estudiantes.estcodigodo')
								->where('citcodigoin', $this->uri->segment(3))
								->get('citaciones')
								->row(0);

		$data['notas'] = $this->db
						->select('anosescolares.anonombreva, secciones.seccodigova')
						->where('notas.estudiante',$data['citacion']->estcodigodo)
						->where('notas.periodo',$this->session->userdata("perid"))
						->join('estudiantes','notas.estudiante = estudiantes.estcodigodo')
						->join('anosescolares','notas.anoescolar = anosescolares.anoid')
						->join('secciones','notas.seccion = secciones.secid')
						->get('notas')
						->row(0);

		$data['representante'] = $this->db
						->where('estudiantes.estcodigodo',$data['citacion']->estcodigodo)
						->join('representantes','representantes.repid = estudiantes.representante')
						->get('estudiantes')
						->row(0);

		$this->load->library('pdf');
		$this->pdf->load_view("dashboard/printCitacion",$data);
		$this->pdf->render();
		$this->pdf->stream('print_inscripcion.pdf');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}