<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class notas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	
	public function ver($data = null, $titulo = null, $vista = "contenido")
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$vista,$titulo);
		$this->load->view('commons/footer');
	}

	public function index($data = null)
	{

		$data['anosescolares'] = $this->db
									->order_by('anoid','ASC')
									->get('anosescolares')
									->result();

		$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
		$data['periodo'] = $p->perid;
		$data['secciones'] = 
			$this->db
				->where("seccodanoescolarva",$data['anosescolares'][0]->anoid)
				->where("seccodigova <>","TRASLADO")
				->get('secciones')
				->result();
		$data['materias'] = $this->db
				->where("unicodanoescolarva",$data['anosescolares'][0]->anoid)
				->where("uniestatusen","Activo")
				->get('unidadescurriculares')
				->result();
		$data['lapsos'] = $this->db
				->get('lapsos')
				->result();

		$titulo['titulo'] = "Notas";
		if (!empty($this->session->flashdata('clase'))) {
			$data['clase'] = $this->session->flashdata('clase');
		}
		if (!empty($this->session->flashdata('mensaje'))) {
			$data['mensaje'] = $this->session->flashdata('mensaje');
		}
		$this->ver($data,$titulo,"formfilnotas");
		
	}

	public function insertarNotas()
	{

		$ane = $this->input->post('anoescolar');
		$per = $this->session->userdata('perid');
		$mat = $this->input->post('materia');
		$sec = $this->input->post('seccion');
		$lap = $this->input->post('lapso');
		if (
			!empty($ane) &&
			!empty($per) &&
			!empty($mat) &&
			!empty($sec) &&
			!empty($lap) 
			) {
				$datos['anoescolar'] = $this->db
								->where('anoid',$ane)
								->get('anosescolares')
								->row(0);

				$datos['periodo'] = $this->db
								->where('perid',$per)
								->get('periodos')
								->row(0);

				$datos['materia'] = $this->db
								->where('uniid',$mat)
								->get('unidadescurriculares')
								->row(0);

				$datos['seccion'] = $this->db
								->where('secid',$sec)
								->get('secciones')
								->row(0);

				$datos['lapso'] = $this->db
								->where('lapsoid',$lap)
								->get('lapsos')
								->row(0);

				$datos['notas'] = $this->db
								->where('anoescolar',$ane)
								->where('periodo',$per)
								->where('materia',$mat)
								->where('seccion',$sec)
								// ->where('nota'.$this->input->post('lapso').' IS NULL',FALSE,FALSE)
								->join('estudiantes','notas.estudiante = estudiantes.estcodigodo')
								->get('notas')
								->result();

				if (empty($datos['notas'])) {
					$datos['mensaje'] = "No hay estudiantes registrados en la sección seleccionada";
					$datos['clase'] = "danger";
					$this->index($datos);
					return false;
				}

		} else {
			$datos['mensaje'] = "Falta información por seleccionar";
			$datos['clase'] = "danger";
			$this->index($datos);
			return false;
		}

		$titulo['titulo'] = "Notas";

		$this->ver($datos,$titulo,"VinsertarNotas");

	}
	
	public function procesarNotas()
	{



		$total = count($this->input->post('notas'));

		$arreglo = array();

		for ($i=0; $i < $total; $i++) { 

			switch ($this->input->post('lapso')) {
				case '1':
					$arreglo['nota1'] = $this->input->post('notas')[$i];
					break;
				case '2':
					$arreglo['nota2'] = $this->input->post('notas')[$i];
					break;
				case '3':
					$arreglo['nota3'] = $this->input->post('notas')[$i];

					$notas = $this->db
												->where('estudiante', $this->input->post('estudiantes')[$i])
												->where('periodo', $this->input->post('periodo'))
												->where('seccion', $this->input->post('seccion'))
												->where('anoescolar', $this->input->post('anoescolar'))
												->where('materia', $this->input->post('materia'))
												->get('notas')
												->row(0);
					$arreglo['notad'] = ( (float)$notas->nota1 + (float)$notas->nota2 + (float)$this->input->post('notas')[$i] )/3;
					break;
			}
			
			$arr = array(
					'estudiante' => $this->input->post('estudiantes')[$i],
					'periodo' => $this->input->post('periodo'),
					'seccion' => $this->input->post('seccion'),
					'anoescolar' => $this->input->post('anoescolar'),
					'materia' => $this->input->post('materia')
					);
			

			if ($this->session->userdata('admnivelen') == "Operador") 
			{
				
				$bus = $this->db
							->where($arr)
							->get("notas")
							->row(0);
				switch ($this->input->post('lapso')) {
					case '1':
						if (!empty($bus->nota1)) {
							$datos['clase'] = 'error';
							$datos['mensaje'] = 'Usted no tiene permisos para modificar notas';
							$this->session->set_flashdata('clase', 'success');
							$this->session->set_flashdata('mensaje', 'Se han actualizado las notas correctamente');
							redirect("notas");
							return;
						}
						break;
					case '2':
						if (!empty($bus->nota2)) {
							$datos['clase'] = 'error';
							$datos['mensaje'] = 'Usted no tiene permisos para modificar notas';
							$this->session->set_flashdata('clase', 'success');
							$this->session->set_flashdata('mensaje', 'Se han actualizado las notas correctamente');
							redirect("notas");					
							return;
						}
						break;
					case '3':
						if (!empty($bus->nota3)) {
							$datos['clase'] = 'error';
							$datos['mensaje'] = 'Usted no tiene permisos para modificar notas';
							$this->session->set_flashdata('clase', 'success');
							$this->session->set_flashdata('mensaje', 'Se han actualizado las notas correctamente');
							redirect("notas");
							return;
						}
						break;
				}
				
			}

			$this->db->update('notas', $arreglo, $arr);
			
			$datos['clase'] = 'success';
			$datos['mensaje'] = 'Se han actualizado las notas correctamente';
			$this->session->set_flashdata('clase', 'success');
			$this->session->set_flashdata('mensaje', 'Se han actualizado las notas correctamente');
			redirect("notas");

		}
	
	}
	
}