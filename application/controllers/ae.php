<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ae extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// print_r( $this->session->all_userdata() );
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Años Escolares";
		$ae = new grocery_CRUD();
		$ae->set_table("anosescolares");
		$ae->order_by("anoid");
		$ae->display_as("anocodigova","Código");
		$ae->display_as("anonombreva","Nombre");
		$ae->display_as("anoestatusen","Estatus");
		
		$ae->set_rules("anocodigova","Código","required");
		$ae->set_rules("anonombreva","Nombre","required");
		$ae->set_rules("anoestatusen","Estatus","required");

		$ae->unset_read();
		$ae->unset_add();
		$ae->unset_delete();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$ae->unset_edit();
			
		}


		$data = $ae->render();
		$this->ver($data,$titulo);
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}