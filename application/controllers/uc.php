<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class uc extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Unidades Curriculares";
		$uc = new grocery_CRUD();
		$uc->set_table("unidadescurriculares");
		$uc->set_relation("unicodanoescolarva","anosescolares","{anocodigova} {anonombreva}", "anosescolares.anoestatusen = 'Activo'");
		$uc->order_by('unicodanoescolarva ASC, uninombreva');
		$uc->display_as("unicodigova","Código");
		$uc->display_as("uninombreva","Nombre");
		$uc->display_as("unicodanoescolarva","Año Escolar");
		$uc->display_as("uniestatusen" ,"Estatus");
		
		$uc->callback_before_delete(array($this,'_before_delete'));

		$uc->field_type("unicodigova", "hidden",null);

		$uc->callback_before_insert(array($this,'_generar_codigo'));
		
		$uc->unset_read();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			$uc->unset_delete();
			$uc->unset_edit();
		}

		if ($this->session->userdata('admnivelen') == "Jefe de Seccional") 
		{
			
			$uc->unset_delete();

		}
		
		$data = $uc->render();
		$this->ver($data,$titulo);
	}

	function _before_delete( $primary_key )
	{
		
		$tieneSecciones = $this->db
												->where('materia', $primary_key)
												->get('notas')
												->num_rows();

		if ( $tieneSecciones == 0 ) 
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function _generar_codigo($post_array)
	{
		
		$codigo = substr($post_array['uninombreva'], 0, 6) . $post_array['unicodanoescolarva'];

		$hayUnCodIgual = $this->db->like('unicodigova', $codigo)->get('unidadescurriculares')->num_rows();

		if ($hayUnCodIgual > 0) {
			$codigo .= "-";
			$codigo .= $hayUnCodIgual + 1;
		} 		

		$post_array['unicodigova'] = $codigo;
		 
		return $post_array;
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}