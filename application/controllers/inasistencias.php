<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class inasistencias extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null, $vista = "contenido")
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$vista,$titulo);
		$this->load->view('commons/footer');
	}
	public function index($d = null)
	{
		
		$d['anosescolares'] = $this->db
									->order_by('anoid','ASC')
									->get('anosescolares')
									->result();

		$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
		$d['periodo'] = $p->perid;
		$d['secciones'] = 
			$this->db
				->where("seccodanoescolarva",$d['anosescolares'][0]->anoid)
				->where("seccodigova <>","TRASLADO")
				->get('secciones')
				->result();
		$d['materias'] = $this->db
				->where("unicodanoescolarva",$d['anosescolares'][0]->anoid)
				->where("uniestatusen","Activo")
				->get('unidadescurriculares')
				->result();
		$titulo['titulo'] = "Agregar Inasistencias";
		$this->ver($d,$titulo,"forminasistencias");
		
	}
	
	public function getSecciones()
	{
		$data = $this->db
				->where("seccodanoescolarva",$this->input->post("elegido"))
				->where("seccodigova <>","TRASLADO")
				->where("periodo",$this->session->userdata('perid'))
				->where("secestatusen","Activo")
				->get('secciones')
				->result();
		
		$opciones = "";

		if ($data) 
		{
			foreach ($data as $opcion) {
				$opciones .= "<option value='".$opcion->secid."'>".$opcion->seccodigova." </option> ";
			}
		}
		else
		{
			$opciones = '<option value="">No hay secciones</option>';
		}
		echo $opciones;
	}

	public function getMaterias()
	{
		$data = $this->db
				->where("unicodanoescolarva",$this->input->post("elegido"))
				->where("uniestatusen","Activo")
				->get('unidadescurriculares')
				->result();
		
		$opciones = "";

		if ($data) 
		{
			foreach ($data as $opcion) {
				$opciones .= "<option value='".$opcion->uniid."'>".$opcion->unicodigova." ". $opcion->uninombreva ." </option> ";
			}
		}
		else
		{
			$opciones = '<option value="">No hay Unidades Curriculares</option>';
		}
		echo $opciones;
	}

	public function insertarInasistencias()
	{
		$data['fecha'] = $this->input->post("fecha");
		$data['anoescolar'] = $this->db->where("anoid",$this->input->post("anoescolar"))->get('anosescolares')->row(0);
		$data['seccion'] = $this->db->where("secid",$this->input->post("seccion"))->get('secciones')->row(0);
		$data['periodo'] = $this->db->where("perid",$this->input->post("periodo"))->get('periodos')->row(0);
		$data['unidadescurriculares'] = $this->db->where("uniid",$this->input->post("materia"))->get("unidadescurriculares")->row(0);
		
		$titulo['titulo'] = 
			"<h3 class='text-center'>Ingresar Inasistencias</h3> <br> <strong>Unidad Curricular</strong>: "
			.$data['unidadescurriculares']->uninombreva
			." "
			."<br><strong>Año Escolar</strong>: "
			.$data['anoescolar']->anonombreva
			."&nbsp;&nbsp;<strong>Sección</strong>: "
			.$data['seccion']->seccodigova
			."&nbsp;&nbsp;<strong>Periodo</strong>: "
			.$data['periodo']->percodigova."<br><br>";

		$data['estudiantes'] = $this->db
									->join("estudiantes","notas.estudiante = estudiantes.estcodigodo")
									->where("anoescolar", $this->input->post("anoescolar"))
									->where("periodo", $this->input->post("periodo"))
									->where("seccion", $this->input->post("seccion"))
									->where("materia", $this->input->post("materia"))
									->where('estudiante NOT IN (SELECT inaidestudiantedo FROM inasistenciasestudiante WHERE inafechatt = CURDATE() AND perid = '.$this->input->post("periodo").' AND secid = '.$this->input->post("seccion").' AND anoid = '.$this->input->post("anoescolar").' AND  uniid = '.$this->input->post("materia").' )',false,false)
									->get("notas")
									->result();

		if (!empty($data['estudiantes'])) 
		{
			$this->ver($data,$titulo,"VinsertarInasistencias");
		}
		else
		{
			$m["clase"] = "danger";
			$m["mensaje"] = "No hay estudiantes en la sección seleccionada";
			$this->index($m);
		}

	}
	
	public function procesarInasistencias()
	{
		
		$inaidestudiantedo = $this->input->post("inaidestudiantedo");
		$inaobservacionva = $this->input->post("inaobservacionva");
		$top = count($inaidestudiantedo);
		$CitacionGenerada = false;
		foreach ($inaidestudiantedo as $key => $value) {
			$ini[] = $key;
			$cuentaIna = $this->db->where("inaidestudiantedo",$key)->from("inasistenciasestudiante")->count_all_results();
			
			$params = $this->db->get("parametros")->row(0);

			if ($cuentaIna == $params->paramtina) {
				$crearCitacion = array(
									"cittipodo" => 1,
									"citidestudiantedo" => $key,
									"citobservacionva" => "Citación Generada Automáticamente por concepto de Acumulación de inasistencias",
									"citfechadate"  => date("Y-m-d - H:i")
								);
				$this->db->insert("citaciones",$crearCitacion);
				$CitacionGenerada = true;
			}

		}
		foreach ($inaobservacionva as $key => $value) {
			$obs[] = $value;
		}

		$i = 0;
		do
		{
			$data = array(
						"perid" => $this->input->post("perid"),
					    "secid" => $this->input->post("secid"),
					    "anoid" => $this->input->post("anoid"),
					    "inafechatt" => $this->datemanager->date2mySQL($this->input->post("inafechatt")),
					    "uniid" => $this->input->post("uniid"),
					    "inaidestudiantedo" => $ini[$i],
					    "inaobservacionva" => $obs[$i]
					);
			$i++;
			$this->db->insert("inasistenciasestudiante",$data);
		}while($i < $top);
		
		if ($CitacionGenerada) {
			$data["clase"] = "info";
			$data["mensaje"] = "Se han generado Citaciones por Inasistencias <br> revise -> <a href='".base_url()."citaciones'>Citaciones Recientes</a> ";
		}

		$this->index($data);
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}