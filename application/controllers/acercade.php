<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acercade extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		
		$this->load->view('commons/header');
		$this->load->view('dashboard/acercade');
		$this->load->view('commons/footer');

	}
}