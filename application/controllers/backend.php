<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$v = $this->session->userdata('valida_sesion');
		if (!$v) 
		{
			$this->logout();	
		}
		$this->_generarDatos();
	}

	public function index()
	{
		$this->load->view('commons/header');
		$this->load->view('dashboard/contenido');
		$this->load->view('commons/footer');
	}
	
	public function _generarDatos()
	{
		$pers = $this->db->get("periodos")->result();
		if(empty($pers))
		{
			$param = $this->db->get("parametros")->row(0);
			if (!empty($param)) {
				$j=1;
				for ($i=(int)$param->paramperiodo; $i <= (int)date('Y') ; $i++) 
				{ 
					
					$datos['perid'] = $j;
					$p = $i;
					$p1 = $i + 1;
					$hoy = (int)date('Y') - 1;
					$datos['percodigova'] = $p."-".$p1;
					$datos['pernombreva'] = $datos['percodigova'];
					if ($i == $hoy) 
					{
						$pa['perid'] = $datos['perid'];
						$this->session->set_userdata('perid',$datos['perid']);
						$this->session->set_userdata('periodoActivo',$datos['percodigova']);

						$datos['perestatusen'] = "Activo";
					}
					else
					{
						$datos['perestatusen'] = "Inactivo";
					}			
					$this->db->insert("periodos",$datos);
					$j++;

				}
				$ae = $this->db->get("anosescolares")->result();
				if(!$ae)
				{
					$aes[] = array(
								'anoid' 		=> 1,
								'anocodigova' 	=> 1, 
								'anonombreva' 	=> 'Primero',
								'anoestatusen' 	=> 'Activo',
							);
					$aes[] = array(
								'anoid' 		=> 2,
								'anocodigova' 	=> 2, 
								'anonombreva' 	=> 'Segundo',
								'anoestatusen' 	=> 'Activo',
							);
					$aes[] = array(
								'anoid' 		=> 3,
								'anocodigova' 	=> 3, 
								'anonombreva' 	=> 'Tercero',
								'anoestatusen' 	=> 'Activo',
							);
					$aes[] = array(
								'anoid' 		=> 4,
								'anocodigova' 	=> 4, 
								'anonombreva' 	=> 'Cuarto',
								'anoestatusen' 	=> 'Activo',
							);
					$aes[] = array(
								'anoid' 		=> 5,
								'anocodigova' 	=> 5, 
								'anonombreva' 	=> 'Quinto',
								'anoestatusen' 	=> 'Activo',
							);
					$aes[] = array(
								'anoid' 		=> 6,
								'anocodigova' 	=> 6, 
								'anonombreva' 	=> 'Sexto',
								'anoestatusen' 	=> 'Inactivo',
							);

					$this->db->insert_batch("anosescolares",$aes);
				}

				$sec = $this->db->get("secciones")->result();
				if(!$sec)
				{ 
					

					$sec[] = array(
								'secid' 		=> 1,
								'seccodigova' 	=> 'Traslado', 
								'seccodanoescolarva' => 1,
								'periodo' => $pa['perid'],
								'secestatusen' 	=> 'Activo',
							);
					$sec[] = array(
								'secid' 		=> 2,
								'seccodigova' 	=> 'Traslado', 
								'seccodanoescolarva' => 2,
								'periodo' => $pa['perid'],
								'secestatusen' 	=> 'Activo',
							);
					$sec[] = array(
								'secid' 		=> 3,
								'seccodigova' 	=> 'Traslado', 
								'seccodanoescolarva' => 3,
								'periodo' => $pa['perid'],
								'secestatusen' 	=> 'Activo',
							);
					$sec[] = array(
								'secid' 		=> 4,
								'seccodigova' 	=> 'Traslado', 
								'seccodanoescolarva' => 4,
								'periodo' => $pa['perid'],
								'secestatusen' 	=> 'Activo',
							);
					$sec[] = array(
								'secid' 		=> 5,
								'seccodigova' 	=> 'Traslado', 
								'seccodanoescolarva' => 5,
								'periodo' => $pa['perid'],
								'secestatusen' 	=> 'Activo',
							);
					$sec[] = array(
								'secid' 		=> 6,
								'seccodigova' 	=> 'Traslado', 
								'seccodanoescolarva' => 6,
								'periodo' => $pa['perid'],
								'secestatusen' 	=> 'Inactivo',
							);

					$i=7;

					foreach (range('A', 'Z') as $letra) {
					    
						$sec[] = array(
									'secid' 		=>$i,
									'seccodigova' 	=> $letra, 
									'seccodanoescolarva' => 1,
									'periodo' => $pa['perid'],
									'secestatusen' 	=> 'Activo',
								);
						$i++;
						$sec[] = array(
									'secid' 		=>$i,
									'seccodigova' 	=> $letra, 
									'seccodanoescolarva' => 2,
									'periodo' => $pa['perid'],
									'secestatusen' 	=> 'Activo',
								);
						$i++;
						$sec[] = array(
									'secid' 		=>$i,
									'seccodigova' 	=> $letra, 
									'seccodanoescolarva' => 3,
									'periodo' => $pa['perid'],
									'secestatusen' 	=> 'Activo',
								);
						$i++;
						$sec[] = array(
									'secid' 		=> $i,
									'seccodigova' 	=> $letra, 
									'seccodanoescolarva' => 4,
									'periodo' => $pa['perid'],
									'secestatusen' 	=> 'Activo',
								);
						$i++;
						$sec[] = array(
									'secid' 		=> $i,
									'seccodigova' 	=> $letra, 
									'seccodanoescolarva' => 5,
									'periodo' => $pa['perid'],
									'secestatusen' 	=> 'Activo',
								);
						$i++;
						$sec[] = array(
									'secid' 		=> $i,
									'seccodigova' 	=> $letra, 
									'seccodanoescolarva' => 6,
									'periodo' => $pa['perid'],
									'secestatusen' 	=> 'Inactivo',
								);

						$i++;
					}

					$this->db->insert_batch("secciones",$sec);
				}
				
				$lap = $this->db->get("lapsos")->result();
				if(!$lap)
				{

					$lap[] = array(
								'lapsoid' 		=> 1,
								'lapnombreva' => 'Primero',
								'lapestatusen' 	=> 'Activo',
							);
					$lap[] = array(
								'lapsoid' 		=> 2,
								'lapnombreva' => 'Segundo',
								'lapestatusen' 	=> 'Activo',
							);
					$lap[] = array(
								'lapsoid' 		=> 3,
								'lapnombreva' => 'Tercero',
								'lapestatusen' 	=> 'Activo',
							);

					$this->db->insert_batch("lapsos",$lap);
				}

				$tc = $this->db->get("tiposcitaciones")->result();
				if(!$tc)
				{
				 
					$tc[] = array(
								'tipcodigoti' 		=> 1,
								'tipnombreva' => 'Por Inasistencias',
							);
					$tc[] = array(
								'tipcodigoti' 		=> 2,
								'tipnombreva' => 'Por Debilidad en el Comportamiento',
							);
					$tc[] = array(
								'tipcodigoti' 		=> 3,
								'tipnombreva' => 'Por bajo rendimiento escolar',
							);

					$tc[] = array(
								'tipcodigoti' 		=> 4,
								'tipnombreva' => 'Por reunión de Representantes',
							);

					$this->db->insert_batch("tiposcitaciones",$tc);
				}
				$seccionales = $this->db->get("seccionales")->result();
				if(!$seccionales)
				{ 
					$p = $this->db->get('parametros')->row(0);

					for ($i=1; $i <= $p->paramcs ; $i++) { 
						$seccionales[] = array(
									'id' 		=> $i,
									'descripcion' 	=> "Seccional $i", 
								);
					}

					$this->db->insert_batch("seccionales",$seccionales);
				}
			}
			else
			{
				echo "<script type='text/javascript'>
					alert('Los parametros del sistema No han sido inicializados completamente, Por favor llene los parámetros iniciales: Estudiantes por sección, Tope de Inasistencias, Periodo Escolar de Inicio, Cantidad de Seccionales y Cantidad de Unidades Curriculares para repetir');
					window.location = '".base_url()."parametros';
					</script>";
			}
			
		}
		else
		{
			$hoy = (int)date('Y');
			$pers2 = $this->db->order_by("perid","DESC")->limit(1)->get("periodos")->row(0);
			$p = substr($pers2->percodigova, -4);
			if ($hoy == $p) 
			{
				$p1 = (int)$p + 1;
				$datos['percodigova'] = $p."-".$p1;
				$datos['pernombreva'] = $datos['percodigova'];
				$datos['perestatusen'] = "Inactivo";
				$this->db->insert("periodos",$datos);
			}
		}
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}