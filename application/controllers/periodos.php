<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class periodos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Periodos Escolares";
		$periodos = new grocery_CRUD();
		$periodos->set_table("periodos");
		$periodos->order_by("perid","DESC");
		$periodos->display_as("percodigova","Código");
		$periodos->display_as("pernombreva","Nombre");
		$periodos->display_as("perestatusen","Estatus");

		$periodos->callback_before_update(array($this,'cambiaractivos'));

		$periodos->unset_read();
		$periodos->unset_delete();
		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$periodos->unset_edit();
			
		}


		$data = $periodos->render();
		$this->ver($data,$titulo);
	}
	public function cambiaractivos($post_array)
	{
		if ($post_array['perestatusen'] == 'Activo') {
			$datos = array('perestatusen' => 'Inactivo');
			$this->db->update("periodos",$datos);
			$this->session->set_userdata("periodoActivo",$post_array['percodigova']);
		}
		unset($post_array['percodigova']);
		unset($post_array['pernombreva']);
		return $post_array;
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}