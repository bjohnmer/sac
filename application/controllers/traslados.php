<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class traslados extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
	}
	public function ver($data = null, $titulo = null, $vista = "btraslado")
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$vista,$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
	
		$titulo['titulo'] = "Trasladar Estudiante";
		$data['anosescolares'] = $this->db->order_by("anoid")->get('anosescolares')->result();
		
		$data['secciones'] = 
			$this->db
				->where("seccodanoescolarva",$data['anosescolares'][0]->anoid)
				->where("seccodigova","TRASLADO")
				->get('secciones')
				->result();
		
		$pactual = $this->db->where('perestatusen','Activo')->get('periodos')->row(0);
		// $panterior = $this->db->where('perid',$pactual->id - 1)->get('periodos')->row(0);
		$panterior = $pactual->perid - 1;

		$data['periodos'] = 
			$this->db
				->where("perid <=",$panterior)
				->order_by('perid','DESC')
				->get("periodos")
				->result();
		
		$data['estudiantes'] = $this->db->where('`estcodigodo` NOT IN (SELECT `estudiante` FROM `notas`)', NULL, FALSE)->get("estudiantes")->result();
		
		$this->ver($data,$titulo);
	
	}
	public function insertarNotas()
	{
				
		$verif = $this->db
				->where( "estudiante", $this->input->post("estudiante") )
				->where( "periodo", $this->session->userdata("perid") )
				->where( "anoescolar", $this->input->post("anoescolar") )
				->get("notas");

		if ($verif->num_rows()) 
		{
			$this->index(array('clase'=>"danger",'mensaje' => "<strong>Este estudiante ya tiene notas ingresadas</strong><br>Usted debe ingresar sus notas por el menú <strong>Proceso > Notas</strong>"));
		}
		else
		{

			$data['anoescolar'] = $this->db->where("anoid",$this->input->post("anoescolar"))->get('anosescolares')->row(0);
			$data['seccion'] = $this->db->where("seccodanoescolarva",$this->input->post("anoescolar"))->where('seccodigova','Traslado')->get('secciones')->row(0);
			$data['periodo'] = $this->db->where("perid",$this->input->post("periodo"))->get('periodos')->row(0);
			$data['estudiante'] = $this->db->where("estcodigodo",$this->input->post("estudiante"))->get("estudiantes")->row(0);
			
			$titulo['titulo'] = 
				"<strong>Estudiante:</strong> "
				.$data['estudiante']->estcedulava
				." "
				.$data['estudiante']->estnombreva
				." "
				.$data['estudiante']->estapellidova
				."<br><strong>Año Escolar:</strong> "
				.$data['anoescolar']->anonombreva
				."&nbsp;&nbsp;<strong>Sección:</strong> "
				.$data['seccion']->seccodigova
				."&nbsp;&nbsp;<strong>Periodo:</strong> "
				.$data['periodo']->percodigova;

			$data['unidadescurriculares'] = $this->db->where("unicodanoescolarva <=", $this->input->post("anoescolar"))->where('uniestatusen','Activo')->get("unidadescurriculares")->result();
			$data['distribucion'] = $this->db
									->join('anosescolares','unidadescurriculares.unicodanoescolarva = anosescolares.anoid')
									->where("unicodanoescolarva <=", $this->input->post("anoescolar"))
									->where('uniestatusen','Activo')
									->select(' DISTINCT unicodanoescolarva, anonombreva, COUNT(unicodanoescolarva)',FALSE)
									->group_by('unicodanoescolarva')
									->get("unidadescurriculares")
									->result();
			$this->ver($data,$titulo,"VinsertarNotasT");
		}

	}
	public function procesarNotas()
	{
		
		$uae = $this->input->post('aes');
		$notas = $this->input->post("notas");


		$aeA = $this->db
						->select('uniid')
						->where('unicodanoescolarva <',$this->input->post('aes'))
						->get('unidadescurriculares')
						->result_array();

		foreach ($notas as $idmateria => $nota) {
			
			$ae = $this->db->where('uniid',$idmateria)
							->get('unidadescurriculares')
							->row(0);
			
			$se = $this->db
						->where('seccodanoescolarva',$ae->unicodanoescolarva)
						->where('seccodigova','Traslado')
						->get('secciones')
						->row(0);

			$data = array(
						'materia' => $idmateria,
						'notad' => $nota, 
						'estudiante' => $this->input->post('estudiante'),
						'periodo' => $this->input->post('periodo'),
						'seccion' => $se->secid,
						'anoescolar' => $ae->unicodanoescolarva,
					);
			$this->db->insert("notas",$data);
		}
		$this->index(array('clase'=>"success",'mensaje' => "Se han agregado todas las notas correctamente"));
	}
	
	public function getSecciones()
	{
		$data = $this->db
				->where("seccodanoescolarva",$this->input->post("elegido"))
				->where("seccodigova","TRASLADO")
				->get('secciones')
				->result();
		
		$opciones = "";

		if ($data) 
		{
			foreach ($data as $opcion) {
				$opciones .= "<option value='".$opcion->secid."'>".$opcion->seccodigova." </option> ";
			}
		}
		else
		{
			$opciones = '<option value="">No hay secciones de Traslado</option>';
		}
		echo json_encode($opciones);
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}