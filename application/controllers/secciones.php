<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class secciones extends CI_Controller {
	
	public function __construct()
	{
	
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	
	}

	public function ver($data = null, $titulo = null, $v = 'contenido')
	{

		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/'.$v,$titulo);
		$this->load->view('commons/footer');

	}
	public function index($data = null)
	{

		$titulo['titulo'] = "Secciones";
		$secciones = new grocery_CRUD();
		$secciones->set_table("secciones");
		$secciones->where("seccodigova <> ","Traslado");
		
		$secciones->set_relation("seccodanoescolarva", "anosescolares"," {anocodigova} {anonombreva}");
		$secciones->set_relation("periodo", "periodos"," {percodigova}");
		$secciones->set_relation("seccional_id", "seccionales"," {descripcion}");

		$secciones->field_type('seccodigova','enum',array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'));

		$secciones->display_as("seccodanoescolarva", "Año Escolar");
		$secciones->display_as("seccodigova", "Sección");
		$secciones->display_as("secestatusen", "Estatus");

		$secciones->set_rules("seccodanoescolarva", "Año Escolar","required");
		$secciones->set_rules("seccodigova", "Sección","required");
		$secciones->set_rules("secestatusen", "Estatus","required");
		$secciones->set_rules("periodo", "Periodo","required");

		$secciones->unset_read();
		$secciones->unset_delete();
		/*
		if ($this->session->userdata('admnivelen') != "Administrador") 
		{
			
			$secciones->unset_edit();
		}
		*/

		$secciones->unset_edit();
		$data = $secciones->render();

		$this->ver($data,$titulo);

	}
	
	public function cs($data=null)
	{
		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$this->logout();
			
		}

		$titulo['titulo'] = "Cambio de Sección";
		$data['anosescolares'] = 
			$this->db
				->order_by("anoid")
				->get('anosescolares')
				->result();
		$p = $this->db->where("perestatusen","Activo")->get('periodos')->row(0);
		$data['periodo'] = $p->perid;
		
		$this->ver($data,$titulo,"formcs");

	}

	public function insertarCS()
	{
		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$this->logout();
			
		}

		$titulo['titulo'] = "Cambio de Sección";
		
		$numSecciones = $this->db
								->select("secciones.secid, secciones.seccodigova")
								->join("secciones","secciones.secid = notas.seccion")
								->where("anoescolar",$this->input->post("anoescolar") )
								->where("secciones.seccodigova <>","Traslado")
								->distinct()
								->get("notas")
								->num_rows();
		if ($numSecciones < 2) 
		{

			$data['mensaje'] = "No existen suficientes secciones abiertas para realizar el cambio";
			$data['clase'] = "danger";
			$this->cs($data);

		}
		else
		{

			$data["secciones"] = $this->db
									->select("secciones.secid, secciones.seccodigova")
									->join("secciones","secciones.secid = notas.seccion")
									->where("anoescolar",$this->input->post("anoescolar") )
									->where("secciones.seccodigova <>","Traslado")
									->where("notas.periodo", $this->session->userdata("perid"))
									->order_by("secid")
									->distinct()
									->get("notas")
									->result();
			$data["estudiantes"] = $this->db
									->select("estudiantes.estcodigodo, estudiantes.estcedulava,estudiantes.estnombreva, estudiantes.estapellidova")
									->join("estudiantes","estudiantes.estcodigodo = notas.estudiante")
									->join("secciones","secciones.secid = notas.seccion")
									->where("anoescolar",$this->input->post("anoescolar") )
									->where("secciones.secid",$data['secciones'][0]->secid)
									->where("notas.periodo", $this->session->userdata("perid"))
									->order_by("estudiantes.estcedulava")
									->distinct()
									->get("notas")
									->result();
			$data['anoescolar'] = $this->input->post("anoescolar");
			$this->ver($data,$titulo,"vinsertarCS");
		}

	}

	public function getEstudiantes()
	{
		$data = $this->db
					->select("estudiantes.estcodigodo, estudiantes.estcedulava,estudiantes.estnombreva, estudiantes.estapellidova")
					->join("estudiantes","estudiantes.estcodigodo = notas.estudiante")
					->join("secciones","secciones.secid = notas.seccion")
					->where("anoescolar",$this->input->post("anoid") )
					->where("secciones.secid", $this->input->post("elegido"))
					->where("notas.periodo", $this->session->userdata("perid"))
					->order_by("estudiantes.estcedulava")
					->distinct()
					->get("notas")
					->result();
		
		$opciones = "";

		if ($data) 
		{
			foreach ($data as $opcion) {
				$opciones .= "<option value='".$opcion->estcodigodo."'>".$opcion->estcedulava." ".$opcion->estnombreva." ".$opcion->estapellidova." </option> ";
			}
		}
		else
		{
			$opciones = '<option value="">No hay estudiantes para hacer cambio</option>';
		}
		echo json_encode($opciones);

	}

	public function ejecutarCambios()
	{
		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			
			$this->logout();
			
		}

		if 
		( 
			($_POST['esDesde'] != '' && $_POST['esDesde'] != null && isset($_POST['esDesde']) ) &&
			($_POST['seDesde'] != '' && $_POST['seDesde'] != null && isset($_POST['seDesde']) ) &&
			($_POST['seHasta'] != '' && $_POST['seHasta'] != null && isset($_POST['seHasta']) ) &&
			($_POST['anoid'] != '' && $_POST['anoid'] != null && isset($_POST['anoid']) ) 
		) 
		{
			
			$data = array('seccion' => $this->input->post('seHasta') );

			if($this->db
				->where('periodo',$this->session->userdata('perid') )
				->where("seccion", $this->input->post('seDesde'))
				->where("anoescolar", $this->input->post('anoid'))
				->where("estudiante", $this->input->post('esDesde'))
				->update("notas",$data))
			{
				echo "El cambio de sección se ha ejecutado satisfactoriamente";
			}
			else
			{
				echo "Ha ocurrido un error al hacer el cambio de sección";
			}

		}
	}

	public function consultarTopeAS()
	{
		
		$num = $this->db->get('parametros')->row(0);
		echo $num->paramlimite;

	}

	public function puedeEditar()
	{
		$secid = $this->input->post('secid');

		$periodo = $this->db->where('perestatusen','Activo')->get('periodos')->row(0);

		$data = $this->db
					->select("notas.estudiante")
					->join("secciones","secciones.secid = notas.seccion")
					->where("secciones.secid", $secid)
					->where("notas.periodo", $periodo->perid)
					->get("notas")
					->result();

		if (!empty($data)) {
			$arr['puede'] = 'No';
		} else {
			$arr['puede'] = 'Sí';
		}
		
		echo json_encode($arr);
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}