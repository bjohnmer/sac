<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}
	public function index($datos = null)
	{
		
		$usuarios = $this->db->get("administrador")->result();
		if ($usuarios) {
			$this->load->view('frontend/vista_inicio', $datos);
		}
		else
		{
			$this->crearUsuario();
		}

	}
	function login()
	{

			// Validación de usuario y clave
			$this->form_validation->set_rules('admloginva','Nombre de Usuario','required|max_length[15]|min_length[4]|trim');
			$this->form_validation->set_rules('admclaveva','Clave de Acceso','required|max_length[15]|min_length[4]|trim');
			$d = $this->form_validation->run();
			if ($d) 
			{
				$this->load->library('encrypt');
				
				$resultado = $this->db
				->where('admloginva',$this->input->post('admloginva'))
				->get('administrador')
				->row(0);
				if (!empty($resultado)) 
				{
					$c = $this->encrypt->decode($resultado->admclaveva);
					if ($c == $this->input->post('admclaveva') && $resultado->admestatusen == 'Activo') 
					{
						$pac = $this->db->where("perestatusen", "Activo")->get("periodos")->row(0);
						if ( !$pac )
						{
							$pactivo = "No hay algún Periodo Escolar activo";
							$perid = 0;
						}
						else
						{
							$pactivo = $pac->percodigova;
							$perid = $pac->perid;
						}

						$this->load->library('session');

						$this->session->set_userdata('admid',$resultado->admid);
						$this->session->set_userdata('admnombreva',$resultado->admnombreva);
						$this->session->set_userdata('admnivelen',$resultado->admnivelen);
						$this->session->set_userdata('admloginva',$resultado->admloginva);
						$this->session->set_userdata('periodoActivo',$pactivo);
						$this->session->set_userdata('perid',$perid);
						$this->session->set_userdata('seccional_id',$resultado->seccional_id);
						$this->session->set_userdata('valida_sesion',TRUE);
						redirect('backend/');
					}
					else
					{
						$enviarError = 
						array('errores' => 
							array(
								'error' => 'danger', 
								'mensaje_error' => 'La clave es inválida'
							)
						);
						$this->index($enviarError);		
					}
				}
				else
				{
					$enviarError = 
					array('errores' => 
						array(
							'error' => 'danger', 
							'mensaje_error' => 'El usuario no se encuentra'
						)
					);
					$this->index($enviarError);
				}

			}
			else
			{
				$this->index();
			}

	}
	function crearUsuario($data = null)
	{
		$usuarios = $this->db->get("administrador")->result();
		if ($usuarios) {
			redirect("/");
		}
		else
		{
			$this->load->view('frontend/crear_usuario',$data);
		}
	}
	function guardarUsuario()
	{
		// Validación de usuario y clave
		$this->form_validation->set_rules('admloginva','Nombre de Usuario','required|max_length[15]|min_length[4]|trim');
		$this->form_validation->set_rules('admnombreva','Nombre Completo','required|max_length[50]|min_length[4]|trim');
		$this->form_validation->set_rules('admclaveva','Clave de Acceso','required|max_length[15]|min_length[4]|trim');
		$this->form_validation->set_rules('admclavecva','Confirmar Clave','required|max_length[15]|min_length[4]|trim|matches[admclaveva]');
		if ($this->form_validation->run()) 
		{
			$this->load->library('encrypt');
			$datos = $this->input->post();
			$datos['admclaveva'] = $this->encrypt->encode($datos['admclaveva']);
			$datos['admclavecva'] = $datos['admclaveva'];
			$datos['admnivelen'] = "Administrador";
			$crear = $this->db->insert("administrador",$datos);
			$enviarMensaje = 
			array('errores' => 
				array(
					'error' => 'success', 
					'mensaje_error' => 'Se ha creado un usuario, ahora puede entrar al sistema'
				)
			);
			$this->index($enviarMensaje);
		}
		else
		{
			$this->crearUsuario();
		}
	}
}