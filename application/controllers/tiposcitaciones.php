<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tiposcitaciones extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// print_r( $this->session->all_userdata() );
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Tipos de Citaciones";
		$tiposcitaciones = new grocery_CRUD();
		$tiposcitaciones->set_table("tiposcitaciones");
		
		$tiposcitaciones->display_as("tipnombreva","Descripción");
		$tiposcitaciones->unset_read();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			$tiposcitaciones->unset_delete();
			$tiposcitaciones->unset_edit();
		}

		if ($this->session->userdata('admnivelen') == "Jefe de Seccional") 
		{
			
			$tiposcitaciones->unset_delete();

		}

		$data = $tiposcitaciones->render();
		$this->ver($data,$titulo);
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}