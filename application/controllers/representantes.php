<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class representantes extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('valida_sesion')) 
		{
			$this->logout();	
		}
		$this->load->library("grocery_CRUD");
	}
	public function ver($data = null, $titulo = null)
	{
		$this->load->view('commons/header',$data);
		$this->load->view('dashboard/contenido',$titulo);
		$this->load->view('commons/footer');
	}
	public function index($data = null)
	{
		$titulo['titulo'] = "Representantes";
		$representantes = new grocery_CRUD();
		$representantes->columns("repcedulava","repnombreva","repapellidova","repdireccionva","reptelefono1va","reptelefono2va");
		$representantes->set_table("representantes");
		$representantes->display_as("repcedulava","Cédula");
		$representantes->display_as("repnombreva","Nombre");
		$representantes->display_as("repapellidova","Apellido");
		$representantes->display_as("repsexoen","Género");
		$representantes->display_as("repdireccionva","Dirección");
		$representantes->display_as("reptelefono1va","Teléfono");
		$representantes->display_as("reptelefono2va","Otro Teléfono");
		$representantes->display_as("repprofesionva","Profesión");
		$representantes->display_as("repedocivilen","Estado Civil");
		$representantes->display_as("repfechanacda","Fecha de Nacimiento");

		$representantes->set_rules("repcedulava","Cédula","required|numeric|integer|is_natural_no_zero");
		$representantes->set_rules("repnombreva","Nombre","required");
		$representantes->set_rules("repapellidova","Apellido","required");
		$representantes->set_rules("repsexoen","Género","required");
		$representantes->set_rules("repdireccionva","Dirección","required");
		$representantes->set_rules("reptelefono1va","Teléfono","required|numeric|integer|is_natural_no_zero");
		$representantes->set_rules("reptelefono2va","Otro Teléfono","numeric|integer|is_natural_no_zero");
		$representantes->set_rules("repprofesionva","Profesión","required");
		$representantes->set_rules("repedocivilen","Estado Civil","required");
		$representantes->set_rules("repfechanacda","Fecha de Nacimiento","required");

		$representantes->callback_before_delete(array($this,'_before_delete'));
		
		$representantes->unset_read();

		if ($this->session->userdata('admnivelen') == "Operador") 
		{
			$representantes->unset_delete();
			$representantes->unset_edit();
		}
		if ($this->session->userdata('admnivelen') == "Jefe de Seccional") 
		{
			
			$representantes->unset_delete();

		}

		$representantes->callback_field('repfechanacda',array($this,'_cambiar_objeto'));
		$data = $representantes->render();
		$this->ver($data,$titulo);
	}
	
	function _before_delete( $primary_key )
	{
		
		$tieneSecciones = $this->db
												->where('representante', $primary_key)
												->get('estudiantes')
												->num_rows();

		if ( $tieneSecciones == 0 ) 
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function _cambiar_objeto()
	{
		return '<input id="field-repfechanacda" class="fecha datepicker-input" type="text" maxlength="10" value="" name="repfechanacda"><a class="datepicker-input-clear" tabindex="-1">Resetear</a>(dd/mm/yyyy)';
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}