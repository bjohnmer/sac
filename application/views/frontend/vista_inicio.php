<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>..::Sistema Académico del Liceo Bolivariano Rafael Rangel::..</title>

    <!-- Bootstrap -->
    <link href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/salbrr.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="contenido">
      <div class="container">
        <div class="row">
          <div class="col-sm-5">
            <img src="<?=base_url()?>assets/imagenes/tope_me.png" alt="" class="img-responsive">
          </div>
          <div class="col-sm-4 titulo">
            <h4>Liceo Bolivariano <br> <strong>Rafael Rangel</strong> </h4>
          </div>
          <div class="col-sm-3">
            <img src="<?=base_url()?>assets/imagenes/tope_derecha.jpg" alt="" class="img-responsive">
          </div>
          <div class="col-sm-12">
            <h3 class="text-center">
              Sistema Académico para el Control y Registro de Estudiantes y Docentes
            </h3>
            <h1>&nbsp;</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <!-- <div class="col-sm-12">
              <div class="col-sm-6">
                <img src="<?=base_url()?>assets/imagenes/candado2.png" alt="" class="">
              </div>
            </div> -->
            <div class="panel panel-warning">
              <div class="panel-heading">
                <div class="panel-title">
                  Ingresar al sistema
                </div>
              </div>
              <div class="panel-body">
                <form role="form" class="form-horizontal" action="<?=base_url()?>welcome/login" method="post">
                  <div class="form-group">
                    <label class="col-sm-6 control-label" for="admloginva">Nombre de Usuario</label>
                    <div class="col-sm-6">
                      <input type="text" placeholder="Nombre de Usuario" id="admloginva" name="admloginva" class="form-control" tabindex="1">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-6 control-label" for="admclaveva">Clave de Acceso</label>
                    <div class="col-sm-6">
                      <input type="password" placeholder="Clave" id="admclaveva" name="admclaveva" class="form-control" tabindex="2">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <button class="btn btn-success pull-right" type="submit" tabindex="3">Entrar</button>
                    </div>
                  </div>
                </form>
              </div>
              <?php if (validation_errors()): ?>
              <div class="alert alert-danger fade in">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=validation_errors()?>
                </div>
              <?php endif ?>

              <?php if (!empty($errores['error'])): ?>
              <div class="alert alert-<?=$errores['error']?> fade in">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=$errores['mensaje_error']?>
                </div>
              <?php endif ?>
              <div class="panel-footer">
                Estimado(a) usuario(a) para ingresar al sistema debes colocar tu nombre de usuario y clave de acceso.
              </div>
            </div>
          </div>
          <div class="col-sm-8">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <div class="item active">
                  <img src="<?=base_url()?>assets/imagenes/1.jpg" alt="" class="img-responsive">
                </div>
                <div class="item">
                  <img src="<?=base_url()?>assets/imagenes/2.jpg" alt="" class="img-responsive">
                </div>
                <div class="item">
                  <img src="<?=base_url()?>assets/imagenes/3.jpg" alt="" class="img-responsive">
                </div>
              </div>
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
        <hr>
        <footer>
          <div class="row">
            <div class="col-sm-12">
              <p>
                <small>
                  Liceo Bolivariano "Rafael Rangel" | Sistema Academico | Derechos Reservados <?=date("Y")?>
                   | Periodo: 
                    <?php 
                      $per = $this->db->where("perestatusen","Activo")->get("periodos")->row(0); 
                      if( !$per )
                      {
                        echo "No hay algún Periodo Escolar activo";
                      }
                      else
                      {
                        echo $per->percodigova;
                      }
                    ?>
                </small> 
              </p>
            </div>
          </div>
        </footer>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/holder.js"></script>
    <script>
    jQuery(document).ready(function($) {
      $("#admloginva").focus();
    });
    </script>
  </body>
</html>


