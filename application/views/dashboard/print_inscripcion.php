<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Inscripción</title>
  <style>
    body{
      font-size: 10pt;
    }
  </style>
</head>
<body>
  <table border="0" width="90%" align="center">
    <tr>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_me.png" width="2000%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/nombre_institucion.png" width="800%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_derecha.jpg" width="1000%">
      </td>
    </tr>
  </table>
  <table border="0" width="100%" align="center">
    <tr>
      <td align="center">
        <h5>
          REPÚBLICA BOLIVARIANA DE VENEZUELA <br>
          MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN <br>
          LICEO BOLIVARIANO "RAFAEL RANGEL" <br>
          VALERA, ESTADO TRUJILLO
        </h5>
        <h6>FORMULARIO DE INSCRIPCIÓN</h6>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" valign="top">
          <tr>
            <td colspan="3" style="border-bottom: #ccc thin solid">Año Escolar: <strong><?=$datos_inscripcion->anocodigova?> (<?=$datos_inscripcion->anonombreva?>)</strong></td>
            <td colspan="1" style="border-bottom: #ccc thin solid">Sección: <strong><?=$datos_inscripcion->seccodigova?></strong></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">
              <strong>A.- DATOS DEL ESTUDIANTE</strong>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border-bottom: #ccc thin solid">Apellidos: <strong><?=$estudiante->estapellidova?></strong></td>
            <td colspan="2" style="border-bottom: #ccc thin solid">Nombres: <strong><?=$estudiante->estnombreva?></strong></td>
          </tr>
          <tr>
            <td style="border-bottom: #ccc thin solid">C.I.: <strong><?=$estudiante->estcedulava?></strong></td>
            <td style="border-bottom: #ccc thin solid">Sexo: <strong><?=$estudiante->estsexoen?></strong></td>
            <td style="border-bottom: #ccc thin solid">Edad: <strong><?=$this->datemanager->edad($estudiante->estfechanacda)?></strong></td>
            <td style="border-bottom: #ccc thin solid">Fecha de Nacimiento: <strong><?=$this->datemanager->date2normal($estudiante->estfechanacda)?></strong></td>
          </tr>
          <tr>
            <td colspan="3" style="border-bottom: #ccc thin solid">Dirección: <strong><?=$estudiante->estdireccionva?></strong></td>
            <td colspan="1" style="border-bottom: #ccc thin solid">Teléfono: <strong><?=$estudiante->esttelefonova?></strong></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4"><strong>B. DATOS DEL REPRESENTANTE</strong></td>
          </tr>
          <tr>
            <td colspan="2" style="border-bottom: #ccc thin solid">Apellidos: <strong><?=$estudiante->repapellidova?></strong></td>
            <td colspan="2" style="border-bottom: #ccc thin solid">Nombres: <strong><?=$estudiante->repnombreva?></strong></td>
          </tr>
          <tr>
            <td colspan="2" style="border-bottom: #ccc thin solid">C.I.: <strong><?=$estudiante->repcedulava?></strong></td>
            <td colspan="2" style="border-bottom: #ccc thin solid">Profesión u Oficio: <strong><?=$estudiante->repprofesionva?></strong></td>
          </tr>
          <tr>
            <td colspan="1" style="border-bottom: #ccc thin solid">Teléfonos: <strong><?=$estudiante->reptelefono1va?>, <?=$estudiante->reptelefono2va?></strong></td>
            <td colspan="3" style="border-bottom: #ccc thin solid">Dirección: <strong><?=$estudiante->repdireccionva?></strong></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><strong>C. RECAUDOS</strong></td>
          </tr>
          <tr>
            <td colspan="4">
              <table border="0" width="100%">
                <tr>
                  <td style="border-bottom: #ccc thin solid">Foto del estudiante: <?=$estudiante->foto_estudiante == "Sí" ? $estudiante->foto_estudiante : "No" ?></td>
                  <td style="border-bottom: #ccc thin solid">Partida de nacimiento: <?=$estudiante->partidanac_estudiante == "Sí" ? $estudiante->partidanac_estudiante : "No" ?></td>
                  <td colspan="2" style="border-bottom: #ccc thin solid">Copia de cédula del estudiante: <?=$estudiante->copiacedula_estudiante == "Sí" ? $estudiante->copiacedula_estudiante : "No" ?></td>
                </tr>
                <tr>
                  <td style="border-bottom: #ccc thin solid">Constancia de buena conducta: <?=$estudiante->constanciabc_estudiante == "Sí" ? $estudiante->constanciabc_estudiante : "No" ?></td>
                  <td style="border-bottom: #ccc thin solid">Notas certificadas: <?=$estudiante->notasc_estudiante == "Sí" ? $estudiante->notasc_estudiante : "No" ?></td>
                  <td colspan="2" style="border-bottom: #ccc thin solid">Foto del representante: <?=$estudiante->foto_representante == "Sí" ? $estudiante->foto_representante : "No" ?></td>
                </tr>
                <tr>
                  <td colspan="3" style="border-bottom: #ccc thin solid">Copia de cédula del representante: <?=$estudiante->copiac_representante == "Sí" ? $estudiante->copiac_representante : "No" ?></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4" style="border-bottom: #ccc thin solid">
              Repite: Sí:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Año Escolar:
            </td>
          </tr>
          <tr>
            <td colspan="4">
              <p><strong>COMPROMISO DEL REPRESENTANTE</strong>
              <br>Hago constar que me comprometo a cumplir y hacer cumplir por mi representado, los deberes y obligaciones que nos imponen las Leyes y Reglamentos vigentes, como también aquellas disposiciones emanadas por las Autoridades del Plantel. <br>
              <strong>NOTA: LA INSTITUCIÓN NO SE HACE RESPONSABLE POR PÉRDIDAS DE CELULARES NI OTROS OBJETOS DE VALOR.</strong></p>
            </td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">
              <table border="0" width="100%">
                <tr>
                  <td style="border-bottom: #ccc thin solid">&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td style="border-bottom: #ccc thin solid">&nbsp;</td>
                </tr>
                <tr>
                  <td><p align="center">Firma del Estudiante</p></td>
                  <td colspan="2">&nbsp;</td>
                  <td><p align="center">Firma del Representante</p></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>