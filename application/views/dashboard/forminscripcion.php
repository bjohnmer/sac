        <!-- 
          Nomina, Notas e Inscripción
         -->
        <?php if (!empty($titulo)): ?>
            <hr>
            <div class="col-sm-12 text-center">
              <h1><?=$titulo?></h1>
              <div class="col-sm-6  col-sm-offset-3">
                <a href="<?=base_url()?>reportes/ii" class="btn btn-success"><span class="glyphicon glyphicon-print"></span> Imprimir Inscripción</a>
              </div>
              <div class="col-sm-12">&nbsp;</div>
              <div class="well col-sm-6  col-sm-offset-3">
                <?php if (validation_errors()): ?>
                <div class="alert alert-warning fade in">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=validation_errors()?>
                </div>
                <?php endif ?>
                <?php if (!empty($errores)): ?>
                  <div class="alert alert-<?=$errores['clase']?> fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?=$errores['mensaje_error']?>
                  </div>
                <?php endif ?>
                
                <?php if ( empty($errores) || !$errores['seccionales']  ): ?>
                <?php 
                  $hayEstudiantes = $this->db
                                      ->get('estudiantes')
                                      ->num_rows();
                 ?>
                <?php if ( $hayEstudiantes > 0  ): ?>
                <form role="form" class="form-horizontal" action="<?=base_url()?>inscripcion/seleccionarDatos" method="post">
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="cedula">Cédula</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cédula del Estudiante">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                      <input type="hidden" name="periodo" value="<?=$periodo?>">
                      <button class="btn btn-primary">Inscribir Estudiante</button>
                    </div>
                  </div>
                </form>
                <?php else: ?>
                  <div class="alert alert-danger fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    No hay Estudiantes inscritos
                  </div>
                <?php endif ?>
                <?php endif ?>
              </div>
            </div>
            
        <?php endif ?>