<div class="col-sm-12">
	<div class="col-sm-7">
		<h1>Datos Personales</h1>
		<?php if (validation_errors()): ?>
		<div class="alert alert-warning fade in">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <?=validation_errors()?>
		</div>
		<?php endif ?>
		<?php if (!empty($e)): ?>
		<div class="alert alert-<?=$e['tipo']?> fade in">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <?=$e['mensaje']?>
		</div>              
		<?php endif ?>

	<?php if (!empty($datos)): ?>
		<form role="form" class="form-horizontal" action="<?=base_url()?>generales/guardarDpersonales" enctype="multipart/form-data" method="post">
	      <?php if ( $this->session->userdata('rol_usuario') == "Alumno" || $this->session->userdata('rol_usuario') == "Tutor Académico" ): ?>
	      
	      <?php if ( $this->session->userdata('rol_usuario') == "Alumno" ): ?>
			
			<?php if (!empty($datos['foto_alumno'])): ?>
				<div class="form-group">
					<div class="col-sm-5">&nbsp;</div>
					<div class="col-sm-7 thumbnail">
						<img src="<?=base_url()?>assets/uploads/files/<?=$datos['foto_alumno']?>" alt="<?=$datos['nombre']?> <?=$datos['apellido']?>" title="<?=$datos['nombre']?> <?=$datos['apellido']?>" class="img-responsive">
					</div>
				</div>
			<?php endif ?>
			
			<div class="form-group">
				<label class="col-sm-5 control-label" for="foto">Foto</label>
				<div class="col-sm-7">
					<input type="file" placeholder="Foto" id="foto" name="foto_alumno">
					<input type="hidden" name="foto_alumnoC" value="<?=$datos['foto_alumno']?>">
				</div>
			</div>
      	  <?php endif; ?>
	      <div class="form-group">
	        <label class="col-sm-5 control-label" for="cedula">Cédula</label>
	        <div class="col-sm-7">
	          <input type="text" placeholder="Cédula" name="cedula" id="cedula" class="form-control" value="<?=$datos['cedula']?>" disabled>
	        </div>
	      </div>
	      <div class="form-group">
	        <label class="col-sm-5 control-label" for="nombre">Nombre</label>
	        <div class="col-sm-7">
	          <input type="text" placeholder="Nombre" name="nombre" id="nombre" class="form-control" value="<?=$datos['nombre']?>">
	        </div>
	      </div>
		  <div class="form-group">
	        <label class="col-sm-5 control-label" for="apellido">Apellido</label>
	        <div class="col-sm-7">
	          <input type="text" placeholder="Apellido" name="apellido" id="apellido" class="form-control" value="<?=$datos['apellido']?>">
	        </div>
	      </div>
		  <div class="form-group">
	        <label class="col-sm-5 control-label" for="correo">Correo</label>
	        <div class="col-sm-7">
	          <input type="text" placeholder="Correo" name="correo" id="correo" class="form-control" value="<?=$datos['correo']?>">
	        </div>
	      </div>
		  <div class="form-group">
	        <label class="col-sm-5 control-label" for="telefono">Teléfono</label>
	        <div class="col-sm-7">
	          <input type="text" placeholder="Teléfono" name="telefono" id="telefono" class="form-control" value="<?=$datos['telefono']?>">
	        </div>
	      </div>

	      <?php if ( $this->session->userdata('rol_usuario') == "Alumno" ): ?>
  			<div class="form-group">
  				<label class="col-sm-5 control-label" for="direccion_hab_alumno">Dirección</label>
  				<div class="col-sm-7">
  					<textarea placeholder="Dirección" id="direccion_hab_alumno" name="direccion_hab_alumno" class="form-control"><?=$datos['direccion_hab_alumno']?></textarea>
  				</div>
  			</div>
  			<div class="form-group">
  				<label class="col-sm-5 control-label" for="localidad_alumno">Localidad</label>
  				<div class="col-sm-7">
  					<input type="text" placeholder="Localidad" id="localidad_alumno" name="localidad_alumno" class="form-control" value="<?=$datos['localidad_alumno']?>">
  				</div>
  			</div>
			<div class="form-group">
  				<label class="col-sm-5 control-label" for="id_carrera">Carrera</label>
  				<div class="col-sm-7">
  					<select name="id_carrera" id="id_carrera" class="form-control">
						<?php if (!empty($carreras)): ?>
							<?php foreach ($carreras as $carrera): ?>
								<option value="<?=$carrera->id_carrera?>" <?php if ($datos['id_carrera'] == $carrera->id_carrera) { echo "selected"; } ?> ><?=$carrera->cod_carrera?> <?=$carrera->nombre_carrera?></option>
							<?php endforeach ?>
						<?php endif ?>
  					</select>
  				</div>
  			</div>

  			<div class="form-group">
  				<label class="col-sm-5 control-label" for="turno_alumno">Turno</label>
  				<div class="col-sm-7">
					<select name="turno_alumno" id="turno_alumno" class="form-control">
						<option value="Diurno" <?php if ($datos['turno_alumno'] == "Diurno") { echo "selected"; }?>>Diurno</option>
						<option value="Nocturno" <?php if ($datos['turno_alumno'] == "Nocturno") { echo "selected"; }?>>Nocturno</option>
						<option value="Fin de Semana" <?php if ($datos['turno_alumno'] == "Fin de Semana") { echo "selected"; }?>>Fin de Semana</option>
					</select>
  				</div>
  			</div>
			<input type="hidden" name="id_alumno" value="<?=$datos['id_alumno']?>">
    	  <?php endif; ?>
		
	      <?php if ( $this->session->userdata('rol_usuario') == "Tutor Académico" ): ?>
			<input type="hidden" name="id_tutoracademico" value="<?=$datos['id_tutoracademico']?>">
    	  <?php endif; ?>

	      <?php endif; ?>
		  <hr>
	      <div class="form-group">
	        <label class="col-sm-5 control-label" for="usuario">Usuario</label>
	        <div class="col-sm-7">
	          <input type="text" placeholder="Usuario" name="usuario" id="usuario" class="form-control" value="<?=$datos['usuario']?>" <?php if( $this->session->userdata('rol_usuario') != "Administrador" ): ?> disabled <?php endif; ?> >
	        </div>
	      </div>
	      <div class="form-group">
	        <label class="col-sm-5 control-label" for="contrasena">Contraseña</label>
	        <div class="col-sm-7">
	          <input type="password" placeholder="Contraseña" name="contrasena" id="contrasena" class="form-control">
	        </div>
	      </div>
	      <div class="form-group">
	        <label class="col-sm-5 control-label" for="confirmacion">Confirmar Contraseña</label>
	        <div class="col-sm-7">
	          <input type="password" placeholder="Confirmar" name="confirmacion" id="confirmacion" class="form-control">
	        </div>
	      </div>
		  <div class="form-group">
	        <div class="col-sm-offset-5 col-sm-7">
	          <button class="btn btn-success" type="submit">Guardar Cambios</button>
	        </div>
	      </div>
	      <input type="hidden" name="rol" value="<?=$this->session->userdata('rol_usuario')?>">
	    </form>
	<?php else: ?>
		<h3>No hay datos de sesión abiertos</h3>
	<?php endif ?>
	</div>
</div>