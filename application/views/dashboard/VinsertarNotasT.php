<?php if (!empty($titulo)): ?>
  <form action="<?=base_url()?>traslados/procesarNotas" class="form" method="post">
    <hr>
    <div class="col-sm-12">
      <h2 class="text-center">Ingresar Notas</h2>
      <h4><?=$titulo?></h4>

       <?php if (!empty($unidadescurriculares)): ?>
        <?php 
          $i = count($distribucion);
        ?>
        <?php foreach ($distribucion as $key): ?>
        <div class="col-sm-4">
          <table class="table">
            <thead>
              <tr>
                <th colspan="2">
                  Año Escolar: <?=$key->anonombreva?>
                  <?php if ($i == 1): ?>
                    <input type="hidden" name="aes" value="<?=$key->unicodanoescolarva?>">
                  <?php endif ?>
                </th>
              </tr>
              <tr>
                <th width="75%">Unidad Curricular</th>
                <th width="25%">Nota</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($unidadescurriculares as $uc): ?>
              <?php if ($uc->unicodanoescolarva == $key->unicodanoescolarva): ?>
              <tr>
                <td><?=$uc->uninombreva?></td>
                <td>
                  <input type="number" name="notas[<?=$uc->uniid?>]" id="<?=$uc->uniid?>" min="<?=$i == 1 ? 1 : 10?>" max="20" style="width:50px" required>
                </td>
              </tr>
              <?php endif ?>
            <?php endforeach ?>
            </tbody>
          </table>
        </div>
              <?php $i--; ?>
        <?php endforeach ?>
      <?php endif ?>
      
      <div class="col-sm-3">
        <div>
          <button class="btn btn-primary">
            Guardar Notas
          </button>
        </div>
      </div>
    </div>
    <input type="hidden" value="<?=$estudiante->estcodigodo?>" name="estudiante">
    <input type="hidden" value="<?=$periodo->perid?>" name="periodo">
    <input type="hidden" value="<?=$seccion->secid?>" name="seccion">
    <input type="hidden" value="<?=$anoescolar->anoid?>" name="anoescolar">
  </form>
<?php endif ?>