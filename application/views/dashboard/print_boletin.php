<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Boletín</title>
  <style>
    body{
      font-size: 10pt;
    }
  </style>
</head>
<body>
  <table border="0" width="90%" align="center">
    <tr>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_me.png" width="2000%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/nombre_institucion.png" width="800%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_derecha.jpg" width="1000%">
      </td>
    </tr>
  </table>
  <table border="0" width="100%" align="center">
    <tr>
      <td align="center">
        <h5>
          REPÚBLICA BOLIVARIANA DE VENEZUELA <br>
          MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN <br>
          LICEO BOLIVARIANO "RAFAEL RANGEL" <br>
          VALERA, ESTADO TRUJILLO
        </h5>
        <h3>BOLETÍN</h3>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" valign="top">
          <tr>
            <td colspan="4">
              <strong>DATOS DEL ESTUDIANTE</strong>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border-bottom: #ccc thin solid">Apellidos: <strong><?=$estudiante->estapellidova?></strong></td>
            <td colspan="2" style="border-bottom: #ccc thin solid">Nombres: <strong><?=$estudiante->estnombreva?></strong></td>
          </tr>
          <tr>
            <td colspan="2" style="border-bottom: #ccc thin solid">C.I.: <strong><?=$estudiante->estcedulava?></strong></td>
            <td style="border-bottom: #ccc thin solid">Año Escolar: <strong><?=$notas[0]->anonombreva?></strong></td>
            <td style="border-bottom: #ccc thin solid">Sección: <strong><?=$notas[0]->seccodigova?></strong></td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4" align="center"><strong>NOTAS</strong></td>
          </tr>
          <tr>
            <td colspan="4">
              <table border="1" width="100%" align="center" cellspacing="0">
                <tr>
                  <td align="center"><strong>ASIGNATURAS</strong></td>
                  <td align="center"><strong>1er Lapso</strong></td>
                  <td align="center"><strong>2do Lapso</strong></td>
                  <td align="center"><strong>3er Lapso</strong></td>
                  <td align="center"><strong>Definitiva</strong></td>
                </tr>
                <?php foreach ($notas as $nota): ?>
                <tr>
                  <td align="center" height="20"><?=!empty($nota->uninombreva) ? $nota->uninombreva : "-"?></td>
                  <td align="center" height="20"><?=!empty($nota->nota1) ? $nota->nota1 : "-"?></td>
                  <td align="center" height="20"><?=!empty($nota->nota2) ? $nota->nota2 : "-"?></td>
                  <td align="center" height="20"><?=!empty($nota->nota3) ? $nota->nota3 : "-"?></td>
                  <td align="center" height="20"><?=!empty($nota->notad) ? $nota->notad : "-"?></td>
                </tr>
                <?php endforeach ?>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4">
              <table border="0" width="100%">
                <tr>
                  <td style="border-bottom: #ccc thin solid">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td style="border-bottom: #ccc thin solid">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td style="border-bottom: #ccc thin solid">&nbsp;</td>
                </tr>
                <tr>
                  <td><p align="center">Coordinador de Seccional</p></td>
                  <td>&nbsp;</td>
                  <td><p align="center">Coordinador del Departamento de Evaluación</p></td>
                  <td>&nbsp;</td>
                  <td><p align="center">Firma del Representante</p></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>