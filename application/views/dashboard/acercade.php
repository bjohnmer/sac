            <div class="col-sm-8">
              <img src="<?=base_url()?>assets/imagenes/acercade.jpg" alt="..::Sistema Académico del Liceo Bolivariano Rafael Rangel::.." class="img-responsive">
            </div>
            <div class="col-sm-4">
            <?php 
              $per = $this->db->where("perestatusen","Activo")->get("periodos")->row(0);
            ?>
            <?php if ( $per ): ?>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>estudiantes" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-user"></span>
                    </h3>
                    <h6>Estudiantes</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>citaciones" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-bullhorn"></span>
                    </h3>
                    <h6>Citaciones</h6>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>inscripcion" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-pencil"></span>
                    </h3>
                    <h6>Inscripciones</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>inasistencias" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-check"></span>
                    </h3>
                    <h6>Inasistencias</h6>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>traslados" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-hand-down"></span>
                    </h3>
                    <h6>Estudiante de traslado</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>notas" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-star"></span>
                    </h3>
                    <h6>Notas</h6>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>reportes/boletin" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-list-alt"></span>
                    </h3>
                    <h6>Boletín</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>secciones/cs" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-random"></span>
                    </h3>
                    <h6>Cambio de Sección</h6>
                  </a>
                </div>
              </div>
            <?php endif; ?>

              
            </div>
            
          </div>