        <?php if (!empty($titulo)): ?>
            <hr>
            <div class="col-sm-12 text-center">
              <h1><?=$titulo?></h1>
              <div class="col-sm-12">&nbsp;</div>
              <div class="well col-sm-12">
                <?php if (validation_errors()): ?>
                <div class="alert alert-warning fade in">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=validation_errors()?>
                </div>
                <?php endif ?>
                <?php 
                  if (!empty($this->session->flashdata('clase'))) {
                    $clase = $this->session->flashdata('clase');
                  }
                  if (!empty($this->session->flashdata('mensaje'))) {
                    $mensaje = $this->session->flashdata('mensaje');
                  }
                ?>
                <?php if (!empty($mensaje)): ?>
                  <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                      <div class="alert alert-<?=$clase?> fade in">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?=$mensaje?>
                      </div>
                    </div>
                  </div>
                <?php endif ?>
                <form role="form" class="form-horizontal" action="<?=base_url()?>seccionales/storeAsec" method="post">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <h5 for="cedula">Seleccione las Secciones de los años escolares que van a ser asignadas a esta seccional</h5>
                    </div>
                    <?php foreach ($anos_escolares as $anio): ?>
                      <div class="col-sm-2">
                        <div class="col-sm-12">
                          <label class="control-label" for="<?=$anio->anoid?>"><?=$anio->anocodigova?>° <?=$anio->anonombreva?></label>
                        </div>
                        <div class="col-sm-12">
                          <select name="secciones[<?=$anio->anoid?>][]" id="<?=$anio->anoid?>" class="form-control" multiple="multiple" style="height:30em">
                          <?php foreach ($secciones as $seccion): ?>
                            <?php if ($seccion->seccodanoescolarva == $anio->anoid): ?>
                              <option value="<?=$seccion->secid?>" <?=$seccion->seccional_id == $seccional->id ? "selected" : "" ?>><?=$seccion->seccodigova?></option>
                            <?php endif ?>                                
                          <?php endforeach ?>
                          </select>
                        </div>
                      </div>
                    <?php endforeach ?>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                      <input type="hidden" name="seccional" value="<?=$seccional->id?>">
                      <button class="btn btn-primary"><span class="glyphicon glyphicon-tasks"></span> Asignar Seccional</button>
                    </div>
                  </div>
                </form> 
              </div>
            </div>
            
        <?php endif ?>