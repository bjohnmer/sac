        <?php if (!empty($titulo)): ?>
            <hr>
            <div class="col-sm-12 text-center">
              <h1><?=$titulo?></h1>
              <div class="col-sm-12">&nbsp;</div>
              <div class="well col-sm-6  col-sm-offset-3">
                <?php if (validation_errors()): ?>
                <div class="alert alert-warning fade in">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=validation_errors()?>
                </div>
                <?php endif ?>
                <?php 
                  if (!empty($this->session->flashdata('clase'))) {
                    $clase = $this->session->flashdata('clase');
                  }
                  if (!empty($this->session->flashdata('mensaje'))) {
                    $mensaje = $this->session->flashdata('mensaje');
                  }
                ?>
                <?php if (!empty($mensaje)): ?>
                  <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                      <div class="alert alert-<?=$clase?> fade in">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?=$mensaje?>
                      </div>
                    </div>
                  </div>
                <?php endif ?>
                <form role="form" class="form-horizontal" action="<?=base_url()?>usuarios/storeAsec" method="post">
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="cedula">Seccionales</label>
                    <div class="col-sm-5">
                      <?php foreach ($seccionales as $seccional): ?>
                        <div class="radio">
                          <label>
                            <input type="radio" name="seccional_id" id="seccional_<?=$seccional->id?>" value="<?=$seccional->id?>" <?=$seccional->id == $usuario->seccional_id ? "checked" : ""?>>
                            <?=$seccional->descripcion?>
                          </label>
                        </div>
                      <?php endforeach ?>
                      <div class="radio">
                        <label>
                          <input type="radio" name="seccional_id" id="seccional_0" value="0" <?= 0 == $usuario->seccional_id ? "checked" : ""?>>
                          Ninguna
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                      <input type="hidden" name="usuario" value="<?=$usuario->admid?>">
                      <button class="btn btn-primary"><span class="glyphicon glyphicon-tasks"></span> Asignar Seccional</button>
                    </div>
                  </div>
                </form> 
              </div>
            </div>
            
        <?php endif ?>