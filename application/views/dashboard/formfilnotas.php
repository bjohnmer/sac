        <?php if (!empty($titulo)): ?>
            <hr>
            <div class="col-sm-12 text-center">
              <h1><?=$titulo?></h1>
              <div class="well col-sm-6  col-sm-offset-3">
                <?php if (!empty($mensaje)): ?>
                  <div class="alert alert-<?=$clase?> fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?=$mensaje?>
                  </div>
                <?php endif ?>

                <?php 
                  $hayInscritos = $this->db
                                      ->where('periodo', $this->session->userdata('perid'))
                                      ->get('notas')
                                      ->num_rows();
                 ?>
                <?php if ( $hayInscritos > 0  ): ?>

                <form role="form" class="form-horizontal" action="<?=base_url()?>notas/insertarNotas" method="post">
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="anoescolar">Año Escolar</label>
                    <div class="col-sm-6">
                      <select name="anoescolar" id="anoescolar" class="form-control">
                        <?php if (!empty($anosescolares)): ?>
                          <?php foreach ($anosescolares as $anio): ?>
                            <option value="<?=$anio->anoid?>"><?=$anio->anonombreva?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="seccion">Sección</label>
                    <div class="col-sm-6">
                      <select name="seccion" id="seccion" class="form-control">
                        <?php if (!empty($secciones)): ?>
                          <?php foreach ($secciones as $seccion): ?>
                              <option value="<?=$seccion->secid?>"><?=$seccion->seccodigova?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="materia">Unidad Curricular</label>
                    <div class="col-sm-6">
                      <select name="materia" id="materia" class="form-control">
                        <?php if (!empty($materias)): ?>
                          <?php foreach ($materias as $materia): ?>
                              <option value="<?=$materia->uniid?>"><?=$materia->uninombreva?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label" for="lapso">Lapso</label>
                    <div class="col-sm-6">
                      <select name="lapso" id="lapso" class="form-control">
                        <?php if (!empty($lapsos)): ?>
                          <?php foreach ($lapsos as $lapso): ?>
                              <option value="<?=$lapso->lapsoid?>"><?=$lapso->lapnombreva?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class=" col-sm-4 pull-right">
                      <br>
                      <input type="hidden" name="periodo" value="<?=$periodo?>">
                      <button class="btn btn-lg btn-primary">Agregar Notas</button>
                    </div>
                  </div>
                </form


                <?php else: ?>
                  <div class="alert alert-danger fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    No hay Estudiantes inscritos en este peiriodo
                  </div>
                <?php endif ?>
              </div>
            </div>
            
        <?php endif ?>