        <?php if (!empty($titulo)): ?>
            <hr>
            <div class="col-sm-12">
              <h1 class="text-center"><?=$titulo?></h1>
              <?php if (!empty($mensaje)): ?>
                <div class="alert alert-<?=$clase?> fade in">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=$mensaje?>
                </div>
              <?php endif ?>
              <div class="well col-sm-5">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="secciones1">Sección</label>
                    <div class="col-sm-3">
                      <select name="secciones1" id="secciones1" class="form-control">
  
                        <?php if (!empty($secciones)): ?>
                          <?php foreach ($secciones as $seccion): ?>
                            <option value="<?=$seccion->secid?>"><?=$seccion->seccodigova?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                        
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">&nbsp;</div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="col-sm-3 control-label" for="estudiantes1">Estudiantes</label>
                    <div class="col-sm-9">
                      <input type="hidden" name="anoid" id="anoid" value="<?=$anoescolar?>">
                      <select name="estudiantes1" id="estudiantes1" class="form-control" multiple="multiple">
                        
                        <?php if (!empty($estudiantes)): ?>
                          <?php foreach ($estudiantes as $estudiante): ?>
                            <option value="<?=$estudiante->estcodigodo?>"><?=$estudiante->estcedulava?> <?=$estudiante->estnombreva?> <?=$estudiante->estapellidova?></option>
                          <?php endforeach ?>
                        <?php endif ?>

                      </select>
                    </div>
                  </div>
                </div>
                
              </div>

              <div class="col-sm-1">
                <a alt="Mover a la derecha" title="Mover a la derecha" id="unod" href="#" class="col-sm-12 btn btn-primary"><span class="glyphicon glyphicon-chevron-right"></span></a>
                <div class="col-sm-12">&nbsp;</div>
                <!-- <a alt="Mover todos a la derecha" title="Mover todos a la derecha" id="todosd" href="#" class="col-sm-12 btn btn-primary"><span class="glyphicon glyphicon-forward"></span></a> -->
                <a alt="Mover a la izquierda" title="Mover a la izquierda" id="unoi" href="#" class="col-sm-12 btn btn-primary"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <!-- <a alt="Mover todos a la izquierda" title="Mover todos a la izquierda" id="todosi" href="#" class="col-sm-12 btn btn-primary"><span class="glyphicon glyphicon-backward"></span></a> -->
              </div>
              <div class="well col-sm-5">
                <form role="form" class="form-horizontal" action="#" method="post">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="col-sm-3 control-label" for="secciones2">Sección</label>
                      <div class="col-sm-3">
                        <select name="secciones2" id="secciones2" class="form-control">
                          <?php if (!empty($secciones)): ?>
                            <?php foreach ($secciones as $seccion): ?>
                              <option value="<?=$seccion->secid?>"><?=$seccion->seccodigova?></option>
                            <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="col-sm-3 control-label" for="estudiantes2">Estudiantes</label>
                      <div class="col-sm-9">
                        <select name="estudiantes2" id="estudiantes2" class="form-control" multiple="multiple">
                          <?php if (!empty($estudiantes)): ?>
                            <?php foreach ($estudiantes as $estudiante): ?>
                              <option value="<?=$estudiante->estcodigodo?>"><?=$estudiante->estcedulava?> <?=$estudiante->estnombreva?> <?=$estudiante->estapellidova?></option>
                            <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </div>
                  </div>
                
                </form> 
              </div>
            </div>

            <div class="col-sm-12">&nbsp;</div>

        <?php endif ?>