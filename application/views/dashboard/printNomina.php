<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Inscripción</title>
  <style>
    body{
      font-size: 10pt;
    }
  </style>
</head>
<body>
  <table border="0" width="90%" align="center">
    <tr>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_me.png" width="2000%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/nombre_institucion.png" width="800%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_derecha.jpg" width="1000%">
      </td>
    </tr>
  </table>
  <table border="0" width="100%" align="center">
    <tr>
      <td align="center">
        <h5>
          REPÚBLICA BOLIVARIANA DE VENEZUELA <br>
          MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN <br>
          LICEO BOLIVARIANO "RAFAEL RANGEL" <br>
          VALERA, ESTADO TRUJILLO
        </h5>
        <h6>NÓMINA DE ESTUDIANTES <?=$anoescolar->anocodigova?>° Año Sección <?=$seccion->seccodigova?></h6>
        <h6>Periodo <?=$this->session->userdata("periodoActivo")?></h6>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" cellspacing="0" border="1" valign="middle" align="left">
          <tr>
            <td width="5%"  align="center">N°</td>
            <td width="10%">Cédula</td>
            <td width="25%">Apellidos y Nombres</td>
            <td width="2%" align="center">S<br>e<br>x<br>o</td>
            <td width="2%" align="center">E<br>d<br>a<br>d</td>
            <td width="10%">&nbsp;<br><br><br><br><br><br><br><br><br></td>
            <td width="10%">&nbsp;</td>
            <td width="10%">&nbsp;</td>
            <td width="10%">&nbsp;</td>
            <td width="10%">&nbsp;</td>
            <td width="10%">&nbsp;</td>
          </tr>
          <?php $i=1; foreach ($notas as $est): ?>
            <tr>
              <td  align="center">
                <?=$i?>
              </td>
              <td><?=$est->estcedulava?></td>
              <td><?=$est->estapellidova?> <?=$est->estnombreva?></td>
              <td  align="center"><?=substr($est->estsexoen, 0,1)?></td>
              <td  align="center"><?=$this->datemanager->edad($est->estfechanacda)?></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          <?php $i++; endforeach ?>

          
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
