<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Citación</title>
  <style>
    /*body{
      font-size: 12pt;
    }*/
  </style>
</head>
<body>
  <table border="0" width="90%" align="center">
    <tr>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_me.png" width="2000%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/nombre_institucion.png" width="800%">
      </td>
      <td>
          <img src="<?=base_url()?>assets/imagenes/tope_derecha.jpg" width="1000%">
      </td>
    </tr>
  </table>

  <table border="0" width="90%" align="center">
    <tr>
      <td>
        <h4 align="center">CITACIÓN</h4>
      </td>
    </tr>
    <tr><td>Datos del Estudiante:</td></tr>
    <tr>
      <td>
        Nombre y Apellido: <strong><?=$citacion->estnombreva?> <?=$citacion->estapellidova?></strong>
      </td>
    </tr>
    <tr>
      <td>
        Año Escolar: <strong><?=$notas->anonombreva?></strong> Seccion: <strong><?=$notas->seccodigova?></strong>
      </td>
    </tr>
    <tr>
      <td>
        Motivo: <strong><?=$citacion->tipnombreva?></strong>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>
      Señor(a): <strong><?=$representante->repnombreva . ' ' . $representante->repapellidova?></strong>, sirva la presente para notificarle
      que es necesario que se presente en la institución, para tratar tema relacionado con su representado.
      </td>
    </tr>
    <tr>
      <td>
        <p>
          Observación: <strong><?=$citacion->citobservacionva?></strong>
        </p>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="border-bottom: #ccc thin solid">
        Citado por: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Firma: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <br><br><br><br>
  <p align="center" style="width:30%; margin:0 auto; border-top: #000 thin solid; padding: 1em;">
    Firma del Representante
  </p>
</body>
</html>