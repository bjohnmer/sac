        <?php if (!empty($titulo)): ?>
          <form action="<?=base_url()?>inasistencias/procesarInasistencias" class="form" method="post">
            <hr>
            <div class="col-sm-12">
              <div class="col-sm-8">
                  <h4 class="text-center"><?=$titulo?></h4>
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Lista de Estudiantes</th>
                        <th>Inasistencia</th>
                        <th>Observación</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (!empty($estudiantes)): ?>
                        <?php foreach ($estudiantes as $estudiante): ?>
                          <tr>
                            <td><?=$estudiante->estcedulava?> <?=$estudiante->estapellidova?> <?=$estudiante->estnombreva?></td>
                            <td>
                              <input type="checkbox" name="inaidestudiantedo[<?=$estudiante->estcodigodo?>]" id="<?=$estudiante->estcodigodo?>">
                            </td>
                            <td>
                              <input type="text" name="inaobservacionva[<?=$estudiante->estcodigodo?>]" id="<?=$estudiante->estcodigodo?>" class="form-control">
                            </td>
                          </tr>
                        <?php endforeach ?>
                      <?php endif ?>
                    </tbody>
                  </table>
              </div>
              <div class="col-sm-3">
                <div>
                  <button class="btn btn-primary">
                    Guardar Inasistencias
                  </button>
                </div>
              </div>
            </div>
            <input type="hidden" value="<?=$periodo->perid?>" name="perid">
            <input type="hidden" value="<?=$seccion->secid?>" name="secid">
            <input type="hidden" value="<?=$anoescolar->anoid?>" name="anoid">
            <input type="hidden" name="inafechatt" id="inafechatt" value="<?=$fecha?>">
            <input type="hidden" value="<?=$unidadescurriculares->uniid?>" name="uniid">    
          </form>
        <?php endif ?>