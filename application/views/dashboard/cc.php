        <?php if (!empty($titulo)): ?>
            <hr>
            <h1 class="text-center"><?=$titulo?></h1>
            <div class="row">
              <div class="col-sm-offset-3 col-sm-6">
                <?php if (!empty($output)): ?>
                  <?=$output?>
                <?php endif ?>
              </div>    
            </div>
        
        <?php else: ?>
            <div class="col-sm-8">
              
              <div id="carousel-dashboard" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?=base_url()?>assets/imagenes/4.jpg" class="img-responsive">
                  </div>
                  <div class="item">
                    <img src="<?=base_url()?>assets/imagenes/5.jpg" class="img-responsive">
                  </div>
                  <div class="item">
                    <img src="<?=base_url()?>assets/imagenes/6.jpg" class="img-responsive">
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
              
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>estudiantes" class="text-center thumbnail">
                    <h2>
                      <span class="glyphicon glyphicon-book"></span>
                    </h2>
                    <h5>Estudiantes</h5>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>citaciones" class="text-center thumbnail">
                    <h2>
                      <span class="glyphicon glyphicon-warning-sign"></span>
                    </h2>
                    <h5>Citaciones</h5>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>inscripciones" class="text-center thumbnail">
                    <h2>
                      <span class="glyphicon glyphicon-pencil"></span>
                    </h2>
                    <h5>Inscripciones</h5>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>inasistencias" class="text-center thumbnail">
                    <h2>
                      <span class="glyphicon glyphicon-th-list"></span>
                    </h2>
                    <h5>Inasistencias</h5>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>traslados" class="text-center thumbnail">
                    <h2>
                      <span class="glyphicon glyphicon-hand-down"></span>
                    </h2>
                    <h5>Estudiante de traslado</h5>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>notas" class="text-center thumbnail">
                    <h2>
                      <span class="glyphicon glyphicon-star"></span>
                    </h2>
                    <h5>Notas</h5>
                  </a>
                </div>
              </div>


              
            </div>
            
          </div>
        <?php endif ?>