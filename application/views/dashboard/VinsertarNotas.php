<?php if (!empty($titulo)): ?>
  <form action="<?=base_url()?>notas/procesarNotas" class="form" method="post">
    <hr>
    <div class="col-sm-12">
      <h2 class="text-center">Ingresar Notas</h2>
      <?php if (!empty($notas)): ?>
          <div class="col-sm-4">
            <table class="table">
              <thead>
                <tr>
                  <th colspan="2">
                    Año Escolar: <?=$anoescolar->anocodigova?>° (<?=$anoescolar->anonombreva?>)  <br>
                    Sección: <?=$seccion->seccodigova?><br>
                    Unidad Curricular: <?=$materia->uninombreva?> <br>
                    Lapso: <?=$lapso->lapnombreva?> - <?=$lapso->lapsoid?>°
                  </th>
                </tr>
                <tr>
                  <th width="75%">Estudiante</th>
                  <th width="25%">Nota</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($notas as $nota): ?>
                <tr>
                  <td><?=$nota->estcedulava?> <?=$nota->estnombreva?> <?=$nota->estapellidova?></td>
                  <td>
                    <input type="number" name="notas[]" id="<?=$nota->uniid?>" value="<?php 
                      switch ($lapso->lapsoid) {
                        case 1:
                          echo $nota->nota1 <> null ? $nota->nota1 : '';
                          break;
                        case 2:
                          echo $nota->nota2 <> null ? $nota->nota2 : '';
                          break;
                        case 3:
                          echo $nota->nota3 <> null ? $nota->nota3 : '';
                          break;
                      }
                     ?>" min="1" max="20" style="width:50px" required>
                    <input type="hidden" value="<?=$nota->estcodigodo?>" name="estudiantes[]">
                  </td>
                </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
      <?php endif ?>
      <div class="col-sm-3">
        <div>
          <button class="btn btn-primary">
            Guardar Notas
          </button>
        </div>
      </div>
    </div>
    <input type="hidden" value="<?=$lapso->lapsoid?>" name="lapso">
    <input type="hidden" value="<?=$materia->uniid?>" name="materia">
    <input type="hidden" value="<?=$periodo->perid?>" name="periodo">
    <input type="hidden" value="<?=$seccion->secid?>" name="seccion">
    <input type="hidden" value="<?=$anoescolar->anoid?>" name="anoescolar">
  </form>
<?php endif ?>