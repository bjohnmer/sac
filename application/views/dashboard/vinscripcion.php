        <?php if (!empty($titulo)): ?>
          <form action="<?=base_url()?>inscripcion/procesarInscripcion" method="post">
            <hr>
            <div class="col-sm-12">
              <h1 class="text-center"><?=$titulo?></h1>
              <?php if (!empty($mensaje)): ?>
                <div class="alert alert-<?=$clase?> fade in">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=$mensaje?>
                </div>
              <?php endif ?>
              <?php if (validation_errors()): ?>
              <div class="alert alert-warning fade in">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?=validation_errors()?>
              </div>
              <?php endif ?>
              <div class="col-sm-12">
                <div class="col-sm-12">&nbsp;</div>
                <div class="col-sm-6">
                  <div class="col-sm-12">
                    <h3>Representante</h3>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-12 control-label" for="nombre">Cédula</label>
                      <div class="col-sm-12">
                        <?=$estudiante->repcedulava?>
                      </div>
                    </div>

                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-12 control-label" for="nombre">Nombre</label>
                      <div class="col-sm-12">
                        <?=$estudiante->repapellidova?> <?=$estudiante->repnombreva?>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">&nbsp;</div>
                
                </div>
                
                <div class="col-sm-6">
                  <div class="col-sm-12">
                    <h3>Estudiante</h3>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-12 control-label" for="nombre">Cédula</label>
                      <div class="col-sm-12">
                        <?=$estudiante->estcedulava?>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-12 control-label">Nombre</label>
                      <div class="col-sm-12">
                        <?=$estudiante->estapellidova?> <?=$estudiante->estnombreva?>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-12">&nbsp;</div>
                  
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-12 control-label">Año escolar</label>
                    <div class="col-sm-12">
                      <?=$nanoescolar?> (<?=$anoescolar?>)
                    </div>
                  </div>
                  </div>
                </div>
              </div>
              
              <div class="col-sm-12">&nbsp;</div>
              
              <div class="well col-sm-5 caja-inscripcion">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="col-sm-12 control-label" for="materiasCursar">Unidades Curriculares a Cursar</label>
                    <div class="col-sm-12">
                      
                      <table class="table">
                        <input type="hidden" name="anoescolar" value="<?=$anoescolar?>">
                        <input type="hidden" name="cedula" value="<?=$estudiante->estcedulava?>">
                        <input type="hidden" name="periodo" value="<?=$this->session->userdata('perid')?>">
                        <input type="hidden" name="estudiante" value="<?=$estudiante->estcodigodo?>">
                        <input type="hidden" name="seccionAnterior" value="<?=$seccionAnterior?>">
                        <tbody>
                          <?php if (!empty($materiasAC)): ?>
                            <?php foreach ($materiasAC as $materia): ?>
                              <tr>
                                <input type="hidden" name="id_materiaAC[]" id="id_materia" value="<?=$materia->uniid?>">
                                <td><?=$materia->uninombreva?></td>
                              </tr>
                            <?php endforeach ?>
                          <?php endif ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                
              </div>
              
              <div class="col-sm-1">&nbsp;</div>
              <?php if (!empty($materiasAR)): ?>
              <div class="well col-sm-5 caja-inscripcion">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="col-sm-12 control-label" for="materiasArrastre">Unidades Curriculares de Arrastre</label>
                    <div class="col-sm-12">
                      <table class="table">
                        <tbody>
                            <?php foreach ($materiasAR as $materia): ?>
                              <tr>
                                <input type="hidden" name="id_materiaAR[]" id="id_materia" value="<?=$materia->uniid?>">
                                <td><?=$materia->uninombreva?></td>
                              </tr>
                            <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <?php endif ?>
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="well col-sm-12">
                    <div class="row">
                      <div class="col-sm-12">
                        <h3 class="text-center">Recaudos</h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-3">
                              <label>
                                <span style="color:red; font-size:1.2em">* </span><input type="checkbox" name="foto_estudiante" value="Sí"> Foto del Estudiante
                              </label>
                            </div>
                            <div class="col-sm-3">
                              <label>
                                <input type="checkbox" name="partidanac_estudiante" value="Sí"> Partida de Nacimiento
                              </label>
                            </div>
                            <div class="col-sm-3">
                              <label>
                                <input type="checkbox" name="copiacedula_estudiante" value="Sí"> Copia de Cédula del Estudiante
                              </label>
                            </div>
                            <div class="col-sm-3">
                              <label>
                                <input type="checkbox" name="constanciabc_estudiante" value="Sí"> Constancia de Buena Conducta
                              </label>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-3">
                            <label>
                              <input type="checkbox" name="notasc_estudiante" value="Sí"> Notas Certificadas
                            </label>
                          </div>
                          <div class="col-sm-3">
                            <label>
                              <input type="checkbox" name="foto_representante" value="Sí"> Foto del Representante
                            </label>
                          </div>
                          <div class="col-sm-6">
                            <label>
                              <span style="color:red; font-size:1.2em">* </span><input type="checkbox" name="copiac_representante" value="Sí"> Copia de Cédula del Representante
                            </label>
                          </div>
                        </div>
                        <br><br>
                        <div class="row">
                          <div class="col-sm-12">
                            <span style="color:red; font-size:1.2em">* Requerido</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <button class="btn btn-primary pull-right">Procesar Inscripción</button>
            </div>

            <div class="col-sm 12">&nbsp;</div>

          </form>
        <?php endif ?>