        <?php if (!empty($titulo)): ?>
            <hr>
            <div class="col-sm-12 text-center">
              <h1><?=$titulo?></h1>
              <div class="well col-sm-6  col-sm-offset-3">
                <?php if (!empty($mensaje)): ?>
                  <div class="alert alert-<?=$clase?> fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?=$mensaje?>
                  </div>
                <?php endif ?>


                <?php 
                  $hayEstudiantes = $this->db
                                      ->get('estudiantes')
                                      ->num_rows();
                 ?>
                <?php if ( $hayEstudiantes > 0  ): ?>


                <form role="form" class="form-horizontal" action="<?=base_url()?>traslados/insertarNotas" method="post">
                  <div class="form-group">
                    <label class="col-sm-5 control-label" for="estudiante">Estudiante</label>
                    <div class="col-sm-7">
                      <select name="estudiante" id="estudiante" class="form-control">
                        <?php if (!empty($estudiantes)): ?>
                          <?php foreach ($estudiantes as $estudiante): ?> 
                            <option value="<?=$estudiante->estcodigodo?>"><?=$estudiante->estcedulava?> - <?=$estudiante->estnombreva?> <?=$estudiante->estapellidova?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" for="anoescolar">Último Año Escolar estudiado</label>
                    <div class="col-sm-5">
                      <select name="anoescolar" id="anoescolar" class="form-control">
                        <?php if (!empty($anosescolares)): ?>
                          <?php foreach ($anosescolares as $anio): ?>
                            <option value="<?=$anio->anoid?>"><?=$anio->anonombreva?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" for="seccion">Sección</label>
                    <div class="col-sm-5">
                      <select name="seccion" id="seccion" class="form-control" readonly="readonly">
                        <?php if (!empty($secciones)): ?>
                          <?php foreach ($secciones as $seccion): ?>
                              <option value="<?=$seccion->secid?>"><?=$seccion->seccodigova?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" for="periodo">Periodo</label>
                    <div class="col-sm-5">
                      <select name="periodo" id="periodo" class="form-control">
                        <?php if (!empty($periodos)): ?>
                          <?php foreach ($periodos as $periodo): ?>
                              <option value="<?=$periodo->perid?>"><?=$periodo->percodigova?></option>
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                      <button class="btn btn-primary">Agregar Notas</button>
                    </div>
                  </div>
                </form>


                <?php else: ?>
                  <div class="alert alert-danger fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    No hay Estudiantes inscritos
                  </div>
                <?php endif ?>

              </div>
            </div>
            
        <?php endif ?>