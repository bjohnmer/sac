        <?php if (!empty($titulo)): ?>
            <hr>
            <h1 class="text-center"><?=$titulo?></h1>
            <?php 
              if (!empty($this->session->flashdata('clase'))) {
                $clase = $this->session->flashdata('clase');
              }
              if (!empty($this->session->flashdata('mensaje'))) {
                $mensaje = $this->session->flashdata('mensaje');
              }
            ?>
            <?php if (!empty($mensaje)): ?>
              <div class="row">
                <div class="col-sm-offset-2 col-sm-8">
                  <div class="alert alert-<?=$clase?> fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?=$mensaje?>
                  </div>
                </div>
              </div>
            <?php endif ?>

              <?php if ($this->uri->segment(1) == 'citaciones'): ?>
            <div class="row">
              <div class="col-sm-offset-5 col-sm-8">
                <?php if ( (empty( $this->uri->segment(2) ) || $this->uri->segment(2) == 'index' ) && $this->uri->segment(1) == 'citaciones' ): ?>
                <a href="<?=base_url()?>citaciones/cerradas" class="btn btn-success">Ver Cerradas</a>
                <?php elseif ( !empty( $this->uri->segment(2) ) && $this->uri->segment(2) == "cerradas" ): ?>
                <a href="<?=base_url()?>citaciones" class="btn btn-success">Ver Abiertas</a>
                <?php endif ?>
              </div>
            </div>
              <?php endif ?>
            <div class="row">
              <div class="col-sm-offset-2 col-sm-8">
                <?php if (!empty($output)): ?>
                  <?php if ($this->uri->segment(1) == 'estudiantes'): ?>
                  <?php $hayRep = $this->db->get('representantes')->num_rows() ?>
                  <?php if ($hayRep > 0): ?>
                    <?=$output?>
                  <?php else: ?>
                    <div class="alert alert-danger fade in">
                      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      No hay Representantes registrados -> 
                    <a href="<?=base_url()?>representantes" class="btn btn-danger">Ir a Representantes</a>
                    </div>
                  <?php endif ?>
                  <?php else: ?>
                    <?php if ( $this->uri->segment(1) == 'citaciones' ): ?>
                      
                      <?php 
                        
                        $hayInscritos = $this->db
                                            ->where('periodo', $this->session->userdata('perid'))
                                            ->get('notas')
                                            ->num_rows();
                       ?>
                      <?php if ( $hayInscritos > 0  ): ?>
                        <?=$output?>
                      <?php else: ?>
                        <div class="alert alert-danger fade in">
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                          No hay Estudiantes inscritos en este periodo
                        </div>
                      <?php endif ?>
                    <?php else: ?>
                      <?=$output?>
                    <?php endif ?>
                  <?php endif ?>
                <?php endif ?>
              </div>    
            </div>

        <?php else: ?>
            <div class="col-sm-8">
              
              <div id="carousel-dashboard" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?=base_url()?>assets/imagenes/4.jpg" class="img-responsive">
                  </div>
                  <div class="item">
                    <img src="<?=base_url()?>assets/imagenes/5.jpg" class="img-responsive">
                  </div>
                  <div class="item">
                    <img src="<?=base_url()?>assets/imagenes/6.jpg" class="img-responsive">
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
              </div>
            </div>
            <div class="col-sm-4">
            <?php 
              $per = $this->db->where("perestatusen","Activo")->get("periodos")->row(0);
            ?>
            <?php if ( $per ): ?>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>estudiantes" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-user"></span>
                    </h3>
                    <h6>Estudiantes</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>citaciones" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-bullhorn"></span>
                    </h3>
                    <h6>Citaciones</h6>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>inscripcion" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-pencil"></span>
                    </h3>
                    <h6>Inscripciones</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>inasistencias" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-check"></span>
                    </h3>
                    <h6>Inasistencias</h6>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>traslados" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-hand-down"></span>
                    </h3>
                    <h6>Estudiante de traslado</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>notas" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-star"></span>
                    </h3>
                    <h6>Notas</h6>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <a href="<?=base_url()?>reportes/boletin" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-list-alt"></span>
                    </h3>
                    <h6>Boletín</h6>
                  </a>
                </div>
                <div class="col-sm-6">
                  <a href="<?=base_url()?>secciones/cs" class="text-center thumbnail">
                    <h3>
                      <span class="glyphicon glyphicon-random"></span>
                    </h3>
                    <h6>Cambio de Sección</h6>
                  </a>
                </div>
              </div>
            <?php endif; ?>

              
            </div>
            
          </div>
        <?php endif ?>