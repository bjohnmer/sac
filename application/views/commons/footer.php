          <hr>
          <footer>
            <div class="content">
              <div class="row">
                <div class="col-sm-12">
                  <p>
                    <small>
                      Liceo Bolivariano "Rafael Rangel" 
                      | Sistema Academico 
                      | Periodo: <?=$this->session->userdata("periodoActivo")?>
                      | Usuario: <?=$this->session->userdata("admnivelen")?>
                      | Módulo: 
                      <?php 
                        switch ($this->uri->segment(1)) {
                          case 'periodos':
                            $ret = "Periodos Escolares";
                            break;
                          case 'ae':
                            $ret = "Años Escolares";
                            break;
                          case 'lapsos':
                            $ret = "lapsos";
                            break;
                          case 'secciones':
                            $ret = "Secciones";
                            break;
                          case 'uc':
                            $ret = "Unidades Curriculares";
                            break;
                          case 'docentes':
                            $ret = "Docentes";
                            break;
                          case 'representantes':
                            $ret = "Representantes";
                            break;
                          case 'estudiantes':
                            $ret = "Estudiantes";
                            break;
                          case 'citaciones':
                            $ret = "Citaciones";
                            break;
                          case 'inscripcion':
                            $ret = "Inscripción";
                            break;
                          case 'inasistencias':
                            $ret = "Inasistencias";
                            break;
                          case 'traslados':
                            $ret = "Traslados";
                            break;
                          case 'notas':
                            $ret = "Notas";
                            break;
                          case 'usuarios':
                            $ret = "Usuarios";
                            break;

                          default:
                            $ret = "Inicio";
                            break;
                        }
                        if ($this->uri->segment(1) == "reportes" && $this->uri->segment(2) == "ii") 
                        {
                          $ret = "Inscripción";
                        }
                        if ($this->uri->segment(1) == "secciones" && $this->uri->segment(2) == "cs") 
                        {
                          $ret = "Cambio de Sección";
                        }
                        echo $ret;
                      ?>
                      | Derechos Reservados <?=date("Y")?>
                    </small> 
                  </p>
                </div>
              </div>
            </div>
          </footer>

        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <?php if (!empty($js_files)): ?>
      <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
      <?php endforeach; ?>
    <?php else: ?>
      <script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js"></script>
      
      <script src="<?=base_url()?>assets/js/jquery-ui-1.10.4.custom.min.js"></script>
    
   
    <?php endif ?>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/holder.js"></script>
    <?php if (!empty($this->uri->segment(1))): ?>
      <?php if ($this->uri->segment(1) == "representantes"): ?>
        <script src="<?=base_url()?>assets/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="<?=base_url()?>assets/js/appR.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "estudiantes"): ?>
        <script src="<?=base_url()?>assets/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="<?=base_url()?>assets/js/appE.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "docentes"): ?>
        <script src="<?=base_url()?>assets/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="<?=base_url()?>assets/js/appD.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "traslados"): ?>
        <script src="<?=base_url()?>assets/js/app.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "uc"): ?>
        <script src="<?=base_url()?>assets/js/appuc.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "inasistencias" || $this->uri->segment(1) == "notas"): ?>
        <script src="<?=base_url()?>assets/js/appI.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "periodos"): ?>
        <script src="<?=base_url()?>assets/js/appP.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "ae"): ?>
        <script src="<?=base_url()?>assets/js/appA.js"></script>
      <?php endif ?>
       <?php if ($this->uri->segment(1) == "secciones"): ?>
        <script src="<?=base_url()?>assets/js/appS.js"></script>
      <?php endif ?>
       <?php if ($this->uri->segment(1) == "lapsos"): ?>
        <script src="<?=base_url()?>assets/js/appL.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "citaciones"): ?>
        <script src="<?=base_url()?>assets/js/appC.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "parametros"): ?>
        <script src="<?=base_url()?>assets/js/appPar.js"></script>
      <?php endif ?>
      <?php if ($this->uri->segment(1) == "reportes"): ?>
        <script src="<?=base_url()?>assets/js/appRN.js"></script>
      <?php endif ?>
    <?php endif ?>
    
  </body>
</html>