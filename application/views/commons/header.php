<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>..::Sistema Académico del Liceo Bolivariano Rafael Rangel::..</title>

    <?php if (!empty($css_files)): ?>
      <?php foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
      <?php endforeach; ?>
    <?php endif ?>

    <!-- Bootstrap -->
    <link href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/salbrr.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/blitzer/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="contenido">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-5">
            <img src="<?=base_url()?>assets/imagenes/tope_me.png" alt="" class="img-responsive">
          </div>
          <div class="col-sm-4 titulo">
            <h4>Liceo Bolivariano <br> <strong>Rafael Rangel</strong> </h4>
          </div>
          <div class="col-sm-3">
            <img src="<?=base_url()?>assets/imagenes/tope_derecha.jpg" alt="" class="img-responsive">
          </div>
          <h5>&nbsp;</h5>
          <div class="col-sm-12">
            <ul class="nav nav-pills">
              <li class="<?=$this->uri->segment(1) == 'backend' ? 'active' : ''?>"><a href="<?=base_url()?>backend">Inicio</a></li>
              <li class="dropdown <?php
                switch ($this->uri->segment(1)) {
                  case 'periodos':
                  case 'ae':
                  case 'lapsos':
                  case 'secciones':
                  case 'uc':
                  case 'docentes':
                  case 'representantes':
                    echo "active";
                    break;
                }
              ?>">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                  Registro Académico <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                  <?php 

                    $hayParams = $this->db->get('parametros')->result();

                   ?>
                  <?php if ($hayParams): ?>
                    <li><a href="<?=base_url()?>periodos">Periodos Escolares</a></li>
                  <?php endif ?>
                  
                  <?php 
                    $p = $this->db->where("perestatusen","Activo")->get("periodos")->result();
                  ?>
                  <?php if ( $p ): ?>
                  <li><a href="<?=base_url()?>ae">Años Escolares</a></li>
                  <li class="divider"></li>
                  <li><a href="<?=base_url()?>lapsos">Lapsos</a></li>
                  <li><a href="<?=base_url()?>secciones">Secciones</a></li>
                  <li><a href="<?=base_url()?>seccionales">Seccionales</a></li>
                  <li class="divider"></li>
                  <li><a href="<?=base_url()?>uc">Unidades Curriculares</a></li>
                  <li class="divider"></li>
                  <li><a href="<?=base_url()?>docentes">Docentes</a></li>
                  <li class="divider"></li>
                  <li><a href="<?=base_url()?>representantes">Representantes</a></li>
                  <?php endif ?>
                </ul>
              </li>
                  <?php if ( $p ): ?>
              <li class="dropdown <?php
                switch ($this->uri->segment(1)) {
                  case 'estudiantes':
                  case 'citaciones':
                    echo "active";
                    break;
                }
              ?>">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                  Estudiante <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="<?=base_url()?>estudiantes">Estudiantes</a></li>
                  <li class="divider"></li>
                  <li><a href="<?=base_url()?>citaciones">Citaciones</a></li>
                </ul>
              </li>
              <li class="dropdown <?php
                switch ($this->uri->segment(1)) {
                  case 'inscripcion':
                  case 'inasistencias':
                  case 'traslado':
                  case 'notas':
                    echo "active";
                    break;
                }
              ?>">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                  Procesos <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="<?=base_url()?>inscripcion">Inscripción</a></li>
                  <li><a href="<?=base_url()?>inasistencias">Inasistencias</a></li>
                  <li class="divider"></li>
                  <?php if ($this->session->userdata('admnivelen') != "Operador") { ?>
                  <li><a href="<?=base_url()?>secciones/cs">Cambio de Sección</a></li>
                  <?php } ?>
                  <li><a href="<?=base_url()?>traslados">Incluir notas a estudiante de traslado</a></li>
                  <li class="divider"></li>
                  <li><a href="<?=base_url()?>notas">Notas</a></li>
                </ul>
              </li>
              <li>
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                  Reportes <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<?=base_url()?>reportes/nomina">Nómina de Estudiantes</a></li>
                    <li class="divider"></li>
                    <li><a href="<?=base_url()?>reportes/boletin">Boletín</a></li>
                </li>
                </ul>
              </li>
                  <?php endif ?>
              
              <ul class="nav nav-pills navbar-right">
                <?php if ($this->session->userdata('admnivelen') != "Operador") { ?>
                <li class="dropdown <?php
                switch ($this->uri->segment(1)) {
                  case 'usuarios':
                    echo "active";
                    break;
                }
              ?>">
                  <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    Mantenimiento <span class="caret"></span>
                  </a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="<?=base_url()?>usuarios">Usuarios <span class="glyphicon glyphicon-user"></span></a></li>
                
                <?php if ($this->session->userdata('admnivelen') == "Administrador") : ?>
                    <li><a href="<?=base_url()?>parametros">Parámetros Generales <span class="glyphicon glyphicon-cog"></span></a></li>
                <?php endif;?>
                
                    <li><a href="<?=base_url()?>acercade">Acerca de <span class="glyphicon glyphicon-info-sign"></span></a></li>

                    <li><a href="<?=base_url()?>assets/documentos/manual.pdf" target="_blank">Manual de Usuario <span class="glyphicon glyphicon-book"></span></a></li>

                  </ul>
                </li>
                <?php } ?>
                <li class="dropdown <?php
                switch ($this->uri->segment(1)) {
                  case 'cc':
                    echo "active";
                    break;
                }
              ?>">
                  <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                    <?=$this->session->userdata("admnombreva")?> <span class="caret"></span>
                  </a>
                  <ul role="menu" class="dropdown-menu">
                    <li class="bg-info">
                        <small>&nbsp;&nbsp;Usuario <?=$this->session->userdata("admnivelen")?>&nbsp;&nbsp;</small>
                    </li>
                    <li><a href="<?=base_url()?>cc/index/edit/<?=$this->session->userdata("admid")?>">Cambiar Clave</a></li>
                  </ul>
                </li>
                <a href="<?=base_url()?>backend/logout"  class="btn btn-danger btn-nav">Cerrar Sesión <span class="glyphicon glyphicon-log-out"></span></a>
              </ul>
            </ul>
          </div>
          <div class="col-sm-12">
            <?php 
              $per = $this->db->where("perestatusen","Activo")->get("periodos")->row(0);

            ?>
            <?php if ($hayParams): ?>
              <?php if ( !$per ): ?>
              <div class="row">
                
                <div class="col-sm-12">
                  
                  <div class="alert alert-warning fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Advertencia:</h4>
                    <p><strong><?=$this->session->userdata("periodoActivo")?>.</strong> Debe activar un periodo en el menú <strong>Registro Académico > Periodos Escolares</strong></p>
                    <!-- <p> -->
                      <a class="btn btn-warning btn-sm" href="<?=base_url()?>periodos">Activar Periodo Escolar</a>
                    <!-- </p> -->
                  </div>
                  
                </div>

              </div>
              <?php endif ?>
            <?php else: ?>
              <div class="row">
                
                <div class="col-sm-12">
                  
                  <div class="alert alert-info fade in">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Información:</h4>
                    <p>En El módulo de <strong>Mantenimiento -> Parámetros Generales</strong> Dele clic a la opción <img src="<?=base_url()?>assets/imagenes/anadir_reg.jpg">
                    </p>
                  </div>
                  
                </div>

              </div>
            <?php endif ?>
            <h3 class="text-center">
              Sistema Académico para el Control y Registro de Estudiantes y Docentes
            </h3>
          </div>
        </div>
        <div class="row">