<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datemanager
{
  public function date2mySQL($date)
  {
    $fecha = explode("/", $date);
    return $fecha[2]."-".$fecha[1]."-".$fecha[0];
  }

  public function date2normal($date)
  {
    $fecha = explode("-", $date);
    return $fecha[2]."/".$fecha[1]."/".$fecha[0];
  }

  public function is_date($fecha)
  {
    $f = explode("/", $fecha);
    if (checkdate($f[1], $f[0], $f[2])) {
      return true;
    }
    else
    {
      return false;
    }
  }

  public function edad($fn = null)
  {
    $fecha = explode("-", $fn);
    $edad = date('Y') - (int)$fecha[0];
    if ((int)$fecha[1] > date('m')) 
    {
      $edad--;
    } elseif ((int)$fecha[1] == date('m')) {

      if ((int)$fecha[2] > date('d') ) {
        $edad--;
      }
    }
    return $edad;
  }
  
}