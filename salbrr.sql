-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 02, 2015 at 10:08 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `salbrr`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `admid` int(11) NOT NULL AUTO_INCREMENT,
  `admloginva` varchar(15) NOT NULL,
  `admclaveva` varchar(255) NOT NULL,
  `admclavecva` varchar(255) NOT NULL,
  `admnombreva` varchar(50) NOT NULL,
  `admnivelen` enum('Administrador','Jefe de Seccional','Operador') NOT NULL DEFAULT 'Operador',
  `seccional_id` int(11) DEFAULT NULL,
  `admestatusen` enum('Activo','Inactivo') NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`admid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Administrador (Usuario del Sistema)' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `administrador`
--

INSERT INTO `administrador` (`admid`, `admloginva`, `admclaveva`, `admclavecva`, `admnombreva`, `admnivelen`, `seccional_id`, `admestatusen`) VALUES
(1, 'Leonard', 'oc4CeRbstm1qzjkPmScZvFu2c5uE7NCE5wYV/x/JsV1ujONCUYVK7d8QS0fUXAj9oXAp7qRPFPUT/dFz+Q6/TQ==', 'oc4CeRbstm1qzjkPmScZvFu2c5uE7NCE5wYV/x/JsV1ujONCUYVK7d8QS0fUXAj9oXAp7qRPFPUT/dFz+Q6/TQ==', 'Leonard', 'Administrador', 0, 'Activo'),
(2, 'bjohnmer', '0BkdnyVJrnyaLS2/UUQUa56ebFxNisAsgFRI8T8rQWcY/HYhsTpuJXPgB2CELdwnONmSFqy6A8sV+n7gfMOe2w==', '0BkdnyVJrnyaLS2/UUQUa56ebFxNisAsgFRI8T8rQWcY/HYhsTpuJXPgB2CELdwnONmSFqy6A8sV+n7gfMOe2w==', 'Johnmer Bencomo', 'Jefe de Seccional', 5, 'Activo');

-- --------------------------------------------------------

--
-- Table structure for table `anosescolares`
--

CREATE TABLE IF NOT EXISTS `anosescolares` (
  `anoid` int(11) NOT NULL AUTO_INCREMENT,
  `anocodigova` varchar(2) NOT NULL DEFAULT '',
  `anonombreva` varchar(15) NOT NULL,
  `anoestatusen` enum('Activo','Inactivo') NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`anoid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Año Escolar (7, 8, 9...)' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `anosescolares`
--

INSERT INTO `anosescolares` (`anoid`, `anocodigova`, `anonombreva`, `anoestatusen`) VALUES
(1, '1', 'Primero', 'Activo'),
(2, '2', 'Segundo', 'Activo'),
(3, '3', 'Tercero', 'Activo'),
(4, '4', 'Cuarto', 'Activo'),
(5, '5', 'Quinto', 'Activo'),
(6, '6', 'Sexto', 'Inactivo');

-- --------------------------------------------------------

--
-- Table structure for table `citaciones`
--

CREATE TABLE IF NOT EXISTS `citaciones` (
  `citcodigoin` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cittipodo` int(11) NOT NULL,
  `citidestudiantedo` int(11) NOT NULL,
  `citobservacionva` text NOT NULL,
  `citfechadate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `citestatusen` enum('Abierta','Cerrada') NOT NULL DEFAULT 'Abierta',
  `fechacierre` date DEFAULT NULL,
  `observacion` text,
  PRIMARY KEY (`citcodigoin`),
  KEY `citcodigoestudiantedo` (`citidestudiantedo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Citacion al Representante del Estudiante' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `docentes`
--

CREATE TABLE IF NOT EXISTS `docentes` (
  `doccodigodo` double NOT NULL AUTO_INCREMENT,
  `doccedulava` varchar(10) NOT NULL,
  `docnombreva` varchar(75) NOT NULL,
  `docapellidova` varchar(75) NOT NULL,
  `docsexoen` enum('Masculino','Femenino') NOT NULL DEFAULT 'Masculino',
  `docdireccionva` varchar(255) NOT NULL,
  `doctelefono1va` varchar(12) NOT NULL,
  `doctelefono2va` varchar(12) DEFAULT NULL,
  `docprofesionva` varchar(30) NOT NULL,
  `docfechanacda` date NOT NULL,
  PRIMARY KEY (`doccodigodo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Docente (Profesor)' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `estudiantes`
--

CREATE TABLE IF NOT EXISTS `estudiantes` (
  `estcodigodo` double NOT NULL AUTO_INCREMENT,
  `estcedulava` varchar(10) NOT NULL,
  `estnombreva` varchar(75) NOT NULL,
  `estapellidova` varchar(75) NOT NULL,
  `estfechanacda` date NOT NULL,
  `estsexoen` enum('Masculino','Femenino') NOT NULL DEFAULT 'Masculino',
  `estdireccionva` varchar(255) NOT NULL,
  `esttelefonova` varchar(11) DEFAULT NULL,
  `representante` int(11) NOT NULL,
  PRIMARY KEY (`estcodigodo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Estudiantes (Alumno)' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `estudiantes`
--

INSERT INTO `estudiantes` (`estcodigodo`, `estcedulava`, `estnombreva`, `estapellidova`, `estfechanacda`, `estsexoen`, `estdireccionva`, `esttelefonova`, `representante`) VALUES
(1, '10123456', 'Maria Fernanda', 'Bellorin Castro', '1999-03-04', 'Femenino', 'Av. Bolivar c/calle 10, Valera                    ', '04168987655', 1),
(2, '20123456', 'Ana Maria', 'Perez', '1999-03-04', 'Femenino', 'Carvajal                        ', '02712213333', 1),
(3, '1234', 'wwww', 'Perez', '1999-03-04', 'Masculino', '    1w1w                    ', '11', 1),
(4, '4444', 'rr', 'rrr', '1999-03-04', 'Masculino', ' r4r4r                       ', '444', 1),
(5, '5555', 'd', 'd', '1999-03-04', 'Masculino', 'd5                        ', '5', 1),
(6, '6666', 'eeee', 'eeeee', '1999-03-04', 'Masculino', 'd66666             ', '566666', 1),
(7, '3333', 'TRES TRES', 'TRES', '2006-01-17', 'Masculino', 'KZDGHCKDSF', '76776568568', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inasistenciasestudiante`
--

CREATE TABLE IF NOT EXISTS `inasistenciasestudiante` (
  `inasistenciaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria de la Tabla',
  `inaidestudiantedo` int(10) unsigned NOT NULL COMMENT 'Clave concatenada con la tabla estudiantes',
  `inafechatt` date NOT NULL COMMENT 'Fecha de las inasistencias',
  `inaobservacionva` text NOT NULL COMMENT 'Observación de las inasistencias',
  `perid` int(11) NOT NULL,
  `secid` int(11) NOT NULL,
  `anoid` int(11) NOT NULL,
  `uniid` int(11) NOT NULL,
  PRIMARY KEY (`inasistenciaid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Inasistencias de los Estudiantes' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lapsos`
--

CREATE TABLE IF NOT EXISTS `lapsos` (
  `lapsoid` int(11) NOT NULL AUTO_INCREMENT,
  `lapnombreva` varchar(15) NOT NULL,
  `lapestatusen` enum('Activo','Inactivo') NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`lapsoid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Lapso Académico (1, 2, 3...)' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lapsos`
--

INSERT INTO `lapsos` (`lapsoid`, `lapnombreva`, `lapestatusen`) VALUES
(1, 'Primero', 'Activo'),
(2, 'Segundo', 'Activo'),
(3, 'Tercero', 'Activo');

-- --------------------------------------------------------

--
-- Table structure for table `notas`
--

CREATE TABLE IF NOT EXISTS `notas` (
  `notaid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria de la tabla de notas',
  `estudiante` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla estudiantes',
  `periodo` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla de periodos',
  `seccion` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla secciones',
  `anoescolar` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla de anosescolares',
  `materia` int(11) NOT NULL COMMENT 'Clave concatenada con la tabla de materias',
  `nota1` int(11) DEFAULT NULL,
  `nota2` int(11) DEFAULT NULL,
  `nota3` int(11) DEFAULT NULL,
  `notad` int(11) NOT NULL COMMENT 'Nota del alumno',
  PRIMARY KEY (`notaid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Información de las Notas de Todos los Alumnos' AUTO_INCREMENT=90 ;

--
-- Dumping data for table `notas`
--

INSERT INTO `notas` (`notaid`, `estudiante`, `periodo`, `seccion`, `anoescolar`, `materia`, `nota1`, `nota2`, `nota3`, `notad`) VALUES
(84, 7, 5, 7, 1, 1, NULL, NULL, NULL, 0),
(85, 7, 5, 7, 1, 2, NULL, NULL, NULL, 0),
(86, 7, 5, 7, 1, 3, NULL, NULL, NULL, 0),
(87, 7, 5, 7, 1, 4, NULL, NULL, NULL, 0),
(88, 7, 5, 7, 1, 5, NULL, NULL, NULL, 0),
(89, 7, 5, 7, 1, 20, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parametros`
--

CREATE TABLE IF NOT EXISTS `parametros` (
  `paramid` int(11) NOT NULL AUTO_INCREMENT,
  `paramcs` int(11) NOT NULL,
  `paramlimite` tinyint(3) NOT NULL,
  `paramtina` tinyint(3) NOT NULL,
  `paramperiodo` smallint(5) NOT NULL,
  `paramcmr` tinyint(4) NOT NULL COMMENT 'Cantidad de materias para que el estudiante repita',
  PRIMARY KEY (`paramid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `parametros`
--

INSERT INTO `parametros` (`paramid`, `paramcs`, `paramlimite`, `paramtina`, `paramperiodo`, `paramcmr`) VALUES
(1, 6, 2, 2, 2010, 2);

-- --------------------------------------------------------

--
-- Table structure for table `periodos`
--

CREATE TABLE IF NOT EXISTS `periodos` (
  `perid` int(11) NOT NULL AUTO_INCREMENT,
  `percodigova` varchar(15) NOT NULL DEFAULT '',
  `pernombreva` varchar(15) NOT NULL,
  `perestatusen` enum('Activo','Inactivo') NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`perid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Periodo Escolar (2011-2012)' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `periodos`
--

INSERT INTO `periodos` (`perid`, `percodigova`, `pernombreva`, `perestatusen`) VALUES
(1, '2010-2011', '2010-2011', 'Inactivo'),
(2, '2011-2012', '2011-2012', 'Inactivo'),
(3, '2012-2013', '2012-2013', 'Inactivo'),
(4, '2013-2014', '2013-2014', 'Inactivo'),
(5, '2014-2015', '2014-2015', 'Activo'),
(6, '2015-2016', '2015-2016', 'Inactivo');

-- --------------------------------------------------------

--
-- Table structure for table `recaudos`
--

CREATE TABLE IF NOT EXISTS `recaudos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estudiante_id` int(11) NOT NULL,
  `periodo_id` int(11) NOT NULL,
  `foto_estudiante` enum('Sí','No') NOT NULL DEFAULT 'No',
  `partidanac_estudiante` enum('Sí','No') NOT NULL DEFAULT 'No',
  `copiacedula_estudiante` enum('Sí','No') NOT NULL DEFAULT 'No',
  `constanciabc_estudiante` enum('Sí','No') NOT NULL DEFAULT 'No',
  `notasc_estudiante` enum('Sí','No') NOT NULL DEFAULT 'No',
  `foto_representante` enum('Sí','No') NOT NULL DEFAULT 'No',
  `copiac_representante` enum('Sí','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `recaudos`
--

INSERT INTO `recaudos` (`id`, `estudiante_id`, `periodo_id`, `foto_estudiante`, `partidanac_estudiante`, `copiacedula_estudiante`, `constanciabc_estudiante`, `notasc_estudiante`, `foto_representante`, `copiac_representante`) VALUES
(1, 1, 5, 'Sí', '', '', '', '', '', 'Sí'),
(2, 4, 5, 'Sí', '', '', '', '', '', 'Sí'),
(4, 3, 5, 'Sí', '', '', '', '', '', 'Sí'),
(5, 5, 5, 'Sí', '', '', '', '', '', 'Sí'),
(6, 6, 5, 'Sí', '', '', '', '', '', 'Sí'),
(7, 7, 5, 'Sí', '', '', '', '', '', 'Sí');

-- --------------------------------------------------------

--
-- Table structure for table `representantes`
--

CREATE TABLE IF NOT EXISTS `representantes` (
  `repid` int(11) NOT NULL AUTO_INCREMENT,
  `repcedulava` varchar(10) NOT NULL,
  `repnombreva` varchar(75) NOT NULL,
  `repapellidova` varchar(75) NOT NULL,
  `repsexoen` enum('Masculino','Femenino') NOT NULL DEFAULT 'Masculino',
  `repdireccionva` varchar(255) NOT NULL,
  `reptelefono1va` varchar(11) NOT NULL,
  `reptelefono2va` varchar(11) DEFAULT NULL,
  `repprofesionva` varchar(50) DEFAULT NULL,
  `repedocivilen` enum('Soltero','Casado','Divorciado','Viudo') NOT NULL DEFAULT 'Soltero',
  `repfechanacda` date NOT NULL,
  PRIMARY KEY (`repid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `representantes`
--

INSERT INTO `representantes` (`repid`, `repcedulava`, `repnombreva`, `repapellidova`, `repsexoen`, `repdireccionva`, `reptelefono1va`, `reptelefono2va`, `repprofesionva`, `repedocivilen`, `repfechanacda`) VALUES
(1, '5315888', 'Pedro Jose', 'Manzanilla Pere', 'Masculino', 'Trujillo, Calle la Paz            ', '04147657632', '02726353422', 'Lic en Deporte y Rec', 'Casado', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `seccionales`
--

CREATE TABLE IF NOT EXISTS `seccionales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Seccionales de la institución' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `seccionales`
--

INSERT INTO `seccionales` (`id`, `descripcion`) VALUES
(1, 'Seccional 1'),
(2, 'Seccional 2'),
(3, 'Seccional 3'),
(4, 'Seccional 4'),
(5, 'Seccional 5'),
(6, 'Seccional 6');

-- --------------------------------------------------------

--
-- Table structure for table `secciones`
--

CREATE TABLE IF NOT EXISTS `secciones` (
  `secid` int(11) NOT NULL AUTO_INCREMENT,
  `seccodanoescolarva` varchar(2) NOT NULL,
  `seccodigova` enum('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','Traslado') NOT NULL DEFAULT 'A',
  `periodo` int(11) NOT NULL,
  `secestatusen` enum('Activo','Inactivo') NOT NULL DEFAULT 'Activo',
  `seccional_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`secid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Seccion (7A, 7B, 8C,...)' AUTO_INCREMENT=163 ;

--
-- Dumping data for table `secciones`
--

INSERT INTO `secciones` (`secid`, `seccodanoescolarva`, `seccodigova`, `periodo`, `secestatusen`, `seccional_id`) VALUES
(1, '1', 'Traslado', 5, 'Activo', 0),
(2, '2', 'Traslado', 5, 'Activo', 0),
(3, '3', 'Traslado', 5, 'Activo', 0),
(4, '4', 'Traslado', 5, 'Activo', 0),
(5, '5', 'Traslado', 5, 'Activo', 0),
(6, '6', 'Traslado', 5, 'Inactivo', 0),
(7, '1', 'A', 5, 'Activo', 0),
(8, '2', 'A', 5, 'Activo', 0),
(9, '3', 'A', 5, 'Activo', 0),
(10, '4', 'A', 5, 'Activo', 0),
(11, '5', 'A', 5, 'Activo', 5),
(12, '6', 'A', 5, 'Inactivo', 0),
(13, '1', 'B', 5, 'Activo', 5),
(14, '2', 'B', 5, 'Activo', 0),
(15, '3', 'B', 5, 'Activo', 0),
(16, '4', 'B', 5, 'Activo', 0),
(17, '5', 'B', 5, 'Activo', 0),
(18, '6', 'B', 5, 'Inactivo', 0),
(19, '1', 'C', 5, 'Activo', 0),
(20, '2', 'C', 5, 'Activo', 0),
(21, '3', 'C', 5, 'Activo', 0),
(22, '4', 'C', 5, 'Activo', 0),
(23, '5', 'C', 5, 'Activo', 5),
(24, '6', 'C', 5, 'Inactivo', 0),
(25, '1', 'D', 5, 'Activo', 0),
(26, '2', 'D', 5, 'Activo', 0),
(27, '3', 'D', 5, 'Activo', 0),
(28, '4', 'D', 5, 'Activo', 0),
(29, '5', 'D', 5, 'Activo', 0),
(30, '6', 'D', 5, 'Inactivo', 0),
(31, '1', 'E', 5, 'Activo', 0),
(32, '2', 'E', 5, 'Activo', 0),
(33, '3', 'E', 5, 'Activo', 0),
(34, '4', 'E', 5, 'Activo', 0),
(35, '5', 'E', 5, 'Activo', 0),
(36, '6', 'E', 5, 'Inactivo', 0),
(37, '1', 'F', 5, 'Activo', 0),
(38, '2', 'F', 5, 'Activo', 0),
(39, '3', 'F', 5, 'Activo', 0),
(40, '4', 'F', 5, 'Activo', 5),
(41, '5', 'F', 5, 'Activo', 0),
(42, '6', 'F', 5, 'Inactivo', 0),
(43, '1', 'G', 5, 'Activo', 0),
(44, '2', 'G', 5, 'Activo', 0),
(45, '3', 'G', 5, 'Activo', 0),
(46, '4', 'G', 5, 'Activo', 0),
(47, '5', 'G', 5, 'Activo', 0),
(48, '6', 'G', 5, 'Inactivo', 0),
(49, '1', 'H', 5, 'Activo', 0),
(50, '2', 'H', 5, 'Activo', 0),
(51, '3', 'H', 5, 'Activo', 0),
(52, '4', 'H', 5, 'Activo', 5),
(53, '5', 'H', 5, 'Activo', 0),
(54, '6', 'H', 5, 'Inactivo', 0),
(55, '1', 'I', 5, 'Activo', 0),
(56, '2', 'I', 5, 'Activo', 0),
(57, '3', 'I', 5, 'Activo', 0),
(58, '4', 'I', 5, 'Activo', 5),
(59, '5', 'I', 5, 'Activo', 0),
(60, '6', 'I', 5, 'Inactivo', 0),
(61, '1', 'J', 5, 'Activo', 0),
(62, '2', 'J', 5, 'Activo', 0),
(63, '3', 'J', 5, 'Activo', 0),
(64, '4', 'J', 5, 'Activo', 5),
(65, '5', 'J', 5, 'Activo', 0),
(66, '6', 'J', 5, 'Inactivo', 0),
(67, '1', 'K', 5, 'Activo', 0),
(68, '2', 'K', 5, 'Activo', 0),
(69, '3', 'K', 5, 'Activo', 0),
(70, '4', 'K', 5, 'Activo', 0),
(71, '5', 'K', 5, 'Activo', 0),
(72, '6', 'K', 5, 'Inactivo', 0),
(73, '1', 'L', 5, 'Activo', 0),
(74, '2', 'L', 5, 'Activo', 0),
(75, '3', 'L', 5, 'Activo', 0),
(76, '4', 'L', 5, 'Activo', 0),
(77, '5', 'L', 5, 'Activo', 0),
(78, '6', 'L', 5, 'Inactivo', 0),
(79, '1', 'M', 5, 'Activo', 0),
(80, '2', 'M', 5, 'Activo', 0),
(81, '3', 'M', 5, 'Activo', 0),
(82, '4', 'M', 5, 'Activo', 5),
(83, '5', 'M', 5, 'Activo', 0),
(84, '6', 'M', 5, 'Inactivo', 0),
(85, '1', 'N', 5, 'Activo', 0),
(86, '2', 'N', 5, 'Activo', 0),
(87, '3', 'N', 5, 'Activo', 0),
(88, '4', 'N', 5, 'Activo', 0),
(89, '5', 'N', 5, 'Activo', 0),
(90, '6', 'N', 5, 'Inactivo', 0),
(91, '1', 'O', 5, 'Activo', 0),
(92, '2', 'O', 5, 'Activo', 0),
(93, '3', 'O', 5, 'Activo', 0),
(94, '4', 'O', 5, 'Activo', 0),
(95, '5', 'O', 5, 'Activo', 0),
(96, '6', 'O', 5, 'Inactivo', 0),
(97, '1', 'P', 5, 'Activo', 0),
(98, '2', 'P', 5, 'Activo', 0),
(99, '3', 'P', 5, 'Activo', 0),
(100, '4', 'P', 5, 'Activo', 0),
(101, '5', 'P', 5, 'Activo', 0),
(102, '6', 'P', 5, 'Inactivo', 0),
(103, '1', 'Q', 5, 'Activo', 0),
(104, '2', 'Q', 5, 'Activo', 0),
(105, '3', 'Q', 5, 'Activo', 0),
(106, '4', 'Q', 5, 'Activo', 0),
(107, '5', 'Q', 5, 'Activo', 0),
(108, '6', 'Q', 5, 'Inactivo', 0),
(109, '1', 'R', 5, 'Activo', 0),
(110, '2', 'R', 5, 'Activo', 0),
(111, '3', 'R', 5, 'Activo', 0),
(112, '4', 'R', 5, 'Activo', 0),
(113, '5', 'R', 5, 'Activo', 0),
(114, '6', 'R', 5, 'Inactivo', 0),
(115, '1', 'S', 5, 'Activo', 0),
(116, '2', 'S', 5, 'Activo', 0),
(117, '3', 'S', 5, 'Activo', 0),
(118, '4', 'S', 5, 'Activo', 0),
(119, '5', 'S', 5, 'Activo', 0),
(120, '6', 'S', 5, 'Inactivo', 0),
(121, '1', 'T', 5, 'Activo', 0),
(122, '2', 'T', 5, 'Activo', 0),
(123, '3', 'T', 5, 'Activo', 0),
(124, '4', 'T', 5, 'Activo', 0),
(125, '5', 'T', 5, 'Activo', 0),
(126, '6', 'T', 5, 'Inactivo', 0),
(127, '1', 'U', 5, 'Activo', 0),
(128, '2', 'U', 5, 'Activo', 0),
(129, '3', 'U', 5, 'Activo', 0),
(130, '4', 'U', 5, 'Activo', 0),
(131, '5', 'U', 5, 'Activo', 0),
(132, '6', 'U', 5, 'Inactivo', 0),
(133, '1', 'V', 5, 'Activo', 0),
(134, '2', 'V', 5, 'Activo', 0),
(135, '3', 'V', 5, 'Activo', 0),
(136, '4', 'V', 5, 'Activo', 0),
(137, '5', 'V', 5, 'Activo', 0),
(138, '6', 'V', 5, 'Inactivo', 0),
(139, '1', 'W', 5, 'Activo', 0),
(140, '2', 'W', 5, 'Activo', 0),
(141, '3', 'W', 5, 'Activo', 0),
(142, '4', 'W', 5, 'Activo', 0),
(143, '5', 'W', 5, 'Activo', 0),
(144, '6', 'W', 5, 'Inactivo', 0),
(145, '1', 'X', 5, 'Activo', 0),
(146, '2', 'X', 5, 'Activo', 0),
(147, '3', 'X', 5, 'Activo', 0),
(148, '4', 'X', 5, 'Activo', 0),
(149, '5', 'X', 5, 'Activo', 0),
(150, '6', 'X', 5, 'Inactivo', 0),
(151, '1', 'Y', 5, 'Activo', 0),
(152, '2', 'Y', 5, 'Activo', 0),
(153, '3', 'Y', 5, 'Activo', 0),
(154, '4', 'Y', 5, 'Activo', 0),
(155, '5', 'Y', 5, 'Activo', 0),
(156, '6', 'Y', 5, 'Inactivo', 0),
(157, '1', 'Z', 5, 'Activo', 0),
(158, '2', 'Z', 5, 'Activo', 0),
(159, '3', 'Z', 5, 'Activo', 0),
(160, '4', 'Z', 5, 'Activo', 0),
(161, '5', 'Z', 5, 'Activo', 0),
(162, '6', 'Z', 5, 'Inactivo', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sesiones_salbrr`
--

CREATE TABLE IF NOT EXISTS `sesiones_salbrr` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sesiones_salbrr`
--

INSERT INTO `sesiones_salbrr` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('13bcdbe34e7e5691f1bfa21267dc9703', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422928554, ''),
('3eeb4db9a8969429cb9ac738631aeaf9', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422928544, ''),
('4f70265e180eadeef0116b57cce4ca86', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422928537, 'a:9:{s:9:"user_data";s:0:"";s:5:"admid";s:1:"1";s:11:"admnombreva";s:7:"Leonard";s:10:"admnivelen";s:13:"Administrador";s:10:"admloginva";s:7:"Leonard";s:13:"periodoActivo";s:9:"2014-2015";s:5:"perid";s:1:"5";s:12:"seccional_id";s:1:"0";s:13:"valida_sesion";b:1;}'),
('6575e91fd499f98e7090bd36a4bc6892', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422930844, 'a:9:{s:9:"user_data";s:0:"";s:5:"admid";s:1:"1";s:11:"admnombreva";s:7:"Leonard";s:10:"admnivelen";s:13:"Administrador";s:10:"admloginva";s:7:"Leonard";s:13:"periodoActivo";s:9:"2014-2015";s:5:"perid";s:1:"5";s:12:"seccional_id";s:1:"0";s:13:"valida_sesion";b:1;}'),
('78c4336ab53f15ac09a521c8019364de', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422928541, ''),
('9ee99017e43ab2fd4f2262e3903475df', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422926806, 'a:9:{s:9:"user_data";s:0:"";s:5:"admid";s:1:"1";s:11:"admnombreva";s:7:"Leonard";s:10:"admnivelen";s:13:"Administrador";s:10:"admloginva";s:7:"Leonard";s:13:"periodoActivo";s:9:"2014-2015";s:5:"perid";s:1:"5";s:12:"seccional_id";s:1:"0";s:13:"valida_sesion";b:1;}'),
('a0af8f84ce9654f0c76814b617fc2b65', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422925012, ''),
('fd27b80c4b1d977d11d47bd384df6342', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:34.0) Gecko/20100101 Firefox/34.0', 1422928559, '');

-- --------------------------------------------------------

--
-- Table structure for table `tiposcitaciones`
--

CREATE TABLE IF NOT EXISTS `tiposcitaciones` (
  `tipcodigoti` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `tipnombreva` varchar(100) NOT NULL,
  PRIMARY KEY (`tipcodigoti`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Tipo de Citacion (Mala conducta, Inasistencia...)' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tiposcitaciones`
--

INSERT INTO `tiposcitaciones` (`tipcodigoti`, `tipnombreva`) VALUES
(1, 'Por Inasistencias'),
(2, 'Por Debilidad en el Comportamiento'),
(3, 'Por bajo rendimiento escolar'),
(4, 'Por reunión de Representantes');

-- --------------------------------------------------------

--
-- Table structure for table `unidadescurriculares`
--

CREATE TABLE IF NOT EXISTS `unidadescurriculares` (
  `uniid` int(11) NOT NULL AUTO_INCREMENT,
  `unicodigova` varchar(20) NOT NULL,
  `uninombreva` varchar(40) NOT NULL,
  `unicodanoescolarva` varchar(2) NOT NULL,
  `uniestatusen` enum('Activo','Inactivo') NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`uniid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Unidad Curricular (Materia - Asignatura)' AUTO_INCREMENT=23 ;

--
-- Dumping data for table `unidadescurriculares`
--

INSERT INTO `unidadescurriculares` (`uniid`, `unicodigova`, `uninombreva`, `unicodanoescolarva`, `uniestatusen`) VALUES
(1, 'Biolog1', 'Biologia Primero', '1', 'Activo'),
(2, 'Matrma1', 'Matemática Primero', '1', 'Activo'),
(3, 'Castel1', 'Castellano Primero', '1', 'Activo'),
(4, 'Religi1', 'Religion Primero', '1', 'Activo'),
(5, 'Deport1', 'Deporte Primero', '1', 'Activo'),
(6, 'Castel2', 'Castellano Segundo', '2', 'Activo'),
(7, 'Matema2', 'Matematica Segundo', '2', 'Activo'),
(8, 'Ingles2', 'Ingles Segundo', '2', 'Activo'),
(9, 'Matema3', 'Matematica Tercero', '3', 'Activo'),
(10, 'Castel3', 'Castellano Tercero', '3', 'Activo'),
(11, 'Ingles3', 'Ingles Tercero', '3', 'Activo'),
(12, 'Deport3', 'Deporte Tercero', '3', 'Activo'),
(13, 'Castel4', 'Castellano Cuarto', '4', 'Activo'),
(14, 'Matema4', 'Matematica Cuarto', '4', 'Activo'),
(15, 'Religi4', 'Religion Cuarto', '4', 'Activo'),
(16, 'Deport4', 'Deporte Cuarto', '4', 'Activo'),
(17, 'deport5', 'deporte Quinto', '5', 'Activo'),
(18, 'Matema5', 'Matematica Quinto', '5', 'Activo'),
(19, 'Franc5', 'Francés', '5', 'Activo'),
(20, 'Pueri1', 'Puericultura de Primero', '1', 'Activo'),
(21, 'Pueri3', 'Puericultura Tercero', '3', 'Activo'),
(22, 'Pueri3', 'Puericultura Tercero', '3', 'Activo');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
