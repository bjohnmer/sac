jQuery(document).ready(function($) {

  $(".fecha").datepicker({
    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
    changeMonth: true,
    changeYear: true,
    maxDate: '-16y',
    yearRange: "-100:",
    dateFormat: 'dd/mm/yy'
  });
  $('.datepicker-input-clear').click(function(){
    $('.fecha').val('');
  });

});