$(document).ready(function(){
	
	$("#anoescolar").change(function () {

		$("#anoescolar option:selected").each(function () {

			var URLActual = document.URL;
			var arr = URLActual.split("/");
			var URLFinal = "";

			var ultimo = arr.length - 1; //arr[ultimo].length
			if (arr[ultimo] != 'inasistencias') 
			{
				if (arr[ultimo] == 'notas')
				{
					URLfinal = 'inasistencias';
				}
				else
				{
					URLfinal = URLActual.substr(0, URLActual.length - arr[ultimo].length - 1 ) ;
				};
			
			}
			else
			{
				URLfinal = URLActual;
			};

		   	elegido = $(this).val();

			$.post(URLfinal+"/getSecciones", { elegido: elegido }, function(data){

				$("#seccion").html(data);

			});
			$.post(URLfinal+"/getMaterias", { elegido: elegido }, function(data){

				$("#materia").html(data);

			});


		});

	});
	$("#fecha").datepicker({
	  monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	  changeMonth: true,
	  changeYear: true,
	  maxDate: '0d',
	  minDate: '-1y',
	  yearRange: "-1:",
      dateFormat: 'dd/mm/yy'
	});

});