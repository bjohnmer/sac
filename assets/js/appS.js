jQuery(document).ready(function($) {
	$("#field_seccodanoescolarva_chzn, #field_seccodanoescolarva_chzn > .chzn-drop").css("width",200);
	$("#field_seccodanoescolarva_chzn > .chzn-drop > .chzn-search > input").css("width",190);
	
	$("#field_seccodigova_chzn, #field_seccodigova_chzn > .chzn-drop").css("width",80);
	$("#field_seccodigova_chzn > .chzn-drop > .chzn-search > input").css("width",70);

	$("#field_secestatusen_chzn, #field_secestatusen_chzn > .chzn-drop").css("width",100);
	$("#field_secestatusen_chzn > .chzn-drop > .chzn-search > input").css("width",90);

  $.post("consultarTopeAS", function(data){
    
    var topeAS = data;

    $('#unod').click(function() {
      if($('#secciones1 option:selected').val() != $('#secciones2 option:selected').val())
      {
        if (confirm("¿Está seguro de ejecutar el cambio de sección a éste estudiante?")) 
        {
          
          if($('#estudiantes2 option').length < topeAS)
          {
            var esDesde = $('#estudiantes1 option:selected').val();
            var seDesde = $('#secciones1 option:selected').val();
            var seHasta = $('#secciones2 option:selected').val();
            anoid = $("#anoid").val();
                    
            $.post("ejecutarCambios", { 
              esDesde: esDesde, 
              seDesde: seDesde,
              seHasta: seHasta,
              anoid: anoid
            }, function(data){

              alert(data);

            });

            $('#estudiantes1 option:selected').remove().appendTo('#estudiantes2');
          }
          else
          {
            alert("Usted no puede hacer cambio de estudiantes\n Esta sección está completa, seleccione otra");
          }
        };
      }
      else
      {
        alert("Usted no puede hacer cambio de estudiantes en la misma sección \nSeleccione otra sección");
      }
      return false;
    }); 
    
    $('#unoi').click(function() {
      if($('#secciones2 option:selected').val() != $('#secciones1 option:selected').val())
      {
        
        if (confirm("¿Está seguro de ejecutar el cambio de sección a éste estudiante?")) 
        {

          
          if($('#estudiantes1 option').length < topeAS)
          {

          
            var esDesde = $('#estudiantes2 option:selected').val();
            var seDesde = $('#secciones2 option:selected').val();
            var seHasta = $('#secciones1 option:selected').val();
            anoid = $("#anoid").val();
                    
            $.post("ejecutarCambios", { 
              esDesde: esDesde, 
              seDesde: seDesde,
              seHasta: seHasta,
              anoid: anoid
            }, function(data){

              alert(data);

            });

            $('#estudiantes2 option:selected').remove().appendTo('#estudiantes1');
          }
          else
          {
            alert("Usted no puede hacer cambio de estudiantes\n Esta sección está completa, seleccione otra");
          }
        };  
      
      }
      else
      {
        alert("Usted no puede hacer cambio de estudiantes en la misma sección \nSeleccione otra sección");
      }
      return false;
    });  
    
  }) ;
   

  $("#secciones1").change(function () {

  	$("#secciones1 option:selected").each(function () {

  	   	elegido = $(this).val();
  	   	anoid = $("#anoid").val();
  		
  		$.post("getEstudiantes", { elegido: elegido, anoid:anoid }, function(data){

  			$("#estudiantes1").html(data);

  		},"json");

  	});

  });

  $("#secciones2").change(function () {

  	$("#secciones2 option:selected").each(function () {

  	   	elegido = $(this).val();
  	   	anoid = $("#anoid").val();
  		$.post("getEstudiantes", { elegido: elegido, anoid:anoid }, function(data){

  			$("#estudiantes2").html(data);

  		},"json");

  	});

  });

  var URLActual = document.URL;
  var arr = URLActual.split("/");
  var URLFinal = "";

  var ultimo = arr.length;

  console.log(arr[3]);

  if (arr[3]=='secciones') {
    if (arr[5] == 'edit') {
      $('#field_seccodanoescolarva_chzn .chzn-drop').css('display', 'none');
      $('#field_seccodanoescolarva_chzn .search-choice-close').css('display', 'none');

      $('#field_seccodigova_chzn .chzn-drop').css('display', 'none');
      $('#field_seccodigova_chzn .search-choice-close').css('display', 'none');

      $('#field_periodo_chzn .chzn-drop').css('display', 'none');
      $('#field_periodo_chzn .search-choice-close').css('display', 'none');
      

      var href = URLActual.split('/');
      var id = href[href.length - 1];
      
      var URL = window.location.pathname;
      var arr = URL.split("/");

      if (arr[1]=='salbrr') 
      {
        URL = window.location.protocol + "//" + window.location.hostname + "/" + arr[1] + "/secciones/puedeEditar/";
      }
      else
      {
        URL = "/secciones/puedeEditar/";
      };
      
      $.post(URL, { secid:id }, function(data){
        if (data.puede == "No") {
          $('#field_secestatusen_chzn .chzn-drop').css('display', 'none');
          $('#field_secestatusen_chzn .search-choice-close').css('display', 'none');
        };
      },"json");

      
    };
  };



});