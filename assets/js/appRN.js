$(document).ready(function(){
	
	$("#anoescolar").change(function () {

		$("#anoescolar option:selected").each(function () {

			elegido = $(this).val();

			$.post("/reportes/getSecciones", { elegido: elegido }, function(data){

				$("#seccion").html(data);

			});

		});

	});
	$("#fecha").datepicker({
	  monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	  changeMonth: true,
	  changeYear: true,
	  maxDate: '0d',
	  minDate: '-1y',
	  yearRange: "-1:",
      dateFormat: 'dd/mm/yy'
	});

});